package com.two.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.AclUser;

public interface AclUserDao extends BaseMapper<AclUser> {
}