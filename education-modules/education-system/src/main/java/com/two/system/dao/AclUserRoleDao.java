package com.two.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.AclUserRole;

import java.util.List;

public interface AclUserRoleDao extends BaseMapper<AclUserRole> {

    void batchInsert(List<AclUserRole> list);
}