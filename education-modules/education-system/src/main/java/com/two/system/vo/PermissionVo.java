package com.two.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/7 13:33
 * @description：
 * @modified By：
 * @version:
 */
@Data
@ApiModel(value = "搜索菜单对象")
public class PermissionVo {
    @ApiModelProperty(value = "菜单名")
    private String name;
    @ApiModelProperty(value = "菜单状态")
    private Integer isDeleted;
    @ApiModelProperty(value = "ids")
    private List<String> permissionIds;
}