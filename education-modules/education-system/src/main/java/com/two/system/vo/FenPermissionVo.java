package com.two.system.vo;

import lombok.Data;

import java.util.List;

@Data
public class FenPermissionVo {
    private String roleId;

    private List<String> permissionIds;
}