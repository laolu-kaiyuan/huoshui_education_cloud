package com.two.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.two.core.entity.AclRolePermission;
import com.two.system.dao.AclRolePermissionDao;
import com.two.system.service.AclRolePermissionService;
import org.springframework.stereotype.Service;

@Service
public class AclRolePermissionServiceImpl extends ServiceImpl<AclRolePermissionDao, AclRolePermission> implements AclRolePermissionService {
}