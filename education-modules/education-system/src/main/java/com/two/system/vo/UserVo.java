package com.two.system.vo;

import lombok.Data;

@Data
public class UserVo {
    private String username;
    private String startDate;
    private String endDate;

}