package com.two.system.service;

import com.two.core.entity.AclPermission;
import com.two.core.vo.Result;
import com.two.system.vo.FenPermissionVo;
import com.two.system.vo.PermissionVo;
import com.two.system.vo.RoleVo;

import java.util.List;
import java.util.Map;

public interface AclPermissionService {

    public Result findByUserId(String userid);

    public Result<List<AclPermission>> findByUsername();

    /**
     * 查询所有的权限 和 当前角色具有的权限id
     * @return
     */
    public Result<Map<String,Object>> findAll(String roleId);

    Result confirmFenPermission(FenPermissionVo fenPermissionVo);

    /**
     * 根据条件分页查询具有的权限
     * @param permissionVo 条件对象
     * @param current 页码
     * @param pageSize 每页显示的记录
     * @return
     */
    Result findByConditionPage(PermissionVo permissionVo, Integer current, Integer pageSize);

    /**
     * 添加菜单
     * @param aclPermission
     * @return
     */
    Result insert(AclPermission aclPermission);

    /**
     * 删除
     * @param id
     * @return
     */
    Result deleteById(List<String> ids);
}