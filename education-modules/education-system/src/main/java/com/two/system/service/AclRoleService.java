package com.two.system.service;

import com.two.core.entity.AclRole;
import com.two.core.vo.Result;
import com.two.system.vo.FenRoleVo;
import com.two.system.vo.RoleVo;

public interface AclRoleService {
    public Result findRoleByUserId(String userid);

    Result fenUserRole(FenRoleVo fenRoleVo);

    /**
     * 根据条件分页搜索角色信息
     * @param roleVo 条件对象
     * @param current 页码
     * @param pageSize 每页显示的记录
     * @return
     */
    Result findByConditionPage(RoleVo roleVo, Integer current, Integer pageSize);

    /**
     * 添加角色
     * @param aclRole
     * @return
     */
    Result addOrUpdateRole(AclRole aclRole);

    /**
     * 修改角色
     * @param aclRole
     * @return
     */
    Result updateRoleById(AclRole aclRole);

    /**
     * 改变角色状态
     * @param id
     * @param deleted
     * @return
     */
    Result updateRoleStatusByid(String id,Integer deleted);

    /**
     * 删除角色
     * @param id
     * @return
     */
    Result deleteRoleById(String id);

}