package com.two.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.AclRolePermission;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/6 16:48
 * @description：
 * @modified By： * @version:
 */
public interface AclRolePermissionDao extends BaseMapper<AclRolePermission> {
}