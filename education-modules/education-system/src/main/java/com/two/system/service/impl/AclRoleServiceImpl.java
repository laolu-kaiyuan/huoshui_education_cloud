package com.two.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.AclRole;
import com.two.core.entity.AclRolePermission;
import com.two.core.entity.AclUser;
import com.two.core.entity.AclUserRole;
import com.two.core.vo.Result;
import com.two.system.dao.AclRoleDao;
import com.two.system.dao.AclRolePermissionDao;
import com.two.system.dao.AclUserDao;
import com.two.system.dao.AclUserRoleDao;
import com.two.system.service.AclRoleService;
import com.two.system.vo.FenRoleVo;
import com.two.system.vo.RoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AclRoleServiceImpl implements AclRoleService {
    @Autowired
    AclUserDao userDao;
    @Autowired
    private AclRoleDao roleDao;
    @Autowired
    private AclUserRoleDao userRoleDao;
    @Autowired
    private AclRolePermissionDao rolePermissionDao;


    @Override
    public Result findRoleByUserId(String userid) {
        //1. 查询所有的角色
        List<AclRole> aclRoles = roleDao.selectList(null);
        //2. 查询当前用户具有的角色
        List<AclRole> userRoles = roleDao.selectByUserid(userid);
        List<String> userRoleIds = userRoles.stream().map(item -> item.getId()).collect(Collectors.toList());
        Map<String,Object> map=new HashMap<>();
        map.put("aclRoles",aclRoles);
        map.put("userRoles",userRoleIds);
        return new Result(2000,"查询成功",map);
    }

    @Override
    @Transactional
    public Result fenUserRole(FenRoleVo fenRoleVo) {
        //删除当前用户具的角色
        UpdateWrapper<AclUserRole> wrapper=new UpdateWrapper<>();
        wrapper.eq("user_id",fenRoleVo.getUserId());
        userRoleDao.delete(wrapper);
        //添加该用户具有的新角色 insert into 表名 values(值,值...)(值,值...)(值,值....)  动态sql语句
        List<AclUserRole> list=new ArrayList<>();
        for(String roleId : fenRoleVo.getRoleIds()){
            AclUserRole aclUserRole = new AclUserRole();
            aclUserRole.setIsDeleted(0);
            aclUserRole.setRoleId(roleId);
            aclUserRole.setUserId(fenRoleVo.getUserId());
            aclUserRole.setGmtCreate(LocalDateTime.now());
            aclUserRole.setGmtModified(LocalDateTime.now());
            list.add(aclUserRole);
        }
        userRoleDao.batchInsert(list);

        return new Result(2000,"分配角色成功");
    }
    @Override
    public Result findByConditionPage(RoleVo roleVo, Integer current, Integer pageSize) {
        IPage<AclRole> page=new Page<>(current,pageSize);
        QueryWrapper<AclRole> wrapper=new QueryWrapper<>();
        if(StringUtils.hasText(roleVo.getRoleName())){
            wrapper.like("role_name",roleVo.getRoleName());
        }
        if(roleVo.getIsDeleted()!=null){
            wrapper.eq("is_deleted",roleVo.getIsDeleted());
        }

        IPage<AclRole> page1 = roleDao.selectPage(page, wrapper);
        return new Result(2000,"分页查询角色成功",page1);
    }

    @Override
    public Result addOrUpdateRole(AclRole aclRole) {
        aclRole.setGmtCreate(LocalDateTime.now());
        aclRole.setGmtModified(LocalDateTime.now());
        int insert = roleDao.insert(aclRole);
        if (insert > 0) {
            return new Result(2000,"添加成功");
        }
        return new Result(5000,"添加失败");
    }

    @Override
    public Result updateRoleStatusByid(String id, Integer deleted) {
        AclRole aclRole = new AclRole();
        aclRole.setId(id);
        aclRole.setIsDeleted(deleted);
        int i = roleDao.updateById(aclRole);
        if (i > 0) {
            return new Result(2000,"禁用成功");
        }
        return new Result(5000,"禁用失败");
    }

    @Override
    @Transactional
    public Result deleteRoleById(String id) {
        //根据角色id查用户角色中间表,检查是否被用户具有
        QueryWrapper<AclUserRole> userRoleWrapper=new QueryWrapper<>();
        userRoleWrapper.eq("role_id",id);
        List<AclUserRole> aclUserRoles = userRoleDao.selectList(userRoleWrapper);
        if (aclUserRoles != null && aclUserRoles.size() > 0) {
            String userId = aclUserRoles.get(0).getUserId();
            AclUser aclUser = userDao.selectById(userId);
            return new Result(5000,"该角色正在被"+aclUser.getUsername()+"等"+aclUserRoles.size()+"名用户具有");
        }
        //删除角色表和角色权限表
        int i = roleDao.deleteById(id);
        QueryWrapper<AclRolePermission> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("role_id",id);
        int delete = rolePermissionDao.delete(queryWrapper);
        if (delete > 0 && i >0) {
            return new Result<>(2000,"删除成功");
        }
        return new Result<>(5000,"删除失败");
    }

    @Override
    public Result updateRoleById(AclRole aclRole){
        int i = roleDao.updateById(aclRole);
        if (i > 0){
            return new Result<>(2000,"修改成功");
        }
        return new Result<>(5000,"修改失败");
    }
}