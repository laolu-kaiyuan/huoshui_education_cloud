package com.two.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.AclUser;
import com.two.core.vo.Result;
import com.two.system.vo.UserVo;

public interface AclUserService {

    public Result findByName(String username);

    Result<AclUser> findUserinfo();

    /**
     * 根据条件分页查询用户列表
     * @param current 页码
     * @param pageSize 每页显示的条数
     * @param userVo 条件类
     * @return
     */
    Result<IPage<AclUser>>  findByConditionPage(Integer current, Integer pageSize, UserVo userVo);

    /**
     * 修改用户状态
     * @return
     */
    Result updateUserStatusByid(String id,Integer deleted);

    /**
     * 添加用户
     * @param user
     * @return
     */
    Result addUserByUser(AclUser user);

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    Result deleteUserById(String id);

    /**
     * 修改用户
     * @param user
     * @return
     */
    Result updateUserByUser(AclUser user);
}