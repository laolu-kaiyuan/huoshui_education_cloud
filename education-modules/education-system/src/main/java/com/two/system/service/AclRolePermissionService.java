package com.two.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.two.core.entity.AclRolePermission;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/6 16:52
 * @description：
 * @modified By： * @version:
 */
public interface AclRolePermissionService extends IService<AclRolePermission> {
}