package com.two.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.AclPermission;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AclPermissionDao extends BaseMapper<AclPermission> {

    /**
     * 根据用户id查询权限信息
     * @return 不想写映射文件
     */
    @Select(value = "select p.* from acl_permission p join acl_role_permission rp on p.id=rp.permission_id " +
            "join acl_user_role ur on ur.role_id=rp.role_id where ur.user_id=#{userid}")
    public List<AclPermission> selectByUserid(String userid);

    /**
     * 根据用户名查询具有的权限。
     * @param username
     * @return
     */
    @Select(value = "select p.* from acl_permission p join acl_role_permission rp on p.id=rp.permission_id " +
            "join acl_user_role ur on ur.role_id=rp.role_id join acl_user u on ur.user_id=u.id " +
            "where u.username=#{username} and p.type=1")
    public List<AclPermission> selectByUsername(String username);
    /**
     * 根据用户名查询具有的权限。
     * @param
     * @return
     */
    @Select(value = "select p.* from acl_permission p join acl_role_permission rp on p.id=rp.permission_id " +
            "join acl_user_role ur on ur.role_id=rp.role_id join acl_user u on ur.user_id=u.id " +
            "where u.username=#{username} and p.type in (1,2,3)")
    public List<AclPermission> selectAll(String username);

    /**
     * 根据用户名和权限查询具有的权限。
     * @param username
     * @return
     */
    @Select(value = "select p.* from acl_permission p join acl_role_permission rp on p.id=rp.permission_id " +
            "join acl_user_role ur on ur.role_id=rp.role_id join acl_user u on ur.user_id=u.id " +
            "where u.username=#{username} and p.name like #{name} and p.type in (1,2) and p.is_deleted=0")
    public List<AclPermission> selectIdsByUsername(String username,String name);


    /**
     * 根据角色id查询对于的权限id
     * @param roleId
     * @return
     */
    @Select(value="select distinct permission_id from acl_role_permission where role_id=#{roleId}")
    List<String> selectByRoleId(String roleId);
}