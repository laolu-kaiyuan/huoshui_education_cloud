package com.two.system.controller;

import com.two.core.entity.AclRole;
import com.two.core.entity.AclUser;
import com.two.core.vo.Result;
import com.two.system.service.AclRoleService;
import com.two.system.vo.FenRoleVo;
import com.two.system.vo.RoleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/5 23:49
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/system/role")
@Api(tags = "角色的API接口类")
public class AclRoleController {

    @Autowired
    private AclRoleService roleService;
    @GetMapping("findRoleByUserId/{userid}")
    public Result findRoleByUserId(@PathVariable String userid){
        return roleService.findRoleByUserId(userid);
    }

    @PostMapping("/fenUserRole")
    public Result fenUserRole(@RequestBody FenRoleVo fenRoleVo){
        return roleService.fenUserRole(fenRoleVo);
    }

    @ApiOperation(value = "根据条件分页查询角色信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current",value = "当前页码"),
            @ApiImplicitParam(name = "pageSize",value = "每页显示的条数")
    })
    @PostMapping("/findRoleByPage/{current}/{pageSize}")
    public Result findRoleByPage(@RequestBody RoleVo roleVo,
                                 @PathVariable Integer current,
                                 @PathVariable Integer pageSize
    ){
        return roleService.findByConditionPage(roleVo,current,pageSize);
    }

    @PostMapping("/addOrUpdateRole")
    @ApiOperation(value = "添加角色")
    public Result addOrUpdateRole(@RequestBody AclRole aclRole){
        return roleService.addOrUpdateRole(aclRole);
    }
    @PostMapping("/updateRoleById")
    @ApiOperation(value = "修改角色")
    public Result updateRoleById(@RequestBody AclRole aclRole){
        return roleService.updateRoleById(aclRole);
    }
    @DeleteMapping("deleteRoleById/{roleid}")
    @ApiOperation(value = "删除角色")
    public Result deleteRoleById(@PathVariable String roleid){
        return roleService.deleteRoleById(roleid);
    }

    @ApiOperation(value = "改变角色状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "角色id"),
            @ApiImplicitParam(name = "deleted",value = "状态")})
    @PostMapping("updateRoleStatusByid/{id}/{deleted}")
    public Result updateRoleStatusByid(@PathVariable String id,
                                       @PathVariable Integer deleted){
        return roleService.updateRoleStatusByid(id,deleted);
    }

}