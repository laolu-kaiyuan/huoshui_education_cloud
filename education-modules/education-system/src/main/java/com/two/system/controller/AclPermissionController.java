package com.two.system.controller;

import com.two.core.entity.AclPermission;
import com.two.core.vo.Result;
import com.two.system.service.AclPermissionService;
import com.two.system.vo.FenPermissionVo;
import com.two.system.vo.PermissionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/permission")
@Api(tags = "权限接口类")
public class AclPermissionController {

    @Autowired
    private AclPermissionService permissionService;

    @GetMapping("/getByUserid/{userid}")
    @ApiOperation(value = "根据用户id查询具有的权限")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "用户名id",name = "userid")
            }
    )
    public Result<AclPermission> getByUserid(@PathVariable String userid){
        return permissionService.findByUserId(userid);
    }

    @GetMapping("getAllPermission")
    @ApiOperation(value = "根据当前登录用户名查询具有的权限")
    public Result<List<AclPermission>> getAllPermission(){
        return permissionService.findByUsername();
    }

    @PostMapping("findAllByPage/{currentPage}/{pageSize}")
    @ApiOperation(value = "根据条件分页查询具有的权限")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "currentPage",value = "当前页码"),
                    @ApiImplicitParam(name = "pageSize",value = "每页显示的条数")
            }
    )
    public Result<List<AclPermission>> findAllByPage(@PathVariable(required = true) Integer currentPage,
                                                     @PathVariable(required =true ) Integer pageSize,
                                                     @RequestBody PermissionVo permissionVo){
        return permissionService.findByConditionPage(permissionVo,currentPage,pageSize);
    }


    @GetMapping("findPermissionByRoleId/{roleId}")
    @ApiOperation(value = "根据角色id查询具有的权限")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "角色id",name = "roleId")
            }
    )
    public Result<Map<String,Object>> findPermissionByRoleId(@PathVariable String roleId){
        return permissionService.findAll(roleId);
    }

    @PostMapping("deleteById")
    @ApiOperation(value = "根据id删除菜单")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "ids" , name = "ids")
            }
            )
    public Result deleteById(@RequestBody PermissionVo permissionVo){
            return permissionService.deleteById(permissionVo.getPermissionIds());
    }
    @PutMapping("insert")
    @ApiOperation(value = "添加菜单")
    public Result insert(@RequestBody AclPermission aclPermission) {
        return permissionService.insert(aclPermission);
    }


    @PostMapping("/confirmFenPermission")
    public Result confirmFenPermission(@RequestBody FenPermissionVo fenPermissionVo){
        return permissionService.confirmFenPermission(fenPermissionVo);
    }

}