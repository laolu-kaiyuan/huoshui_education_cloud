package com.two.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.AclRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AclRoleDao extends BaseMapper<AclRole> {
    @Select(value = "select distinct r.* from acl_role r join acl_user_role ur on r.id=ur.role_id where ur.user_id=#{userid}")
    List<AclRole> selectByUserid(String userid);

    @Delete(value = "delete from acl_user_role where user_id=#{userId}")
    void deleteByUserId(String userId);


//    void batchInsert(@Param("userId") String userId, @Param("roleIds") List<String> roleIds);
}