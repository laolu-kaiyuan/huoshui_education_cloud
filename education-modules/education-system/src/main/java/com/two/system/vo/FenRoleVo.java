package com.two.system.vo;

import lombok.Data;

import java.util.List;

@Data
public class FenRoleVo {
    private String userId;
    private List<String> roleIds;
}