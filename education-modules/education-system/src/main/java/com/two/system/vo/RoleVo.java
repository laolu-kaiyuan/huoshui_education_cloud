package com.two.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "搜索角色对象")
public class RoleVo {
    @ApiModelProperty(value = "角色名")
    private String roleName;
    @ApiModelProperty(value = "角色状态")
    private Integer isDeleted;
}