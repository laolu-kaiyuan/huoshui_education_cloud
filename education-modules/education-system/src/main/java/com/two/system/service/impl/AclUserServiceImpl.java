package com.two.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.AclUser;
import com.two.core.entity.AclUserRole;
import com.two.core.util.JWTUtil;
import com.two.core.vo.Result;
import com.two.security.util.WebUtils;
import com.two.system.dao.AclUserDao;
import com.two.system.dao.AclUserRoleDao;
import com.two.system.service.AclUserService;
import com.two.system.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

@Service
public class AclUserServiceImpl implements AclUserService {
    @Autowired
    private AclUserDao userDao;
    @Autowired
    private AclUserRoleDao userRoleDao;
    @Override
    public Result findByName(String username) {
        QueryWrapper<AclUser> wrapper=new QueryWrapper<>();
        wrapper.eq("username",username);
        wrapper.eq("is_deleted",0);
        AclUser aclUser = userDao.selectOne(wrapper);
        if(aclUser!=null){
           return new Result<AclUser>(2000,"查询用户信息成功",aclUser);
        }
        return null;
    }
    @Override
    public Result<AclUser> findUserinfo() {
        //1.得到当前用户的名称
        String token = WebUtils.getRequest().getHeader("token");
        Map<String, Object> map = JWTUtil.getTokenChaim(token);
        String username = (String) map.get("username");

        QueryWrapper<AclUser> wrapper=new QueryWrapper<>();
        wrapper.eq("username",username);
        wrapper.eq("is_deleted",0);
        AclUser aclUser = userDao.selectOne(wrapper);

        return new Result<>(2000,"获取用户信息",aclUser);
    }
    //如果使用mp分页 需要配置分页拦截器
    @Override
    public Result<IPage<AclUser>> findByConditionPage(Integer current, Integer pageSize, UserVo userVo) {
        IPage<AclUser> page=new Page(current,pageSize);

        QueryWrapper<AclUser> wrapper=new QueryWrapper<>();
        if(StringUtils.hasText(userVo.getUsername())){
            wrapper.like("username",userVo.getUsername());
        }

        if(StringUtils.hasText(userVo.getStartDate())){
            wrapper.ge("gmt_create",userVo.getStartDate());
        }

        if(StringUtils.hasText(userVo.getEndDate())){
            wrapper.le("gmt_create",userVo.getEndDate());
        }

        IPage<AclUser> iPage = userDao.selectPage(page, wrapper);


        return new Result<>(2000,"查询成功",iPage);
    }

    @Override
    public Result updateUserStatusByid(String id,Integer deleted) {
        AclUser aclUser=new AclUser();
        aclUser.setIsDeleted(deleted);
        aclUser.setId(id);
        int i = userDao.updateById(aclUser);
        if (i > 0) {
            return new Result<>(2000,"成功");
        }
        return new Result<>(5000,"失败");
    }

    @Override
    public Result addUserByUser(AclUser user) {
        user.setGmtCreate(LocalDateTime.now());
        user.setGmtModified(LocalDateTime.now());
        String password = user.getPassword();
        BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode(password);
        user.setPassword(encode);
        int insert = userDao.insert(user);
        if (insert > 0) {
            return new Result<>(2000,"添加成功");
        }
        return new Result<>(5000,"添加失败");
    }

    @Override
    @Transactional
    public Result deleteUserById(String id) {
        int i = userDao.deleteById(id);
        QueryWrapper<AclUserRole> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("user_id",id);
        int delete = userRoleDao.delete(queryWrapper);
        if (delete > 0 && i >0) {
            return new Result<>(2000,"删除成功");
        }
        return new Result<>(5000,"删除失败");
    }

    @Override
    public Result updateUserByUser(AclUser user) {
        int i = userDao.updateById(user);
        if (i > 0){
            return new Result<>(2000,"修改成功");
        }
        return new Result<>(5000,"修改失败");
    }

}