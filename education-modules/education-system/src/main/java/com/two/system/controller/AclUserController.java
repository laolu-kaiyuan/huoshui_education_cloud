package com.two.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.AclUser;
import com.two.core.vo.Result;
import com.two.security.annotation.RequiresPermissions;
import com.two.system.service.AclUserService;
import com.two.system.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/system/user")
@Api(tags = "用户的API接口类")
public class AclUserController {
    @Autowired
    private AclUserService userService;

    @GetMapping("/getByName/{username}")
    public Result<AclUser> getName(@PathVariable String username){
        return userService.findByName(username);
    }

    @GetMapping("list")
    @RequiresPermissions(value="user:list")
    public String list(){
        return "用户查询";
    }

    @GetMapping("delete")
    @RequiresPermissions(value="user:delete")
    public String delete(){
        return "用户查询";
    }
    @GetMapping("info")
    public Result<AclUser> info(){

        return userService.findUserinfo();
    }

    @PostMapping("getUserByPage/{current}/{pageSize}")
    @ApiOperation(value = "分页查询用户")
    public Result<IPage<AclUser>> getUserByPage(@PathVariable(required = true) Integer current, @PathVariable(required =true ) Integer pageSize,
                                                @RequestBody UserVo userVo
    ){
        return userService.findByConditionPage(current,pageSize,userVo);
    }

    @ApiOperation(value = "改变用户状态")
    @PostMapping("/updateUserStatusByid/{id}/{deleted}")
    public Result updateUserStatusByid(@PathVariable(required = true) String id,
                                       @PathVariable(required =true ) Integer deleted){
        return userService.updateUserStatusByid(id,deleted);
    }
    @PostMapping("/addUserByUser")
    @ApiOperation(value = "添加用户")
    public Result addUserByUser(@RequestBody AclUser aclUser){
        return userService.addUserByUser(aclUser);
    }
    @DeleteMapping("/deleteUserById/{id}")
    @ApiOperation(value = "删除用户")
    public Result deleteUserById(@PathVariable(required = true) String id){
        return userService.deleteUserById(id);
    }
    @PostMapping("/updateUserByUser")
    @ApiOperation(value = "修改用户")
    public Result updateUserByUser(@RequestBody AclUser aclUser){
        return userService.updateUserByUser(aclUser);
    }
}