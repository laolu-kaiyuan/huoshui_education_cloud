package com.two.member.entity;

import java.util.Date;
import java.io.Serializable;

import io.swagger.models.auth.In;
import lombok.Data;

/**
 * 会员表(TbMember)实体类
 *
 * @author makejava
 * @since 2022-12-10 05:18:29
 */
@Data
public class TbMember implements Serializable {
    private static final long serialVersionUID = -80128917973622028L;
    /**
     * 会员id
     */
    private String id;
    /**
     * 微信openid
     */
    private String openid;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别 1 女，0 男
     */
    private Integer sex;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 用户签名
     */
    private String sign;
    /**
     * 用户类型(0 会员 ,1 讲师)
     */
    private Integer isType;
    /**
     * 是否禁用 1（true）已禁用，  0（false）未禁用
     */
    private Integer isDisabled;
    /**
     * 逻辑删除 1（true）已删除， 0（false）未删除
     */
    private Integer isDeleted;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtModified;
    /**
     * 备注
     */
    private String remark;
    /**
     * 盐
     */
    private String salt;


}

