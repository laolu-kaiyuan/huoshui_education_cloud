package com.two.member.service.impl;

import com.two.member.service.MsmService;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.two.member.service.MsmService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @date ：Created in 2023/1/5 13:33
 * @description：发送验证码
 * @modified By：
 * @version:
 */
@Service
public class MsmServiceImpl implements MsmService {
    @Override
    public boolean send(Map<String, Object> param, String mobile) {
        if(StringUtils.isEmpty(mobile)) {
            return false;
        }
        DefaultProfile profile =
                DefaultProfile.getProfile("cn-beijing", "LTAI5t8uUhM2vTc68MCJ3Z2e", "g94ldBWHBmBnhLvhjgA5PBuU7xtG8C");
        IAcsClient client = new DefaultAcsClient(profile);

        //设置相关固定的参数
        CommonRequest request = new CommonRequest();
        //request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");

        //设置发送相关的参数
        request.putQueryParameter("PhoneNumbers",mobile); //手机号
        request.putQueryParameter("SignName","马的博客"); //申请阿里云 签名名称
        request.putQueryParameter("TemplateCode","SMS_267365052"); //申请阿里云 模板code
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(param)); //验证码数据，转换json数据传递

        try {
            //最终发送
            CommonResponse response = client.getCommonResponse(request);
            boolean success = response.getHttpResponse().isSuccess();
            return success;
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
