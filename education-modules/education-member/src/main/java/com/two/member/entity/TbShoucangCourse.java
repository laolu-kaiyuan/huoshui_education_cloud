package com.two.member.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * (TbShoucangCourse)实体类
 * @author makejava
 * @since 2023-01-30 20:37:13
 */
@Data
public class TbShoucangCourse implements Serializable {
    private static final long serialVersionUID = -84466418696088037L;

    private String id;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 课程id
     */
    private String courseId;


}

