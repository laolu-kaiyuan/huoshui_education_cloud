package com.two.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.course.EduCourse;
import com.two.core.vo.Result;
import com.two.member.entity.MemberStatusVo;
import com.two.member.entity.MemberVo;
import com.two.member.entity.ShoucangCourseVo;
import com.two.member.entity.TbMember;

import java.util.List;


/**
 * 会员表(TbMember)表服务接口
 * @author makejava
 * @since 2022-12-10 05:18:29
 */
public interface TbMemberService {
    /**
     * 根据条件分页查询用户列表
     * @param current 页码
     * @param pageSize 每页显示的条数
     * @return
     */
    /*Result<IPage<TbMember>> findByConditionPage(Integer current, Integer pageSize, MemberVo memberVo);*/
    Result<IPage<TbMember>> findAll(Integer current, Integer pageSize, MemberVo memberVo);
    Result<TbMember> stateChange(MemberStatusVo memberStatusVo);

    Result<TbMember> modifyStudent(TbMember tbMember);

    Result<List<TbMember>> getMemberAll();

    Result findStudentBymobile(String mobile);

    Result modifyStudentInformation(TbMember tbMember);

    Result getYanZhengMa(String mobile);

    Result updateMemberPassword(String mobile, String newPassword);

    Result<EduCourse> findFavoriteCourseById(String memberId);

    Result deleteFavoriteCourse(ShoucangCourseVo shoucangCourseVo);
}
