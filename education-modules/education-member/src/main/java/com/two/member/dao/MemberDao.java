package com.two.member.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduCourse;
import com.two.member.entity.TbMember;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 讲师(Teacher)表数据库访问层
 * @author makejava
 * @since 2022-12-08 12:39:26
 */
public interface MemberDao extends BaseMapper<TbMember> {

    @Select("select * from tb_member")
    List<TbMember> selectAll();

    List<EduCourse> findFavoriteCourseById(String memberId);
}
