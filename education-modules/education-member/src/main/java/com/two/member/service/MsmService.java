package com.two.member.service;

import java.util.Map;

/**
 * @date ：Created in 2023/1/5 13:32
 * @description： 发送验证码
 * @modified By：
 * @version:
 */
public interface MsmService {
    boolean send(Map<String,Object> param , String mobile);
}
