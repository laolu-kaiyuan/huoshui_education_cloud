package com.two.member.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.vo.Result;
import com.two.member.entity.MemberStatusVo;
import com.two.member.entity.MemberVo;
import com.two.member.entity.ShoucangCourseVo;
import com.two.member.entity.TbMember;

import com.two.member.service.TbMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 讲师(Teacher)表控制层
 * @author makejava
 * @since 2022-12-08 12:39:25
 */
@RestController
@RequestMapping("/student/member")
@Api(tags = "会员信息")
public class MemberController {
    /**
     * 服务对象
     */
    @Autowired
    private TbMemberService tbmemberService;

    /**
     * 分页查询
     */


    @ApiOperation(value = "分页查询")
    @PostMapping("/getStudentAll/{current}/{pageSize}")
    public Result<IPage<TbMember>>  getStudentAll(@PathVariable Integer current,
                                                  @PathVariable Integer pageSize,
                                                  @RequestBody MemberVo memberVo){
        System.out.println("------------------"+memberVo);
        return tbmemberService.findAll(current,pageSize,memberVo);
    }
    /**
     * @description: 编辑信息
     * @create time:
     * @return
     */
    @ApiOperation(value = "编辑信息")
    @PostMapping("modifyStudentInformation")
    public Result<TbMember>  modifyStudentInformation(@RequestBody TbMember tbMember){
        return tbmemberService.modifyStudent(tbMember);
    }
    /**
     * @description: 修改状态
     * @create time:
     * @return
     */
    @ApiOperation(value = "修改状态")
    @PostMapping("stateChange/{isDisabled}/{id}")
    public Result<TbMember>  stateChange( MemberStatusVo memberStatusVo){
        return tbmemberService.stateChange(memberStatusVo);
    }

    @PostMapping("getMemberAll")
    public Result<List<TbMember>> getMemberAll(){
        return tbmemberService.getMemberAll();
    }
    /**
     * @description: 根据手机号查询个人信息
     * @create time:
     * @return
     */
    @GetMapping("findStudentBymobile/{mobile}")
    @ApiOperation(value = "根据手机号查询个人信息")
    public Result findStudentBymobile(@PathVariable String mobile){
        System.out.println(mobile);
        return tbmemberService.findStudentBymobile(mobile);
    }
    /**
     * @description: 修改前端个人信息
     * @create time:
     * @return
     */
    @PostMapping("modifyInformation")
    @ApiOperation(value = "修改前端个人信息")
    public Result modifyInformation(@RequestBody TbMember tbMember){
        System.out.println(tbMember+"555555555555555");
        return tbmemberService.modifyStudentInformation(tbMember);
    }
    /**
     * @description: 根据手机号获取验证码
     * @create time:
     * @return
     */
    @GetMapping("getYanZhengMa/{mobile}")
    @ApiOperation(value = "根据手机号获取验证码")
    public Result getYanZhengMa(@PathVariable String mobile){
        System.out.println(mobile);
        return tbmemberService.getYanZhengMa(mobile);
    }
    /**
     * @description: 修改密码
     * @create time:
     * @return
     */
    @ApiOperation(value = "修改密码")
    @PostMapping("updateMemberPassword/{mobile}/{newPassword}")
    public Result updateMemberPassword(@PathVariable String mobile,@PathVariable String newPassword){
        return tbmemberService.updateMemberPassword(mobile,newPassword);
    }
    /**
     * @description: 根据会员id查询当前用户的所有收藏课程
     * @create time:
     * @return
     */
    @ApiOperation(value = "根据会员id查询当前用户的所有收藏课程")
    @PostMapping("findFavoriteCourseById/{memberId}")
    public Result findFavoriteCourseById(@PathVariable String memberId){
        return tbmemberService.findFavoriteCourseById(memberId);
    }
    /**
     * @create by: Teacher马
     * @description: 根据课程id和会员id取消收藏
     * @create time:
     * @return
     */
    @ApiOperation(value = "根据课程id和会员id取消收藏")
    @PostMapping("deleteFavoriteCourse")
    public Result deleteFavoriteCourse(@RequestBody ShoucangCourseVo shoucangCourseVo){
        System.out.println(shoucangCourseVo+"=========================");
        return tbmemberService.deleteFavoriteCourse(shoucangCourseVo);
    }
}

