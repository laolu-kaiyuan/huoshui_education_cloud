package com.two.member.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.member.entity.MemberShoucangCourseVo;
import com.two.member.entity.TbShoucangCourse;

/**
 * @date ：Created in 2023/1/10 15:14
 * @description：
 * @modified By：
 * @version:
 */
public interface ShoucangCourseDao extends BaseMapper<TbShoucangCourse> {
}
