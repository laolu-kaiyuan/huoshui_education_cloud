package com.two.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.course.EduCourse;
import com.two.core.vo.Result;
import com.two.member.dao.MemberDao;
import com.two.member.dao.ShoucangCourseDao;
import com.two.member.entity.*;
import com.two.member.service.MsmService;
import com.two.member.service.TbMemberService;

import com.two.member.utils.RandomCode;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 讲师(Teacher)表服务实现类
 * @author makejava
 * @since 2022-12-08 12:39:27
 */
@Service
public class MemberServiceImpl implements TbMemberService {
    @Autowired
    private MemberDao memberDao;

    @Autowired
    private MsmService smsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ShoucangCourseDao shoucangCourseDao;
/*    //如果使用mp分页 需要配置分页拦截器
    @Override
    public Result<IPage<TbMember>> findByConditionPage(Integer current, Integer pageSize, MemberVo memberVo) {
        IPage<TbMember> page=new Page(current,pageSize);

        QueryWrapper<TbMember> wrapper=new QueryWrapper<>();
        if (StringUtils.hasText(memberVo.getMobile())){
            wrapper.like("mobile",memberVo.getMobile());
        }
        if (StringUtils.hasText(memberVo.getNickname())){
            wrapper.like("nickname",memberVo.getNickname());
        }

//        if (ObjectUtil.isNotEmpty(memberVo.getGmtCreate())){
//            wrapper.ge("gmt_create",memberVo.getGmtCreate().get(0)).le("gmt_create",memberVo.getGmtCreate().get(1));
//        }

        IPage<TbMember> iPage = memberDao.selectPage(page, wrapper);


        return new Result<>(2000,"查询成功",iPage);
    }*/
@Override
public Result<IPage<TbMember>> findAll(Integer current, Integer pageSize, MemberVo memberVo) {
    IPage<TbMember> page=new Page<>(current,pageSize);
    QueryWrapper<TbMember> wrapper=new QueryWrapper<>();
    if (StringUtils.hasText(memberVo.getMobile())){
        wrapper.like("mobile",memberVo.getMobile());
    }
    if (StringUtils.hasText(memberVo.getNickname())){
        wrapper.like("nickname",memberVo.getNickname());
    }

    if (ObjectUtils.isNotEmpty(memberVo.getGmtCreate())){
        wrapper.ge("gmt_create",memberVo.getGmtCreate().get(0)).le("gmt_create",memberVo.getGmtCreate().get(1));
    }

    IPage<TbMember> page1 = memberDao.selectPage(page, wrapper);

    return new Result<>(200,"查询成功",page1);
}
    @Override
    public Result<TbMember> stateChange(MemberStatusVo memberStatusVo) {
        UpdateWrapper<TbMember> wrapper=new UpdateWrapper<>();
        if (StringUtils.hasText(memberStatusVo.getId())){
            wrapper.eq("id",memberStatusVo.getId());
        }
        TbMember tbMember=new TbMember();
        tbMember.setIsDisabled(memberStatusVo.getIsDisabled());
        memberDao.update(tbMember,wrapper);
        return new Result<>(200,"修改成功",tbMember);
    }
    /**
     * @description: 编辑信息
     * @create time:
     * @return
     */
    @Override
    public Result<TbMember> modifyStudent(TbMember tbMember) {
        TbMember tbMember1=new TbMember();
        tbMember1.setMobile(tbMember.getMobile());
        tbMember1.setNickname(tbMember.getNickname());
        tbMember1.setSex(tbMember.getSex());
        tbMember1.setRemark(tbMember.getRemark());
        QueryWrapper<TbMember> wrapper=new QueryWrapper<>();
        if (StringUtils.hasText(tbMember.getId())){
            wrapper.eq("id",tbMember.getId());
        }
        memberDao.update(tbMember1,wrapper);
        return new Result<>(200,"编辑成功",tbMember1);
    }
    @Override
    public Result<List<TbMember>> getMemberAll() {
        return new Result(2000,"查询成功",memberDao.selectAll());
    }
    /**
     * @description: 根据手机好查询个人资料
     * @create time:
     * @return
     */
    @Override
    public Result findStudentBymobile(String mobile) {
        QueryWrapper<TbMember> wrapper=new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        TbMember tbMember1 = memberDao.selectOne(wrapper);
        return new Result(200,"成功",tbMember1);
    }
    /**
     * @description: 根据手机好修改个人资料
     * @create time:
     * @return
     */
    @Override
    public Result modifyStudentInformation(TbMember tbMember) {
        TbMember tbMember1=new TbMember();
        tbMember1.setNickname(tbMember.getNickname());
        tbMember1.setAge(tbMember.getAge());
        tbMember1.setSex(tbMember.getSex());
        tbMember1.setAvatar(tbMember.getAvatar());
        UpdateWrapper<TbMember> wrapper=new UpdateWrapper<>();
        if (StringUtils.hasText(tbMember.getMobile())){
            wrapper.eq("mobile",tbMember.getMobile());
        }
        System.out.println(tbMember1+"6666666666666666666");
        memberDao.update(tbMember1,wrapper);

        return new Result(200,"修改成功",tbMember1);
    }
    /**
     * @description: 根据手机号获取验证码
     * @create time:
     * @return
     */
    @Override
    public Result getYanZhengMa(String mobile) {
        if(StringUtils.isEmpty(mobile)){
            return  new Result(500,"手机号为空");
        }

        //从redis中查看有没有该手机号的验证码
 /*       String verifyCode = redisTemplate.opsForValue().get("sms:phone:" + mobile);
        if (StringUtils.hasText(verifyCode)) {
            return new Result(200,"验证码已发送verify",verifyCode);
        }*/

        // 如果redis没有该手机号验证码，则获取验证码并发送短信a
        // 获取六位验证码
        String verifyCode = RandomCode.getSixBitRandom();
        // 短信内容{"code":{验证码}}
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("code", verifyCode);
        System.out.println(verifyCode+"  验证码");
        // 调用短信发送模板
        Boolean isSend = smsService.send(messageMap, mobile);
        if (isSend) {
            // 如果发送成功，则将对应手机号验证码存入redis中，设置5分钟内有效
            // redisTemplate.opsForValue().set("sms:phone:" + mobile, verifyCode, 5, TimeUnit.MINUTES);
            return new Result(200,"验证码已发送",messageMap);
        } else {
            return new Result( 500,"短信发送失败");
        }
    }

    /**
     * @description: 修改密码
     * @create time:
     * @return
     */
    @Override
    public Result updateMemberPassword(String mobile, String newPassword) {
        UpdateWrapper<TbMember> wrapper=new UpdateWrapper<>();
        if(StringUtils.hasText(mobile)){
            wrapper.eq("mobile",mobile);
        }
        TbMember tbMember=new TbMember();
        tbMember.setPassword(passwordEncoder.encode(newPassword));
        memberDao.update(tbMember,wrapper);
        return new Result(200,"修改成功",tbMember);
    }
    /**
     * @description: 根据会员id查询当前用户的所有收藏课程
     * @create time:
     * @return
     */
    @Override
    public Result<EduCourse> findFavoriteCourseById(String memberId) {
        List<EduCourse> course=null;
        if (StringUtils.hasText(memberId)){
            course = memberDao.findFavoriteCourseById(memberId);
        }

        return new Result(200,"成功",course);
    }
    /**
     * @description: 根据课程id和会员id取消收藏
     * @create time:
     * @return
     */
    @Override
    public Result deleteFavoriteCourse(ShoucangCourseVo shoucangCourseVo) {
        UpdateWrapper<TbShoucangCourse> wrapper=new UpdateWrapper<>();
        if (StringUtils.hasText(shoucangCourseVo.getMemberid())){
            wrapper.eq("member_id",shoucangCourseVo.getMemberid());
        }
        if (StringUtils.hasText(shoucangCourseVo.getCourseid())){
            wrapper.eq("course_id",shoucangCourseVo.getCourseid());
        }
        int delete = shoucangCourseDao.delete(wrapper);
        if (delete>0){
            return new Result(200,"成功",delete);
        }
        return new Result(500,"失败");
    }
}
