package com.two.member.entity;


import lombok.Data;

import java.util.List;

/**
 * @date ：Created in 2022/12/10 20:00
 * @description：
 * @modified By：
 * @version:
 */
@Data
public class MemberVo {
private  String mobile;
private  String nickname;
private List<String> gmtCreate;
    private String isDisabled;

}
