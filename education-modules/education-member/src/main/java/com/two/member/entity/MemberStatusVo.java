package com.two.member.entity;

import lombok.Data;

@Data
public class MemberStatusVo {
    private  String id;
    private Integer isDisabled;
}