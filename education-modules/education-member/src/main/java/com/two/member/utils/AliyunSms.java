package com.two.member.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @date ：Created in 2023/1/5 13:17
 * @description：
 * @modified By：
 * @version:
 */
@Component
public class AliyunSms implements InitializingBean {
    @Value("aliyun.sms.region_id")
    private String regionId;

    @Value("aliyun.sms.access_key_id")
    private String accessKeyId;

    @Value("aliyun.sms.secret")
    private String secret;

    public static String REGION_ID;
    public static String ACCESS_KEY_ID;
    public static String SECRET;
    @Override
    public void afterPropertiesSet() throws Exception {
        REGION_ID = regionId;
        ACCESS_KEY_ID = accessKeyId;
        SECRET = secret;
    }
}
