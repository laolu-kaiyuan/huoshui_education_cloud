package com.two.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//springboot默认扫描的主类所在的包以及子包。 能够可以修改springboot的包扫描
@SpringBootApplication(scanBasePackages ={"com.two.member","com.two.security","com.two.core.config"} )
@MapperScan(basePackages = "com.two.member.dao")
@EnableSwagger2
public class MemberApp {
    public static void main(String[] args) {
        SpringApplication.run(MemberApp.class,args);
    }
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}