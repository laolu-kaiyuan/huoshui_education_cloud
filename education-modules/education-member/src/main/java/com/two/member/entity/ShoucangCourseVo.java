package com.two.member.entity;

import lombok.Data;

/**
 * @date ：Created in 2023/1/10 14:29
 * @description：
 * @modified By：
 * @version:
 */
@Data
public class ShoucangCourseVo {
    private  String courseid;
    private  String memberid;
}
