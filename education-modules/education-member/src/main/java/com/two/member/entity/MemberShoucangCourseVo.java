package com.two.member.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @date ：Created in 2023/1/10 14:44
 * @description：
 * @modified By：
 * @version:
 */
@Data
public class MemberShoucangCourseVo {
    //会员id
    private String id;
    //收藏表
    private  String memberId;

    private  String courseId;
    //收藏
    private  String teacherId;
    private String subjectId;
    private String subjectParentId;
    private Integer lessonNum;
    private Long buyCount;
    private Long viewCount;
    private Long version;
    private Boolean isDeleted;
    private String remark;
    private Integer isAdd;
    private String cover;
    private String title;


    private Double price;
    private String status;
    /**
     * 创建时间
     * **/
    private LocalDateTime gmtCreate;
    /**
     * 更新时间
     * **/
    private LocalDateTime gmtModified;
}
