package ke.chart;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages ={"ke.chart","com.two.security","com.two.core.config"} )
@MapperScan(basePackages = "ke.chart.dao")
@EnableSwagger2
public class ChartApp {
        public static void main(String[] args) {
            SpringApplication.run(ChartApp.class,args);
        }

}
