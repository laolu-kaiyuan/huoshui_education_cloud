package ke.chart.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.EduSubject;

public interface SubjectDao extends BaseMapper<EduSubject> {
}
