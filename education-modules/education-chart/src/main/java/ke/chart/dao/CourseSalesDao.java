package ke.chart.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.vo.Result;
import ke.chart.vo.CourseSalesVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CourseSalesDao extends BaseMapper<CourseSalesVo> {
    @Select("SELECT c.title,b.buy_count,b.price from edu_course b JOIN edu_subject c on b.subject_id=c.id WHERE b.buy_count!=0")
    List<CourseSalesVo> queryAllCourseRanking();
}
