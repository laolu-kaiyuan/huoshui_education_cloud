package ke.chart.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.TbMember;

public interface MemberTypeDao extends BaseMapper<TbMember> {
}
