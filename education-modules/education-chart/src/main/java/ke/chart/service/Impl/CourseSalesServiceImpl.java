package ke.chart.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.vo.Result;

import ke.chart.dao.CourseSalesDao;
import ke.chart.service.CourseSalesService;
import ke.chart.vo.CourseSalesVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseSalesServiceImpl implements CourseSalesService {
    @Autowired
    private CourseSalesDao courseSalesDao;
    @Override
    public Result queryAllCourseRanking() {
        List<CourseSalesVo> courseSalesVos = courseSalesDao.queryAllCourseRanking();
        return new Result(2000,"查询成功",courseSalesVos);
    }
}
