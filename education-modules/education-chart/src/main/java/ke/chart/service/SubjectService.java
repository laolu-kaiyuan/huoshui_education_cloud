package ke.chart.service;

import com.two.core.entity.EduSubject;
import com.two.core.vo.Result;

public interface SubjectService {
    Result<EduSubject> getAllSubject();
}
