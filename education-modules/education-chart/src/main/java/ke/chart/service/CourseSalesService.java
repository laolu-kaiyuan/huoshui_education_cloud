package ke.chart.service;

import com.two.core.vo.Result;
import ke.chart.vo.CourseSalesVo;


public interface CourseSalesService {
    Result<CourseSalesVo> queryAllCourseRanking();
}
