package ke.chart.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.TbMember;
import com.two.core.vo.Result;
import ke.chart.dao.MemberTypeDao;
import ke.chart.service.MemberTypeService;
import ke.chart.vo.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberTypeServiceImpl implements MemberTypeService {
    @Autowired
    private MemberTypeDao memberTypeDao;
    @Override
    public Result<TbMember> queryMemberTypeBang() {
        QueryWrapper<TbMember> wrapper = new QueryWrapper<>();
        List<TbMember> tbMembers = memberTypeDao.selectList(wrapper);
        return new Result(2000,"查询成功",tbMembers);
    }
}
