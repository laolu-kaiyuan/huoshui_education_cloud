package ke.chart.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.EduSubject;
import com.two.core.vo.Result;
import ke.chart.dao.SubjectDao;
import ke.chart.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectServiceImpl implements SubjectService {
    @Autowired
    private SubjectDao subjectDao;
    @Override
    public Result<EduSubject> getAllSubject() {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        List<EduSubject> eduSubjects = subjectDao.selectList(wrapper);
        return new Result(2000,"查询成功",eduSubjects);
    }
}
