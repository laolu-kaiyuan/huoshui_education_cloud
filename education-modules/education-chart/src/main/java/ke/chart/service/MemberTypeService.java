package ke.chart.service;

import com.two.core.entity.TbMember;
import com.two.core.vo.Result;
import ke.chart.vo.MemberVo;

public interface MemberTypeService {
    Result<TbMember> queryMemberTypeBang();
}
