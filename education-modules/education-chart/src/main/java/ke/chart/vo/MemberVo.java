package ke.chart.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(value = "会员表")
public class MemberVo {
    private String id;
    private String openid;
    private String mobile;
    private String password;
    private String nickname;
    private Integer sex;
    private Integer age;
    private String avatar;
    private String sign;
    private Integer isType;
    private Integer isDisabled;
    private Integer isDeleted;
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime gmtCreate;
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime gmtModified;
    private String remark;
    private String salt;
}
