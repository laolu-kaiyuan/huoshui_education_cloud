package ke.chart.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data

public class CourseSalesVo  {
    private String title;
    private String buyCount;
    private BigDecimal price;
}
