package ke.chart.controller;

import com.two.core.entity.EduTeacher;
import com.two.core.entity.TbMember;
import com.two.core.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import ke.chart.service.CourseSalesService;
import ke.chart.service.MemberTypeService;
import ke.chart.service.SubjectService;
import ke.chart.vo.CourseSalesVo;
import ke.chart.vo.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("chart")
@Api(tags = "统计接口类")
public class MemberTypeController {
    @Autowired
    private MemberTypeService memberTypeService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private CourseSalesService courseSalesService;
    @PostMapping("/member/queryMemberTypeBang")
    @ApiOperation(value = "会员统计")
    public Result getTeacherByName(){
        return memberTypeService.queryMemberTypeBang();
    }
    @PostMapping("/subject/getAllSubject")
    @ApiOperation(value = "课程类型统计")
    public Result getAllSubject(){
        return subjectService.getAllSubject();
    }
    @PostMapping("/course/queryAllCourseRanking")
    @ApiOperation(value = "课程销量")
    public Result queryAllCourseRanking(){
        return courseSalesService.queryAllCourseRanking();
    }
}
