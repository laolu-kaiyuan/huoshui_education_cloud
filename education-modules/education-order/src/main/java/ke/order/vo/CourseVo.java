package ke.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "课程表")
public class CourseVo {
    @ApiModelProperty("课程ID")
    private String id;
    @ApiModelProperty("课程讲师ID")
    private String teacherId;
    @ApiModelProperty("课程专业ID")
    private String subjectId;
    @ApiModelProperty("课程专业父级ID")
    private String subjectParentId;
    @ApiModelProperty("课程标题")
    private String title;
    @ApiModelProperty("销售价格")
    private BigDecimal price;
    @ApiModelProperty("总课时")
    private Integer lessonNum;
    @ApiModelProperty("图片路径")
    private String cover;
    @ApiModelProperty("销售数量")
    private Integer buyCount;
    @ApiModelProperty("浏览数量")
    private Integer viewCount;
    @ApiModelProperty("乐观锁")
    private Integer version;
    @ApiModelProperty("课程状态")
    private String status;
    @ApiModelProperty("逻辑删除")
    private Integer isDeleted;
    @ApiModelProperty("创建时间")
    private String gmtCreate;
    @ApiModelProperty("更新时间")
    private String gmtModified;
    @ApiModelProperty("课程备注")
    private String remark;
    @ApiModelProperty("是否添加")
    private Integer isAdd;
}
