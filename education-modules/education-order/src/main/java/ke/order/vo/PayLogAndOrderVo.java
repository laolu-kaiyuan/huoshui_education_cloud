package ke.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "订单和支付日志")
public class PayLogAndOrderVo {
    @ApiModelProperty(value = "订单编号")
    private String orderNo;
    @ApiModelProperty(value = "支付时间")
    private String payTime;
    @ApiModelProperty(value = "支付金额")
    private BigDecimal totalFee;
    @ApiModelProperty(value = "交易流水号")
    private String transactionId;
    @ApiModelProperty(value = "支付类型")
    private Integer payType;
    @ApiModelProperty(value = "创建时间")
    private String gmtCreate;
    @ApiModelProperty(value = "课程名称")
    private String courseTitle;
    @ApiModelProperty(value = "讲师姓名")
    private String teacherName;
    @ApiModelProperty(value = "会员ID")
    private String memberId;
}
