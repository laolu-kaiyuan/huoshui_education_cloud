package ke.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel(value = "讲师信息")
public class TeacherVo {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "讲师姓名")
    private String name;
    @ApiModelProperty(value = "讲师简介")
    private String intro;
    @ApiModelProperty(value = "讲师资历")
    private String career;
    @ApiModelProperty(value = "头衔")
    private Integer level;
    @ApiModelProperty(value = "讲师头像")
    private String avatar;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "逻辑删除")
    private Integer isDeleted;
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;
    @ApiModelProperty(value = "更改时间")
    private LocalDateTime gmtModified;
    @ApiModelProperty(value = "讲师手机")
    private String mobile;
    @ApiModelProperty(value = "讲师邮箱")
    private String email;
    @ApiModelProperty(value = "讲师状态")
    private String status;
}
