package ke.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "订单对象")
public class OrderVo {
    @ApiModelProperty(value = "序号")
    private String id;
    @ApiModelProperty(value = "订单号")
    private String orderNo;
    @ApiModelProperty(value = "课程ID")
    private String courseId;
    @ApiModelProperty(value = "课程名称")
    private String courseTitle;
    @ApiModelProperty(value = "课程封面")
    private String courseCover;
    @ApiModelProperty(value = "讲师姓名")
    private String teacherName;
    @ApiModelProperty(value = "会员ID")
    private String memberId;
    @ApiModelProperty(value = "会员昵称")
    private String nickname;
    @ApiModelProperty(value = "会员手机")
    private String mobile;
    @ApiModelProperty(value = "订单金额")
    private BigDecimal totalFee;
    @ApiModelProperty(value = "支付类型")
    private Integer payType;
    @ApiModelProperty(value = "订单状态")
    private Integer status;
    @ApiModelProperty(value = "逻辑删除")
    private Integer isDeleted;
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime gmtModified;

}
