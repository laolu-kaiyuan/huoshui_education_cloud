package ke.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.EduCourse;

public interface CourseDao extends BaseMapper<EduCourse> {
}
