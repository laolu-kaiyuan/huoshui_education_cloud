package ke.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.vo.Result;
import ke.order.vo.PayLogAndOrderVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PayAndOrderDao extends BaseMapper<PayAndOrderDao> {
    @Select("SELECT a.member_id,a.order_no,a.course_title,a.teacher_name,a.pay_type,a.gmt_create,b.transaction_id,b.pay_time,b.total_fee from t_order a JOIN t_pay_log b on a.order_no=b.order_no WHERE a.order_no =#{orderNo}")
    List<PayLogAndOrderVo> queryOrderAndPayLog(String orderNo);
}
