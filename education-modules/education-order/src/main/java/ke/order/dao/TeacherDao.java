package ke.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.EduTeacher;

public interface TeacherDao extends BaseMapper<EduTeacher> {
}
