package ke.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.TOrder;
import com.two.core.vo.Result;
import ke.order.vo.OrderVo;
import org.apache.ibatis.annotations.Select;


public interface OrderDao extends BaseMapper<TOrder> {

}
