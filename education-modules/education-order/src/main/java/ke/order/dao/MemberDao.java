package ke.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.TbMember;

public interface MemberDao extends BaseMapper<TbMember> {
}
