package ke.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.*;
import com.two.core.entity.EduCourse;
import com.two.core.entity.EduTeacher;
import com.two.core.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import ke.order.service.*;
import ke.order.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("order")
@Api(tags = "订单管理接口类")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private PayLogAndOrderService payLogAndOrderService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private MemberService memberService;
    @PostMapping("/order/getAllOrder/{current}/{pageSize}")
    @ApiOperation(value = "分页+条件查询订单")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "页码",name = "current"),
                    @ApiImplicitParam(value = "每页显示",name = "pageSize"),
                    @ApiImplicitParam(value = "接参",name = "orderVo")
            }
    )
    public Result<IPage<TOrder>> getUserByPage(@PathVariable(required = true) Integer current, @PathVariable(required =true ) Integer pageSize,
                                               @RequestBody OrderVo orderVo) {
        return orderService.getAllOrder(current,pageSize,orderVo);
    }

    @PostMapping("/order/queryOrderAndPayLog/{orderNo}")
    @ApiOperation(value = "查询订单+支付日志")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "接参",name = "orderNo")
            }
    )
    public Result queryOrderAndPayLog(@PathVariable String orderNo) {
        return payLogAndOrderService.queryOrderAndPayLog(orderNo);
    }

    @PostMapping("/course/getCourseById/{id}")
    @ApiOperation(value = "查询课程")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "课程ID",name = "id")
            }
    )
    public Result<EduCourse> getCourseById(@PathVariable String id){
        return courseService.getCourseById(id);
    }


    @PostMapping("/teacher/getTeacherByName/{teacherName}")
    @ApiOperation(value = "查询讲师")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "讲师姓名",name = "teacherName")
            }
    )
    public Result<EduTeacher> getTeacherByName(@PathVariable String teacherName){
        return teacherService.getTeacherByName(teacherName);
    }

    @PostMapping("/member/getById/{id}")
    @ApiOperation(value = "查询会员")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(value = "会员编号",name = "id")
            }
    )
    public Result<TbMember> getById(@PathVariable String id){
        return memberService.getById(id);
    }

}
