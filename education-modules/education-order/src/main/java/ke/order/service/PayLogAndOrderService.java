package ke.order.service;

import com.two.core.vo.Result;
import ke.order.vo.PayLogAndOrderVo;

public interface PayLogAndOrderService {
    Result queryOrderAndPayLog(String orderNo);
}
