package ke.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.EduTeacher;
import com.two.core.entity.TbMember;
import com.two.core.vo.Result;
import ke.order.dao.MemberDao;
import ke.order.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberDao memberDao;
    @Override
    public Result<TbMember> getById(String id) {
        QueryWrapper<TbMember> wrapper = new QueryWrapper<>();
        wrapper.like("id",id);
        TbMember tbMember = memberDao.selectOne(wrapper);
        return new Result(2000,"查询成功",tbMember);
    }
}
