package ke.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.EduTeacher;
import com.two.core.vo.Result;
import ke.order.dao.TeacherDao;
import ke.order.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherDao teacherDao;
    @Override
    public Result<EduTeacher> getTeacherByName(String teacherName) {
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        wrapper.like("name",teacherName);
        EduTeacher eduTeacher = teacherDao.selectOne(wrapper);
        return new Result(2000,"查询成功",eduTeacher);
    }
}
