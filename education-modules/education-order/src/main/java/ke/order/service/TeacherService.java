package ke.order.service;

import com.two.core.entity.EduTeacher;
import com.two.core.vo.Result;

public interface TeacherService {
    Result<EduTeacher> getTeacherByName(String teacherName);
}
