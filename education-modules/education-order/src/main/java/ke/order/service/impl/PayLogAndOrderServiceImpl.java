package ke.order.service.impl;

import com.two.core.vo.Result;
import ke.order.dao.PayAndOrderDao;
import ke.order.service.PayLogAndOrderService;
import ke.order.vo.PayLogAndOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PayLogAndOrderServiceImpl implements PayLogAndOrderService {
    @Autowired
    private PayAndOrderDao payAndOrderDao;
    @Override
    public Result queryOrderAndPayLog(String orderNo) {
        List<PayLogAndOrderVo> payLogAndOrderVos = payAndOrderDao.queryOrderAndPayLog(orderNo);
        return new Result(2000,"查询成功",payLogAndOrderVos);
    }
}
