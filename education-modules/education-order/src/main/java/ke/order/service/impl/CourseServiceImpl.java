package ke.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.EduCourse;
import com.two.core.entity.TOrder;
import com.two.core.vo.Result;
import ke.order.dao.CourseDao;
import ke.order.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseDao courseDao;
    @Override
    public Result<EduCourse> getCourseById(String id) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.like("id",id);
        List<EduCourse> eduCourses = courseDao.selectList(wrapper);
        return new Result(2000,"查询成功",eduCourses);
    }
}
