package ke.order.service;

import com.two.core.entity.TbMember;
import com.two.core.vo.Result;

public interface MemberService {
    Result<TbMember> getById(String id);
}
