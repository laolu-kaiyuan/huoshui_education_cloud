package ke.order.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.EduCourse;
import com.two.core.vo.Result;

public interface CourseService {
    Result<EduCourse> getCourseById(String id);
}
