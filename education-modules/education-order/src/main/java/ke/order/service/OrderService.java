package ke.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.TOrder;
import com.two.core.vo.Result;
import ke.order.vo.OrderVo;


import java.util.List;

public interface OrderService {
    //分页+条件查询订单表
    Result<IPage<TOrder>> getAllOrder(Integer current, Integer pageSize, OrderVo orderVo);
    Result<TOrder> getByOrderNo(String orderNo);
}
