package ke.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.TOrder;
import com.two.core.vo.Result;
import ke.order.dao.OrderDao;
import ke.order.service.OrderService;
import ke.order.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;
    @Override
    public Result<IPage<TOrder>> getAllOrder(Integer current, Integer pageSize, OrderVo orderVo) {
        IPage<TOrder> page = new Page(current, pageSize);
        QueryWrapper<TOrder> wrapper = new QueryWrapper<>();
        if(StringUtils.hasText(orderVo.getMobile())){
            wrapper.like("mobile",orderVo.getMobile());
        }
        if(StringUtils.hasText(orderVo.getOrderNo())){
            wrapper.like("orderNo",orderVo.getOrderNo());
        }
        if(StringUtils.hasText(orderVo.getCourseTitle())){
            wrapper.like("course_title",orderVo.getCourseTitle());
        }
        if((orderVo.getGmtCreate()!=null)){
            wrapper.ge("gmt_create",orderVo.getGmtCreate());
        }
        if(orderVo.getGmtModified()!=null){
            wrapper.le("gmt_modified",orderVo.getGmtModified());
        }
        if(orderVo.getStatus()!=null){
            wrapper.like("status",orderVo.getStatus());
        }
        if(StringUtils.hasText(orderVo.getTeacherName())){
            wrapper.like("teacher_name",orderVo.getTeacherName());
        }
        if(orderVo.getPayType()!=null){
            wrapper.like("pay_type",orderVo.getPayType());
        }
        IPage<TOrder> i = orderDao.selectPage(page, wrapper);
        return new Result<>(2000,"查询成功",i);
    }

    @Override
    public Result<TOrder> getByOrderNo(String orderNo) {
        QueryWrapper<TOrder> wrapper = new QueryWrapper<>();
        wrapper.like("order_no",orderNo);
        List<TOrder> tOrders = orderDao.selectList(wrapper);
        return new Result(2000,"查询成功",tOrders);
    }
}
