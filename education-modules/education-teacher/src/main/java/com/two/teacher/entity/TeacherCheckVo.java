package com.two.teacher.entity;

import lombok.Data;

/**
 * @date ：Created in 2022/12/9 15:49
 * @description：讲师审核
 * @modified By：
 * @version:
 */
@Data
public class TeacherCheckVo {
    //讲师名称
    private String name;
    //手机号
    private  String mobile;
    //状态
    private  String status;
}
