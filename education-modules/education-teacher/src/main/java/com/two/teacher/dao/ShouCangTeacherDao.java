package com.two.teacher.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.teacher.entity.TbShoucangTeacher;

/**
 * @date ：Created in 2023/1/2 12:15
 * @description： 取消关注
 * @modified By：
 * @version:
 */
public interface ShouCangTeacherDao extends BaseMapper<TbShoucangTeacher> {
}
