package com.two.teacher.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.AclCourse;

/**

 * @date ：Created in 2023/1/8 13:20
 * @description：
 * @modified By：
 * @version:
 */
public interface TeacherXinxDao extends BaseMapper<AclCourse> {

}
