package com.two.teacher.entity;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 讲师(Teacher)实体类
 *
 * @author makejava
 * @since 2022-12-08 12:39:26
 */
@Data
@TableName("edu_teacher")
public class EduTeacher implements Serializable {

    private static final long serialVersionUID = 332840954001614679L;
    /**
     * 讲师ID
     */
    @TableId
    private String id;
    /**
     * 讲师姓名
     */
    private String name;
    /**
     * 讲师简介
     */
    private String intro;
    /**
     * 讲师资历,一句话说明讲师
     */
    private String career;
    /**
     * 头衔 0高级讲师 1首席讲师
     */
    private String level;
    /**
     * 讲师头像
     */
    private String avatar;
    /**
     * 排序
     */
    private String sort;
    /**
     * 逻辑删除 1（true）已删除， 0（false）未删除
     */
    private String isDeleted;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
    /**
     * 讲师手机
     */
    private String mobile;
    /**
     * 讲师邮箱
     */
    private String email;
    /**
     * 讲师状态  0 正常 ，1 禁用
     */
    private String status;


}

