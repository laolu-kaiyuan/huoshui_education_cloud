package com.two.teacher.entity;

import lombok.Data;

import java.util.Date;

/**
 * @date ：Created in 2022/12/9 17:10
 * @description：连表
 * @modified By：
 * @version:
 */
@Data
public class TeacherAndCheckVo {
 //讲师审核表
 private Integer teacherCheckId;


 private String checkStatus;

 private Integer checkUserId;

 private String checkContent;

 private Date checkTime;


 //讲师表
 private String id;
 /**
  * 讲师姓名
  */
 private String name;
 /**
  * 讲师简介
  */
 private String intro;
 /**
  * 讲师资历,一句话说明讲师
  */
 private String career;
 /**
  * 头衔 0高级讲师 1首席讲师
  */
 private String level;
 /**
  * 讲师头像
  */
 private String avatar;
 /**
  * 排序
  */
 private String sort;
 /**
  * 逻辑删除 1（true）已删除， 0（false）未删除
  */
 private String isDeleted;
 /**
  * 创建时间
  */
 private Date gmtCreate;
 /**
  * 更新时间
  */
 private Date gmtModified;
 /**
  * 讲师手机
  */
 private String mobile;
 /**
  * 讲师邮箱
  */
 private String email;
 /**
  * 讲师状态  0 正常 ，1 禁用
  */
 private String status;

 //收藏表
 private  String memberId;

 private  String teacherId;

}
