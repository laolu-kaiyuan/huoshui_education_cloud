package com.two.teacher.service.impl;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.teacher.entity.EduTeacher;
import com.two.core.vo.Result;
import com.two.teacher.dao.TeacherDao;
import com.two.teacher.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 讲师(Teacher)表服务实现类
 * @author makejava
 * @since 2022-12-08 12:39:27
 */
@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherDao teacherDao;

    //如果使用mp分页 需要配置分页拦截器
    @Override
    public Result<IPage<EduTeacher>> findByConditionPage(Integer current, Integer pageSize, EduTeacher teacher) {
        IPage<EduTeacher> page = new Page(current, pageSize);

        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        if (StringUtils.hasText(teacher.getName())) {
            wrapper.like("name", teacher.getName());
        }

        if (teacher.getIsDeleted() != null) {
            wrapper.le("is_deleted", teacher.getIsDeleted());
        }

        IPage<EduTeacher> iPage = teacherDao.selectPage(page, wrapper);


        return new Result<>(2000, "查询成功", iPage);
    }


    /**
     * @return
     * @create by: Teacher
     * @description: 修改状态
     * @create time:
     */
    @Override
    public Result<EduTeacher> changeStatus(EduTeacher eduTeacher) {
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        if (StringUtils.hasText(eduTeacher.getId())) {
            wrapper.eq("id", eduTeacher.getId());
        }
        EduTeacher eduTeacher1 = new EduTeacher();
        eduTeacher1.setStatus(eduTeacher.getStatus());
        teacherDao.update(eduTeacher1, wrapper);
        return new Result<>(200, "修改成功", eduTeacher1);
    }

    /**
     * @return
     * @create by: Teacher马
     * @description: 修改信息
     * @create time:
     */
    @Override
    public Result<EduTeacher> updateTeacher(EduTeacher eduTeacher) {
        EduTeacher eduTeacher1 = new EduTeacher();
        eduTeacher1.setName(eduTeacher.getName());
        eduTeacher1.setMobile(eduTeacher.getMobile());
        eduTeacher1.setEmail(eduTeacher.getEmail());
        eduTeacher1.setIntro(eduTeacher.getIntro());

        UpdateWrapper<EduTeacher> updateWrapper = new UpdateWrapper<>();
        if (StringUtils.hasText(eduTeacher.getId())) {
            updateWrapper.eq("id", eduTeacher.getId());
        }
        teacherDao.update(eduTeacher1, updateWrapper);
        return new Result<>(200, "修改成功", eduTeacher1);
    }

    /**
     * @return
     * @description: 查询所有关注的讲师信息
     * @create time:
     */
    @Override
    public Result findTeacherById(String memberId) {
        List<EduTeacher> teacherById = teacherDao.findTeacherById(memberId);
        return new Result(200, "成功", teacherById);
    }

    /**
     * @create by: Teacher马
     * @description:  根据手机号查询教师的个人信息
     * @create time:
     * @return
     */
    @Override
    public Result findTeacherBYMobile(String mobile) {
        QueryWrapper<EduTeacher> wrapper=new QueryWrapper<>();
        if (StringUtils.hasText(mobile)){
            wrapper.eq("mobile",mobile);
        }
        EduTeacher eduTeacher = teacherDao.selectOne(wrapper);
        return new Result(200,"成功",eduTeacher);
    }

    /**
     * @create by: Teacher马
     * @description: 根据教师id获取讲师课程信息
     * @create time:

     * @return
     */
    @Override
    public Result findTeacherByTeacherId(String teacherId) {
        QueryWrapper<EduTeacher> queryWrapper=new QueryWrapper<>();
        if (StringUtils.hasText(teacherId)){
            queryWrapper.eq("id",teacherId);
        }
        EduTeacher eduTeacher = teacherDao.selectOne(queryWrapper);
        return new Result(200,"成功",eduTeacher);
    }
    /**
     * @create by: Teacher马
     * @description:  根据手机号修改讲师的个人信息
     * @create time:
     * @return
     */
    @Override
    public Result updateTeacherBYMobile(EduTeacher eduTeacher) {
        EduTeacher eduTeacher1=new EduTeacher();
        eduTeacher1.setAvatar(eduTeacher.getAvatar());
        eduTeacher1.setName(eduTeacher.getName());
        eduTeacher1.setIntro(eduTeacher.getIntro());
        UpdateWrapper<EduTeacher> wrapper=new UpdateWrapper<>();
        if (StringUtils.hasText(eduTeacher.getMobile())){
            wrapper.eq("mobile",eduTeacher.getMobile());
        }
        teacherDao.update(eduTeacher1,wrapper);
        return new Result(200,"修改成功",eduTeacher1);
    }
    /**
     * @create by: Teacher马
     * @description:  前端添加讲师
     * @create time:
     * @return
     */
    @Override
    public Result qianaddTeacher(EduTeacher eduTeacher) {
        EduTeacher eduTeacher1=new EduTeacher();
        eduTeacher1.setAvatar(eduTeacher.getAvatar());
        eduTeacher1.setName(eduTeacher.getName());
        eduTeacher1.setEmail(eduTeacher.getEmail());
        eduTeacher1.setIntro(eduTeacher.getIntro());
        eduTeacher1.setMobile(eduTeacher.getMobile());
        eduTeacher1.setLevel(eduTeacher.getLevel());
        eduTeacher1.setGmtCreate(DateTime.now());
        eduTeacher1.setGmtModified(DateTime.now());
        int insert = teacherDao.insert(eduTeacher);
        if (insert>0){
            return new Result(200,"添加成功");
        }
        return new Result(500,"失败");
    }
}
