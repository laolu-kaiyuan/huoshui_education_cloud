package com.two.teacher.service.impl;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.two.core.vo.Result;
import com.two.teacher.dao.ShouCangTeacherDao;
import com.two.teacher.entity.ShouCangTeacherVo;
import com.two.teacher.entity.TbShoucangTeacher;
import com.two.teacher.service.ShouCangTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ：Teacher马（2508531326@qq.com）
 * @date ：Created in 2023/1/2 12:21
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class ShouCangTeacherServiceImpl implements ShouCangTeacherService {
    @Autowired
    ShouCangTeacherDao shouCangTeacherDao;
    /**
     * @create by: Teacher马
     * @description: 取消关注讲师
     * @create time:  
     a
     * @return 
    */
    @Override
    public Result deleteAttentionTeacher(ShouCangTeacherVo shouCangTeacherVo) {
        UpdateWrapper<TbShoucangTeacher> wrapper=new UpdateWrapper<>();
        wrapper.eq("member_id",shouCangTeacherVo.getMemberid());
        wrapper.eq("teacher_id",shouCangTeacherVo.getTeacherid());
        int delete = shouCangTeacherDao.delete(wrapper);
        if (delete>0){
            return new Result(200,"取消成功",delete);
        }


        return new Result(500,"取消失败");
    }

}
