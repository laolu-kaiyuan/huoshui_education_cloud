package com.two.teacher.entity;

import lombok.Data;

/**
 * @date ：Created in 2023/1/2 12:14
 * @description：取消关注
 * @modified By：
 * @version:
 */
@Data
public class ShouCangTeacherVo {
    private  String memberid;
    private String teacherid;
}
