package com.two.teacher.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.AclCourse;
import com.two.core.vo.Result;

/**
 * @date ：Created in 2023/1/8 13:21
 * @description：
 * @modified By：
 * @version:
 */
public interface TeacherXinxService {
    Result<Page<AclCourse>> findCourseByTeacherId(Integer pageCurrent, Integer pageSize, String teacherId, String courseName);



}