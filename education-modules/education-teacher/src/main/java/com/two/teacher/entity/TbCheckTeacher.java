package com.two.teacher.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("tb_check_teacher")
public class TbCheckTeacher implements Serializable {
    /**
     * 测试审核ID
     */
    @TableId
    private String teacherCheckId;
    /**
     * 测试讲师ID
     */
    private String teacherId;
    /**
     * 测试讲师状态  0 正常 ，1 禁用
     */
    private String checkStatus;

    private String checkUserId;
    /**
     * 测试讲师姓名
     */
    private String checkContent;
    /**
     * 测试时间
     */
    private Date checkTime;
}
