package com.two.teacher.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.teacher.entity.EduTeacher;

import java.util.List;

/**
 * 讲师(Teacher)表数据库访问层
 *
 * @author makejava
 * @since 2022-12-08 12:39:26
 */
public interface TeacherDao extends BaseMapper<EduTeacher> {
    List<EduTeacher> findTeacherById(String memberId);

}
