package com.two.teacher.service;

import com.two.teacher.entity.TbCheckTeacher;
import com.two.core.vo.Result;
import com.two.teacher.entity.TeacherCheckVo;

/**
 * @date ：Created in 2022/12/9 16:38
 * @description：
 * @modified By：
 * @version:
 */
public interface TeacherCheckService {
    Result findAll(Integer current, Integer pageSize, TeacherCheckVo teacherCheckVo);
    Result<TbCheckTeacher>  changeCheckStatus(TbCheckTeacher tbCheckTeacher);
}
