package com.two.teacher.controller;



import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.AclCourse;
import com.two.core.vo.Result;
import com.two.teacher.service.TeacherService;
import com.two.teacher.service.TeacherXinxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @date ：Created in 2023/1/8 12:55
 * @description： 进入主页 获取教师课程信息
 * @modified By：
 * @version:
 */

@RestController
@RequestMapping("/teacher/course")
@Api(tags = "进入主页")
public class TeacherXinXController {
 @Autowired
 private TeacherXinxService teacherXinxService;

 @Autowired
 private TeacherService teacherService;
    /**
     * @description: 获取讲师课程信息 分页
     * @create time:
     * @return
     */
    @ApiOperation(value = "获取讲师课程信息 分页")
    @PostMapping("findCourseByTeacherId/{pageCurrent}/{pageSize}")
    public Result<Page<AclCourse>> findCourseByTeacherId(@PathVariable Integer pageCurrent, @PathVariable Integer pageSize, AclCourse aclCourse){
        return teacherXinxService.findCourseByTeacherId(pageCurrent,pageSize, aclCourse.getTeacherId(), aclCourse.getTitle());

    }
    /**
     * @description: 根据教师id获取讲师课程信息
     * @create time:
     * @return
     */
    @ApiOperation(value = "根据教师id获取讲师课程信息")
    @GetMapping("findTeacherByTeacherId/{teacherId}")
    public Result findTeacherByTeacherId(@PathVariable String teacherId){
        System.out.println(teacherId+"---------------------");
        return teacherService.findTeacherByTeacherId(teacherId);
    }
}
