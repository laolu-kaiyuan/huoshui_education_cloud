package com.two.teacher.service;


import com.two.core.vo.Result;
import com.two.teacher.entity.ShouCangTeacherVo;

/**
 * @date ：Created in 2023/1/2 12:17
 * @description：
 * @modified By：
 * @version:
 */
public interface ShouCangTeacherService {
    Result deleteAttentionTeacher(ShouCangTeacherVo shouCangTeacherVo);


}
