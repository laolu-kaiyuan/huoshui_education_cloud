package com.two.teacher.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.teacher.entity.EduTeacher;
import com.two.teacher.entity.ShouCangTeacherVo;
import com.two.teacher.service.ShouCangTeacherService;
import com.two.teacher.service.TeacherService;
import com.two.core.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 讲师(Teacher)表控制层
 * @author makejava
 * @since 2022-12-08 12:39:25
 */
@RestController
@RequestMapping("teacher")
@Api(tags = "讲师管理")
public class TeacherController {
    /**
     * 服务对象
     */
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private ShouCangTeacherService shouCangTeacherService;

    /**
     * 分页查询
     */
    @ApiOperation(value = "dadada")
    @PostMapping("findTeacher/{current}/{pageSize}")
    public Result<IPage<EduTeacher>> getUserByPage(@PathVariable(required = true) Integer current,
                                                   @PathVariable(required =true ) Integer pageSize,
                                                   @RequestBody EduTeacher teacher
    ){
        return teacherService.findByConditionPage(current,pageSize,teacher);
    }

    @ApiOperation(value = "修改状态")
    @PostMapping("changeStatus")
    public Result<EduTeacher> changeStatus(@RequestBody EduTeacher eduTeacher){
        return teacherService.changeStatus(eduTeacher);
    }



    @ApiOperation(value = "修改信息")
    @PostMapping("updateTeacher")
    public Result<EduTeacher> changeTeacher(@RequestBody EduTeacher eduTeacher){
        return teacherService.updateTeacher(eduTeacher);
    }
    /**
     * @description: 查询所有关注的讲师信息
     * @create time:
     * @return
     */
    @ApiOperation(value = "查询所有关注的讲师信息")
    @PostMapping("findAttentionTeacherById/{memberId}")
    public Result findAttentionTeacherById(@PathVariable String memberId){
        System.out.println(memberId+"memberId++++++++++");
        return teacherService.findTeacherById(memberId);
    }

    /**
     * @description: 取消关注
     * @create time:

     * @return
     */
    @ApiOperation(value = "取消关注")
    @PostMapping("deleteAttentionTeacher")
    public Result deleteAttentionTeacher(@RequestBody ShouCangTeacherVo shouCangTeacherVo){
        System.out.println(shouCangTeacherVo+"------------------------");
        return shouCangTeacherService.deleteAttentionTeacher(shouCangTeacherVo);
    }

    /**
     * @description: 根据手机号查询教师的个人信息
     * @create time:
     * @return
     */
    @ApiOperation(value = "根据手机号查询教师的个人信息")
    @GetMapping("findTeacherBYMobile/{mobile}")
    public Result findTeacherBYMobile(@PathVariable String mobile){
        return teacherService.findTeacherBYMobile(mobile);
    }
    /**
     * @description: 根据手机号修改讲师的个人信息
     * @create time:
     * @return
     */
    @ApiOperation(value = "根据手机号修改讲师的个人信息")
    @PostMapping("updateTeacherBYMobile")
    public Result updateTeacherBYMobile(@RequestBody EduTeacher eduTeacher){

        return teacherService.updateTeacherBYMobile(eduTeacher);
    }
    /**
     * @create by: Teacher马
     * @description: 前端添加讲师
     * @create time:
     * @return
     */
    @ApiOperation(value = "前端添加讲师")
    @PostMapping("qianaddTeacher")
    public Result qianaddTeacher(@RequestBody EduTeacher eduTeacher){
        System.out.println(eduTeacher+"111111111111111111111111111111111");
        return teacherService.qianaddTeacher(eduTeacher);
    }
}

