package com.two.teacher;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//springboot默认扫描的主类所在的包以及子包。 能够可以修改springboot的包扫描
@SpringBootApplication(scanBasePackages ={"com.two.teacher","com.two.security","com.two.core.config","com.two.member"} )
@MapperScan(basePackages = "com.two.teacher.dao")
@EnableSwagger2
public class TeacherApp {
    public static void main(String[] args) {
        SpringApplication.run(TeacherApp.class,args);
    }
}