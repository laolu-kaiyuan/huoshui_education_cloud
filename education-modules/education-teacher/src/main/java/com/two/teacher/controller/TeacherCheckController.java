package com.two.teacher.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.teacher.entity.EduTeacher;
import com.two.teacher.entity.TbCheckTeacher;
import com.two.core.vo.Result;
import com.two.teacher.entity.TeacherAndCheckVo;
import com.two.teacher.service.TeacherCheckService;
import com.two.teacher.service.TeacherService;
import com.two.teacher.entity.TeacherCheckVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @date ：Created in 2022/12/9 16:45
 * @description：
 * @modified By：
 * @version:
 */

@RestController
@RequestMapping("/teacher/checkTeacher")
@Api(tags = "教师审核")
public class TeacherCheckController {
    @Autowired
    private TeacherCheckService teacherCheckService;
    @Autowired
    private TeacherService teacherService;
    /**
     * @description: 分页查询
     * @create time:

     * @return
     */
    @ApiOperation(value = "分页查询")
    @PostMapping("findTeacherCheck/{current}/{pageSize}")
    public Result <IPage<TeacherAndCheckVo>> findTeacherCheck(@PathVariable(required = true) Integer current,
                                                              @PathVariable(required =true ) Integer pageSize,
                                                              @RequestBody TeacherCheckVo teacherCheckVo
    ){
        return teacherCheckService.findAll(current,pageSize,teacherCheckVo);
    }
        /**
         * @description: 审核信息
         * @create time:
         * @return
        */

    @PostMapping("changeTeacherCheck")
    @ApiOperation(value = "审核信息")
    public Result changeTeacherCheck(@RequestBody TbCheckTeacher tbCheckTeacher){
        System.out.println(tbCheckTeacher+"-------------");
        return teacherCheckService.changeCheckStatus(tbCheckTeacher);
    }


    @ApiOperation(value = "修改状态")
    @PostMapping("changeStatus")
    public Result<EduTeacher> changeStatus(@RequestBody EduTeacher eduTeacher){
        return teacherService.changeStatus(eduTeacher);
    }

    @ApiOperation(value = "修改信息")
    @PostMapping("updateTeacher")
    public Result<EduTeacher> changeTeacher(@RequestBody EduTeacher eduTeacher){
        return teacherService.updateTeacher(eduTeacher);
    }
}
