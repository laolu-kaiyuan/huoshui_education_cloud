package com.two.teacher.dao;

import com.two.teacher.entity.TeacherAndCheckVo;
import com.two.teacher.entity.TeacherCheckVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.teacher.entity.TbCheckTeacher;


public interface TeacherCheckDao extends  BaseMapper<TbCheckTeacher>{

     IPage<TeacherAndCheckVo> findAll(IPage page, TeacherCheckVo teacherCheckVo);

}
