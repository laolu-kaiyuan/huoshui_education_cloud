package com.two.teacher.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (TbShoucangTeacher)实体类
 *
 * @author makejava
 * @since 2023-01-02 12:12:31
 */
@Data
public class TbShoucangTeacher implements Serializable {
    private static final long serialVersionUID = 190305449233602609L;
    
    private String id;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 教师id
     */
    private String teacherId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

}

