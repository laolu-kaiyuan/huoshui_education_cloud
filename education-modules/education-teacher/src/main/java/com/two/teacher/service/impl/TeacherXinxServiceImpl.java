package com.two.teacher.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.AclCourse;
import com.two.core.vo.Result;
import com.two.teacher.dao.TeacherXinxDao;
import com.two.teacher.service.TeacherXinxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author ：Teacher马（2508531326@qq.com）
 * @date ：Created in 2023/1/8 13:24
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class TeacherXinxServiceImpl implements TeacherXinxService {
    @Autowired
    private TeacherXinxDao teacherXinxDao;
    /**
     * @create by: Teacher马
     * @description: 分页查询 教师课程信息
     * @create time:  
     
     * @return 
    */
    @Override
    public Result<Page<AclCourse>> findCourseByTeacherId(Integer pageCurrent, Integer pageSize, String teacherId, String courseName) {
        Page<AclCourse>  page=new Page<>(pageCurrent,pageSize);
        QueryWrapper<AclCourse> queryWrapper=new QueryWrapper<>();

        if (StringUtils.hasText(teacherId)){
            queryWrapper.eq("teacher_id",teacherId);
        }

        if (StringUtils.hasText(courseName)){
            queryWrapper.like("title",courseName);
        }


        Page<AclCourse> page1 = teacherXinxDao.selectPage(page, queryWrapper);
        return new Result<>(200,"成功",page1);
    }



}
