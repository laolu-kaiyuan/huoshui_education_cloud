package com.two.teacher.service.impl;

import com.two.teacher.entity.TbCheckTeacher;
import com.two.core.vo.Result;
import com.two.teacher.dao.TeacherCheckDao;
import com.two.teacher.service.TeacherCheckService;
import com.two.teacher.entity.TeacherAndCheckVo;
import com.two.teacher.entity.TeacherCheckVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @date ：Created in 2022/12/9 16:40
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class TeacherCheckServiceImpl implements TeacherCheckService {
@Autowired
private TeacherCheckDao teacherCheckDao;
    /**
     * @description:分页查询
     * @create time:

     * @return
     */
    @Override
    public Result findAll(Integer current, Integer pageSize, TeacherCheckVo teacherCheckVo) {
        IPage<TeacherAndCheckVo> page=new Page(current,pageSize);
        IPage<TeacherAndCheckVo> all = teacherCheckDao.findAll(page, teacherCheckVo);
        return new Result<>(2000,"查询成功",all);
    }

    /**
     * @description: 审核
     * @create time:
     * @return
    */
    @Override
    public Result<TbCheckTeacher>  changeCheckStatus(TbCheckTeacher tbCheckTeacher) {

        QueryWrapper<TbCheckTeacher> wrapper=new QueryWrapper<>();
        if (StringUtils.hasText(tbCheckTeacher.getTeacherId())){
            wrapper.eq("teacher_id",tbCheckTeacher.getTeacherId());
        }
        TbCheckTeacher tbCheckTeacher1=new TbCheckTeacher();
        tbCheckTeacher1.setCheckStatus(tbCheckTeacher.getCheckStatus());
          teacherCheckDao.update(tbCheckTeacher1,wrapper);
        return new Result<>(2000,"修改成功",tbCheckTeacher1);
    }
}
