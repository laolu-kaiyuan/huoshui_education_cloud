package com.two.teacher.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.teacher.entity.EduTeacher;
import com.two.core.vo.Result;


/**
 * 讲师(Teacher)表服务接口
 *
 * @author makejava
 * @since 2022-12-08 12:39:27
 */
public interface TeacherService {

    /**
     * 根据条件分页查询用户列表
     * @param current 页码
     * @param pageSize 每页显示的条数
     * @param teacher 条件类
     * @return
     */
    Result<IPage<EduTeacher>> findByConditionPage(Integer current, Integer pageSize, EduTeacher teacher);
    Result<EduTeacher>  changeStatus(EduTeacher eduTeacher);
    Result<EduTeacher>  updateTeacher(EduTeacher eduTeacher);

    Result findTeacherById(String memberId);

    Result findTeacherBYMobile(String mobile);

    Result findTeacherByTeacherId(String teacherId);

    Result updateTeacherBYMobile(EduTeacher eduTeacher);

    Result qianaddTeacher(EduTeacher eduTeacher);
}
