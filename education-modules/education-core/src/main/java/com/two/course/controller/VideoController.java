package com.two.course.controller;


import com.two.core.entity.course.EduVideo;
import com.two.core.vo.Result;
import com.two.course.service.VideoService;
import com.two.course.vo.reception.AddVideoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 11:47
 */
@RestController
@RequestMapping("/core/video")
@Api(tags = "课时管理接口")
public class VideoController {
    @Autowired
    private VideoService videoService;

    @PostMapping("/updateVideo")
    @ApiOperation(value = "修改对应的课程的章节的课时")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "修改课时的信息",name = "addVideoVo")
    })
    public Result<String> updateVideo(@RequestBody AddVideoVo addVideoVo){
        return videoService.updateVideo(addVideoVo);
    }

    @DeleteMapping("/deleteById/{id}/{sort}/{chapterId}")
    @ApiOperation(value = "删除对应的课程的章节的课时")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "课时id",name = "id"),
            @ApiImplicitParam(value = "课时排序信息",name = "sort"),
            @ApiImplicitParam(value = "章节id",name = "chapterId")
    })
    public Result<String> deleteById(
            @PathVariable("id") String id,
            @PathVariable("sort") Integer sort,
            @PathVariable("chapterId") String chapterId
    ){
        return videoService.deleteByIds(id,sort,chapterId);
    }


    @PutMapping("/insertVideo")
    @ApiOperation(value = "给对应的课程的章节添加上新的课时")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "课时信息",name = "addVideoVo")
    })
    public Result<String> insertVideo(@RequestBody AddVideoVo addVideoVo){
        return videoService.insertVideo(addVideoVo);
    }



    @RequestMapping("/uploadMsg")
    @ApiOperation(value = "上传video类型的视频和一些ppt或者PDF课件")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "文件",name = "file")
    })
    public Result<List<Object>> uploadMsg(MultipartFile file){
        return videoService.uploadMsg(file);
    }


    // 根据章节id查询对应课时的详细信息
    @GetMapping("/findAllVideo/{chapterId}")
    @ApiOperation(value = "根据章节id查询对应的所有课时，并在原先已有的课时基础上添加上新的课时信息，前端表示：‘放心，我会出手’")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "章节id",name = "chapterId")
    })
    public Result<List<EduVideo>> findAllVideoByChapter(@PathVariable("chapterId") String chapterId){
        return videoService.findAllVideoByChapter(chapterId);
    }

    /*==================================下面是后台代码==============上面是前台代码===========================================*/
}
