package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduTeacher;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/11 13:11
 */
public interface TeacherDao extends BaseMapper<EduTeacher> {
}
