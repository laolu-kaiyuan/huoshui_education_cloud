package com.two.course.service;

import com.two.core.vo.Result;
import com.two.course.vo.FenCourseCheck;

import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 17:56
 */
public interface CheckCourseService {




    /*==================================下面是后台代码==============上面是前台代码===========================================*/
    /**
     * 课程审核分页查询
     * @param pageCurrent 第几页
     * @param pageSize 这页显示的条数
     * @param map 一些查询条件
     * @return
     */
    Result<Map<String,Object>> findCourseCheck(Integer pageCurrent, Integer pageSize, FenCourseCheck map);

    /**
     * 修改课程审核信息
     * @param status 审核状态
     * @param checkContent 审核内容
     * @param courseCheckId 课程审核id
     * @return
     */
    Result<String> updateCourseCheck(String status, String checkContent, Integer courseCheckId);

}
