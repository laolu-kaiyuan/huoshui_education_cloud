package com.two.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.two.core.entity.course.EduTeacher;
import com.two.core.entity.course.TbShouCangTeacher;
import com.two.core.vo.Result;
import com.two.course.dao.ShouCangTeacherDao;
import com.two.course.dao.TeacherDao;
import com.two.course.service.ShouCangTeacherService;
import com.two.course.vo.coursedetails.GuanZhuDataVo;
import com.two.course.vo.reception.CancelShouCangTeacherVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 16:36
 */
@Service
public class ShouCangTeacherServiceImpl implements ShouCangTeacherService {
    @Autowired
    private ShouCangTeacherDao shouCangTeacherDao;

    @Autowired
    private TeacherDao teacherDao;

    @Override
    public Result<Integer> findAttentionTeacher(GuanZhuDataVo guanZhuDataVo) {
        // 1. 根据会员id和讲师id来判断中间表里面存在数据不存在
        QueryWrapper<TbShouCangTeacher> tbShouCangTeacherQueryWrapper = new QueryWrapper<>();
        tbShouCangTeacherQueryWrapper.eq("member_id",guanZhuDataVo.getMemberid());
        tbShouCangTeacherQueryWrapper.eq("teacher_id",guanZhuDataVo.getTeacherid());
        List<TbShouCangTeacher> tbShouCangTeachers = shouCangTeacherDao.selectList(tbShouCangTeacherQueryWrapper);

        if(tbShouCangTeachers.size() == 0){
            return new Result<>(2000,"查询成功",0);
        }

        return new Result<>(2000,"查询成功",1);
    }

    @Override
    public Result<String> addAttentionTeacher(GuanZhuDataVo guanZhuDataVo) {
        TbShouCangTeacher tbShouCangTeacher = new TbShouCangTeacher();
        tbShouCangTeacher.setTeacherId(guanZhuDataVo.getTeacherid());
        tbShouCangTeacher.setMemberId(guanZhuDataVo.getMemberid());
        int insert = shouCangTeacherDao.insert(tbShouCangTeacher);
        if(insert > 0){
            return new Result<>(2000,"添加成功",null);
        }
        return new Result<>(5000,"添加失败",null);
    }

    @Override
    public Result<List<EduTeacher>> findAttentionTeacherById(String huiYuanId) {
        // 1. 查询出来该会员收藏所有讲师的id
        QueryWrapper<TbShouCangTeacher> tbShouCangTeacherQueryWrapper = new QueryWrapper<>();
        tbShouCangTeacherQueryWrapper.eq("member_id",huiYuanId);
        List<TbShouCangTeacher> tbShouCangTeachers = shouCangTeacherDao.selectList(tbShouCangTeacherQueryWrapper);
        if (tbShouCangTeachers == null || tbShouCangTeachers.size() == 0) {
            return new Result<>(5000,"没有关注");
        }
        // 1.1 提取出来讲师id
        List<String> teacherId = new ArrayList<>();
        for (TbShouCangTeacher tbShouCangTeacher : tbShouCangTeachers) {
            teacherId.add(tbShouCangTeacher.getTeacherId());
        }
        // 2. 然后根据讲师的id号去查询讲师的基本信息
        QueryWrapper<EduTeacher> eduTeacherQueryWrapper = new QueryWrapper<>();
        eduTeacherQueryWrapper.in("id",teacherId);
        List<EduTeacher> eduTeachers = teacherDao.selectList(eduTeacherQueryWrapper);
        if(eduTeachers == null){
            return new Result<>(5000,"查询收藏讲师失败",null);
        }
        return new Result<>(2000,"查询收藏讲师成功",eduTeachers);
    }

    @Override
    public Result<String> deleteAttentionTeacher(CancelShouCangTeacherVo cancelShouCangTeacherVo) {
        if(cancelShouCangTeacherVo.getId()  == null){
            cancelShouCangTeacherVo.setId(cancelShouCangTeacherVo.getTeacherid());
        }
        // 通过会员id和讲师id删除中间表的数据信息
        UpdateWrapper<TbShouCangTeacher> tbShouCangTeacherUpdateWrapper = new UpdateWrapper<>();
        tbShouCangTeacherUpdateWrapper.eq("member_id",cancelShouCangTeacherVo.getMemberid());
        tbShouCangTeacherUpdateWrapper.eq("teacher_id",cancelShouCangTeacherVo.getId());

        int delete = shouCangTeacherDao.delete(tbShouCangTeacherUpdateWrapper);
        if(delete == 0){
            return new Result<>(5000,"取消收藏讲师失败",null);
        }
        return new Result<>(2000,"取消收藏讲师成功",null);
    }
}
