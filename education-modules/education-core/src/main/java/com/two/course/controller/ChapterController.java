package com.two.course.controller;

import com.two.core.entity.course.EduChapter;
import com.two.core.vo.Result;
import com.two.course.service.ChapterService;
import com.two.course.vo.coursedetails.ChapterAndVideoVo;
import com.two.course.vo.reception.AddCourseChapterVo;
import com.two.course.vo.reception.DeleteCourseChapterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/8 20:28
 */
@RestController
@RequestMapping("/core/chapter")
@Api(tags = "章节管理接口")
public class ChapterController {

    @Autowired
    private ChapterService chapterService;

    @PostMapping("/findChapterAndVideoById/{id}")
    public Result<List<ChapterAndVideoVo>> findChapterAndVideoById(@PathVariable String id){
        return chapterService.findChapterAndVideoById(id);
    }

    /*==================================上面是观看课程界面===========================================*/




    // 这个方法需要完成两个功能：1.添加章节信息2.修改章节信息
    @PostMapping("/insertChapterWithCourseId")
    @ApiOperation(value = "添加或修改章节信息的方法")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "添加或修改章节的信息",name = "data")
    })
    public Result insertChapterWithCourseId(@RequestBody AddCourseChapterVo data){
        return chapterService.insertCourseChapter(data);
    }

    // ；这个方法是将之前对这个课程添加的章节回显出来
    @PostMapping("/findChapterById/{courseId}")
    @ApiOperation(value = "通过课程id去查找已有的章节信息，并在前端展示，在已有的章节基础上添加新的章节，前端会排好章节顺序")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "课程id",name = "courseId")
    })
    public Result<List<EduChapter>> findChapterById(@PathVariable("courseId") String courseId){
        return chapterService.findChapterById(courseId);
    }
    // 删除章节信息
    @PostMapping("/deleteChapterWithCourseId")
    @ApiOperation(value = "删除章节")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "删除章节的信息",name = "data")
    })
    public Result<String> deleteChapterWithCourseId(@RequestBody DeleteCourseChapterVo data){
        return chapterService.deleteCourseChapter(data);
    }

    /*==================================下面是后台代码==============上面是前台代码===========================================*/
}
