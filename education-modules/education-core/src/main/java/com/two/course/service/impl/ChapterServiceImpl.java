package com.two.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.course.EduChapter;
import com.two.core.entity.course.EduVideo;
import com.two.core.vo.Result;
import com.two.course.dao.ChapterDao;
import com.two.course.dao.VideoDao;
import com.two.course.service.ChapterService;
import com.two.course.vo.coursedetails.ChapterAndVideoVo;
import com.two.course.vo.coursedetails.VideoVo;
import com.two.course.vo.reception.AddCourseChapterVo;
import com.two.course.vo.reception.DeleteCourseChapterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 10:22
 */
@Service
public class ChapterServiceImpl implements ChapterService {
    @Autowired
    private ChapterDao chapterDao;

    @Autowired
    private VideoDao videoDao;

    @Override
    public Result<List<ChapterAndVideoVo>> findChapterAndVideoById(String id) {
        List<ChapterAndVideoVo> chapterAndVideoVos = new ArrayList<>();
        // 1. 现根据课程id查询出来所有章节的全部信息
        QueryWrapper<EduChapter> eduChapterQueryWrapper = new QueryWrapper<>();
        eduChapterQueryWrapper.eq("course_id",id);
        List<EduChapter> eduChapters = chapterDao.selectList(eduChapterQueryWrapper);
        // 2. 查询出来章节的所有信息之后将章节title存放到实体类当中
        if(eduChapters != null){
            for (EduChapter eduChapter : eduChapters) {
                ChapterAndVideoVo chapterAndVideoVo = new ChapterAndVideoVo();
                chapterAndVideoVo.setChapterTitle(eduChapter.getTitle());
                chapterAndVideoVo.setChapterId(eduChapter.getId());
                chapterAndVideoVos.add(chapterAndVideoVo);
            }
        }
        // 3. 然后再通过课程id找出来，该课程包含的所有课时信息
        QueryWrapper<EduVideo> eduVideoQueryWrapper = new QueryWrapper<>();
        eduVideoQueryWrapper.eq("course_id",id);
        List<EduVideo> eduVideos = videoDao.selectList(eduVideoQueryWrapper);
        // 4. 然后根据查询出来的所有课时信息，依次存放到实体类当中，对应的章节信息下面
        if(eduVideos != null){
            for (EduVideo eduVideo : eduVideos) {
                for (ChapterAndVideoVo chapterAndVideoVo : chapterAndVideoVos) {
                    if(eduVideo.getChapterId().equals(chapterAndVideoVo.getChapterId())){
                        VideoVo videoVo = new VideoVo();

                        videoVo.setVideTitle(eduVideo.getTitle());
                        videoVo.setDuration(Double.parseDouble(String.format("%.2f",eduVideo.getDuration()/60)));
                        videoVo.setStatus(eduVideo.getStatus());
                        videoVo.setIsDoc(eduVideo.getVideoOriginalName());
                        videoVo.setIsFree(eduVideo.getIsFree());

                        chapterAndVideoVo.getVideo().add(videoVo);
                    }
                }
            }
        }
        // 5. 返回
        return new Result<>(2000,"查询成功",chapterAndVideoVos);
    }


    /*==================================上面是观看课程界面===========================================*/





    @Override
    public Result<List<EduChapter>> findChapterById(String courseId) {
        // 分析：根据id查出来所有跟这个课程有关的章节信息
        QueryWrapper<EduChapter> eduChapterQueryWrapper = new QueryWrapper<>();
        eduChapterQueryWrapper.eq("course_id",courseId);
        List<EduChapter> eduChapters = chapterDao.selectList(eduChapterQueryWrapper);

        return new Result<>(2000,"查询章节信息成功",eduChapters);
    }

    @Override
    public Result insertCourseChapter(AddCourseChapterVo data) {
        // 分析：通过章节id是否为null 来判断是添加章节还是修改章节
        if(StringUtils.hasText(data.getId())){
            // 修改
            EduChapter eduChapter = new EduChapter();
            eduChapter.setCourseId(data.getCourseId());
            eduChapter.setTitle(data.getTitle());
            eduChapter.setSort(data.getSort());
            eduChapter.setId(data.getId());
            eduChapter.setGmtModified(LocalDateTime.now());
            int i = chapterDao.updateById(eduChapter);
            if(i == 0 ){
                return new Result<>(5000,"修改失败",null);
            }
            return new Result<>(2000,"修改成功",null);
        }
        // 直接添加
        EduChapter eduChapter = new EduChapter();
        eduChapter.setCourseId(data.getCourseId());
        eduChapter.setTitle(data.getTitle());
        eduChapter.setSort(data.getSort());
        eduChapter.setGmtCreate(LocalDateTime.now());
        eduChapter.setGmtModified(LocalDateTime.now());

        int insert = chapterDao.insert(eduChapter);
        if(insert == 0){
            return new Result<>(5000,"添加失败",null);
        }
        // 添加成功之后，再次去数据库当中根据CourseId查询出来对应的所有章节信息，并返回
        QueryWrapper<EduChapter> eduChapterQueryWrapper = new QueryWrapper<>();
        eduChapterQueryWrapper.eq("course_id",data.getCourseId());
        List<EduChapter> eduChapters = chapterDao.selectList(eduChapterQueryWrapper);
        return new Result<>(2000,"添加成功",eduChapters);
    }

    @Override
    public Result<String> deleteCourseChapter(DeleteCourseChapterVo data) {
        EduChapter eduChapter = new EduChapter();
        eduChapter.setCourseId(data.getCourseId());
        eduChapter.setTitle(data.getTitle());
        eduChapter.setSort(data.getSort());
        eduChapter.setId(data.getId());

        int i = chapterDao.deleteById(eduChapter);
        if(i == 0){
            return new Result<>(5000,"删除失败",null);
        }
        return new Result<>(2000,"删除成功",null);
    }

    /*==================================下面是后台代码==============上面是前台代码===========================================*/

}
