package com.two.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.course.EduSubject;
import com.two.core.vo.Result;
import com.two.course.vo.ChangeStatusIdVo;
import com.two.course.vo.EditSubjectFormData;
import com.two.course.vo.FenSubjectVo;
import com.two.course.vo.InsertSubjectVo;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 11:43
 */
public interface SubjectService {
    /**
     * 获取到课程分类的菜单
     * @return
     */
    Result<List<EduSubject>> getAllMenu();


    /*==================================上面是课程中心===========================================*/




    /**
     * 查询课程分类(字符菜单)
     * @return
     */
    Result<List<EduSubject>> findSubjectByStatus();


    String findPidById(String pid);


    /*==================================下面是后台代码==============上面是前台代码===========================================*/

    /**
     * 分页查询课程分类
     * @param pageCurrent 第几页
     * @param pageSize 这页几条
     * @param map 其他插叙条件
     * @return
     */
    Result<IPage<EduSubject>> findsubject(Integer pageCurrent, Integer pageSize, FenSubjectVo map);

    /**
     * 添加课程分类
     * @param insertSubjectVo 添加课程分类的一些信息
     * @return
     */
    Result<String> insertsubject(InsertSubjectVo insertSubjectVo);

    /**
     * 修改课程分类
     * @param editSubjectFormData 修改课程分类的一些信息
     * @return
     */
    Result<String> updatesubject(EditSubjectFormData editSubjectFormData);

    /**
     * 删除课程分类通过主键id
     * @param editSubjectFormData 包含要删除的主键id
     * @return
     */
    Result<String> deletesubject(EditSubjectFormData editSubjectFormData);

    /**
     * 修改课程分类状态
     * @param changeStatusIdVo
     * @return
     */
    Result<String> changeStatusId(ChangeStatusIdVo changeStatusIdVo);

}
