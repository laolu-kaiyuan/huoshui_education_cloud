package com.two.course.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.course.EduCourse;
import com.two.core.entity.course.EduTeacher;
import com.two.core.vo.Result;
import com.two.course.service.CourseService;
import com.two.course.vo.ChangeCourseStatusVo;
import com.two.course.vo.FenCourseVo;
import com.two.course.vo.coursecenter.SearchConditionVo;
import com.two.course.vo.coursedetails.CourseAndTeacherVo;
import com.two.course.vo.homepage.FindCourseByTeacherIdVo;
import com.two.course.vo.reception.AddCourseSubjectVo;
import com.two.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 9:50
 */
@RestController
@RequestMapping("/core/course")
@Api(tags = "课程管理接口")
public class CourseController {
    @Autowired
    private CourseService courseService;

    @PostMapping("/findCourseByTeacherId/{pageCurrent}/{pageSize}")
    public Result<Page<EduCourse>> findCourseByTeacherId(
            @PathVariable Integer pageCurrent,
            @PathVariable Integer pageSize,
            @RequestBody FindCourseByTeacherIdVo findCourseByTeacherIdVo
            ){
        return courseService.findCourseByTeacherId(pageCurrent, pageSize, findCourseByTeacherIdVo);
    }

    @GetMapping("/findTeacherByTeacherId/{teacherId}")
    public Result<EduTeacher> findTeacherByTeacherId(@PathVariable String teacherId){
        return courseService.findTeacherByTeacherId(teacherId);
    }


    /*==================================上面是讲师中心===========================================*/

    // 通过课程id查询所有课程信息和对应的讲师信息
    @PostMapping("/queryAllCourseAndTeacherName/{id}")
    public Result<CourseAndTeacherVo> queryAllCourseAndTeacherInfo(@PathVariable String id){
        return courseService.queryAllCourseAndTeacherInfo(id);
    }





    /*==================================上面是观看课程界面===========================================*/


    @PostMapping("/fontFindCourse/{pageCurrent}/{pageSize}")
    public Result<IPage<EduCourse>> fontFindCourse(
            @PathVariable Integer pageCurrent,
            @PathVariable Integer pageSize,
            @RequestBody SearchConditionVo search
            ){
        return courseService.fontFindCourse(pageCurrent,pageSize,search);
    }

    /*==================================上面是课程中心===========================================*/


    // 录播管理的首页 通过讲师的手机号查询出来所有的课程
    @GetMapping("/getCourseByTeacherPhone/{pageCurrent}/{pageSize}/{teacherMobile}/{checkStatus}")
    public Result<IPage<EduCourse>> getCourseByTeacherPhone(
            @PathVariable() Integer pageCurrent,
            @PathVariable() Integer pageSize,
            @PathVariable() String teacherMobile,
            @PathVariable() Integer checkStatus
    ){
        return courseService.getCourseByTeacherPhone(pageCurrent,pageSize,teacherMobile,checkStatus);
    }

    @RequestMapping("/uploadCover")
    @ApiOperation(value = "向阿里云上传文件(课程封面)")
    @ApiImplicitParams(
            @ApiImplicitParam(value = "图片文件",name="file",dataType = "MultipartFile")
    )
    public Result<String> uploadCover(MultipartFile file){
        return courseService.uploadCover(file);
    }

    // 录播管理--- 添加课程
    @PostMapping("/updateCourseSubject")
    @ApiOperation(value = "给对应的讲师上传新的课程信息")
    @ApiImplicitParams(
            @ApiImplicitParam(value = "添加课程的信息",name="addCourseSubjectVo")
    )
    public Result<String> updateCourseSubject(@RequestBody AddCourseSubjectVo addCourseSubjectVo){
        return courseService.AddCourseSubject(addCourseSubjectVo);
    }











    /*==================================下面是后台代码==============上面是前台代码===========================================*/


    // 修改课程信息
    @PostMapping("/updateCourse")
    @ApiOperation(value = "修改课程信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "新的课程信息",name = "formData")
    })
    @RequiresPermissions(value = "course:update")
    public Result<String> updateCourse(@RequestBody EduCourse formData){
        return courseService.updateCourseInfo(formData);
    }



    // 课程上下架
    @PostMapping("/changeStatus")
    @ApiOperation(value = "改变课程状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "课程id和课程上下架信息",name = "changeCourseStatusVo")
    })
    //@RequiresPermissions(value = "course:update")
    public Result<String> changeStatus(@RequestBody ChangeCourseStatusVo changeCourseStatusVo){
        return courseService.changeCourseStatusById(changeCourseStatusVo);
    }

    @PostMapping("/findCourse/{pageCurrent}/{pageSize}")
    @ApiOperation(value = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "展示第几页的数据",name = "pageCurrent"),
            @ApiImplicitParam(value = "这一页展示多少条数据",name = "pageSize"),
            @ApiImplicitParam(value = "查询的条件",name = "map")
    })
    public Result<IPage<EduCourse>> findCourseByPage(@PathVariable(required = true) Integer pageCurrent,
                                                     @PathVariable(required = true) Integer pageSize,
                                                     @RequestBody FenCourseVo map){
        return courseService.findCourseByPage(pageCurrent,pageSize,map);
    }

}
