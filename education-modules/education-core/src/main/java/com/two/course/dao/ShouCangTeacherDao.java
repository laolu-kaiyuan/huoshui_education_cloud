package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.TbShouCangTeacher;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 16:37
 */
public interface ShouCangTeacherDao extends BaseMapper<TbShouCangTeacher> {
}
