package com.two.course.vo.homepage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2023/1/30 18:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindCourseByTeacherIdVo {
    private String teacherId;

    private String courseName;
}
