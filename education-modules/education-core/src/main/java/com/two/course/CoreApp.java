package com.two.course;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author ${USER}
 * @description TODO
 * @date ${DATE} ${TIME}
 */
@SpringBootApplication(scanBasePackages ={"com.two.course","com.two.security","com.two.core"} )
@MapperScan(value = "com.two.course.dao")
//@EnableSwagger2
public class CoreApp {
    public static void main(String[] args) {
        SpringApplication.run(CoreApp.class,args);
    }
}
