package com.two.course.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 10:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "课程分页查询条件接收类")
public class FenCourseVo {
    @ApiModelProperty(value = "课程名关键字")
    private String title;
    @ApiModelProperty(value = "这个课程的状态")
    private String status;
}
