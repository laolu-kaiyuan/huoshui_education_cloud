package com.two.course.service;

import com.two.core.entity.course.EduCourse;
import com.two.core.vo.Result;
import com.two.course.vo.coursedetails.ShouCangDataVo;
import com.two.course.vo.reception.CancelShouCangCourseVo;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 15:29
 */
public interface ShouCangCourseService {
    /**
     * 找到会员收藏的课程
     * @param s
     * @return
     */
    public Result<List<EduCourse>> findFavoriteCourseById(String s);

    /**
     * 删除会员收藏的课程
     * @param s
     * @return
     */
    Result<String> deleteFavoriteCourse(CancelShouCangCourseVo cancelShouCangCourseVo);

    Result<Integer> findFavoriteCourse(ShouCangDataVo shouCangDataVo);

    Result<String> addFavoritescourse(ShouCangDataVo shouCangDataVo);
}
