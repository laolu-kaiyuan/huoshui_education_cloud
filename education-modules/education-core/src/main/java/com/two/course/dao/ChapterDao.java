package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduChapter;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 10:30
 */
public interface ChapterDao extends BaseMapper<EduChapter> {
}
