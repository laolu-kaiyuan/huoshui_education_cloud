package com.two.course.vo.coursedetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2023/1/30 14:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShouCangDataVo {

    private String courseid;

    private String memberid;
}
