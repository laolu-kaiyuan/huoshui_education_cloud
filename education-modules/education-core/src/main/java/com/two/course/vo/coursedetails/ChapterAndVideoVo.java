package com.two.course.vo.coursedetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2023/1/6 12:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChapterAndVideoVo {
    private String chapterId;

    private String chapterTitle;

    private List<VideoVo> video = new ArrayList<>();
}
