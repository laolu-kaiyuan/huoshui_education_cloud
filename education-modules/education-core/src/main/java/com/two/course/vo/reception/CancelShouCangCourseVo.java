package com.two.course.vo.reception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 16:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "通过课程id取消收藏")
public class CancelShouCangCourseVo {

    @ApiModelProperty(value = "课程id")
    private String id;
    private String courseid;

    @ApiModelProperty(value = "会员id")
    private String memberid;
}
