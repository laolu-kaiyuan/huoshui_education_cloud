package com.two.course.vo.coursedetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2023/1/6 12:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VideoVo {

    /**
     * 课时名称
     */
    private String videTitle;
    /**
     * 是否可以试听：0收费1免费
     */
    private Integer isFree;
    /**
     *视频时长
     */
    private Double duration;
    /**
     *1正常2修改
     */
    private Integer status;
    /**
     * 下载课件
     */
    private String isDoc;
}
