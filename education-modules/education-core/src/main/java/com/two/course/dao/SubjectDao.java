package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduSubject;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 11:44
 */
public interface SubjectDao extends BaseMapper<EduSubject> {
}
