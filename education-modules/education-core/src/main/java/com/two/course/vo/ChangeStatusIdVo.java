package com.two.course.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/8 13:28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "修改课程分类状态数据")
public class ChangeStatusIdVo {
    @ApiModelProperty("课程分类id")
    private String id;
    @ApiModelProperty("课程分类状态id")
    private String statusId;
    @ApiModelProperty("课程父id")
    private String parentId;
}
