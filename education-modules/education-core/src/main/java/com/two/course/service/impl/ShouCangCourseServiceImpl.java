package com.two.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.two.core.entity.course.EduCourse;
import com.two.core.entity.course.TbShouCangCourse;
import com.two.core.vo.Result;
import com.two.course.dao.CourseDao;
import com.two.course.dao.ShouCangCourseDao;
import com.two.course.service.ShouCangCourseService;
import com.two.course.vo.coursedetails.ShouCangDataVo;
import com.two.course.vo.reception.CancelShouCangCourseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 15:29
 */
@Service
public class ShouCangCourseServiceImpl implements ShouCangCourseService {
    @Autowired
    private ShouCangCourseDao shouCangCourseDao;
    @Autowired
    private CourseDao courseDao;
    @Override
    public Result<List<EduCourse>> findFavoriteCourseById(String s) {
        // 1. 根据会员id查询出来收藏课程的所有id
        QueryWrapper<TbShouCangCourse> tbShouCangCourseQueryWrapper = new QueryWrapper<>();
        tbShouCangCourseQueryWrapper.eq("member_id",s);
        List<TbShouCangCourse> tbShouCangCourses = shouCangCourseDao.selectList(tbShouCangCourseQueryWrapper);
        if (tbShouCangCourses == null || tbShouCangCourses.size() == 0) {
            return new Result<>(5000,"还没有收藏");
        }
        List<String> courseIds = new ArrayList<>();
        for (TbShouCangCourse tbShouCangCours : tbShouCangCourses) {
            courseIds.add(tbShouCangCours.getCourseId());
        }
        // 2. 再去课程表中根据会员对应的所有课程id查询出来对应的所有课程信息
        QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
        eduCourseQueryWrapper.in("id",courseIds);
        List<EduCourse> eduCourses = courseDao.selectList(eduCourseQueryWrapper);
        if(eduCourses == null){
            return new Result<>(5000,"没有",eduCourses);
        }
        return new Result<>(2000,"收藏课程查询成功",eduCourses);
    }

    @Override
    public Result<String> deleteFavoriteCourse(CancelShouCangCourseVo cancelShouCangCourseVo) {
        // 删除中间表根据会员id和课程id
        UpdateWrapper<TbShouCangCourse> tbShouCangCourseUpdateWrapper = new UpdateWrapper<>();
        tbShouCangCourseUpdateWrapper.eq("member_id",cancelShouCangCourseVo.getMemberid());
        if(cancelShouCangCourseVo.getId()!=null){
            tbShouCangCourseUpdateWrapper.eq("course_id",cancelShouCangCourseVo.getId());
        }else {
            tbShouCangCourseUpdateWrapper.eq("course_id",cancelShouCangCourseVo.getCourseid());
        }


        int delete = shouCangCourseDao.delete(tbShouCangCourseUpdateWrapper);
        if(delete == 0){
            return new Result<>(5000,"删除失败",null);
        }
        return new Result<>(2000,"删除成功",null);
    }

    @Override
    public Result<Integer> findFavoriteCourse(ShouCangDataVo shouCangDataVo) {
        QueryWrapper<TbShouCangCourse> tbShouCangCourseQueryWrapper = new QueryWrapper<>();
        tbShouCangCourseQueryWrapper.eq("member_id",shouCangDataVo.getMemberid());
        tbShouCangCourseQueryWrapper.eq("course_id",shouCangDataVo.getCourseid());

        List<TbShouCangCourse> tbShouCangCourses = shouCangCourseDao.selectList(tbShouCangCourseQueryWrapper);
        if(tbShouCangCourses.size() == 0){
            return new Result<>(2000,"查询成功" , 0);
        }

        return new Result<>(2000,"查询成功" , 1);
    }

    @Override
    public Result<String> addFavoritescourse(ShouCangDataVo shouCangDataVo) {
        TbShouCangCourse tbShouCangCourse = new TbShouCangCourse();
        tbShouCangCourse.setCourseId(shouCangDataVo.getCourseid());
        tbShouCangCourse.setMemberId(shouCangDataVo.getMemberid());
        int insert = shouCangCourseDao.insert(tbShouCangCourse);
        if(insert > 0){
            return new Result<>(2000,"添加成功" , null);
        }

        return new Result<>(2000,"添加成功" , null);
    }
}
