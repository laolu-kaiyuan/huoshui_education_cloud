package com.two.course.vo.reception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 10:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "添加章节名称")
public class DeleteCourseChapterVo {
    @ApiModelProperty(value = "章节id，为null，要雪花算法生成")
    private String id;
    @ApiModelProperty(value = "课程id号")
    private String courseId;
    @ApiModelProperty(value = "章节排序号(可以理解为第几章)")
    private Integer sort;
    @ApiModelProperty(value = "章节名")
    private String title;
}
