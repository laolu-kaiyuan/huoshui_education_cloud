package com.two.course.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 19:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FenPageVo {
    // 从哪一页查询
    private Integer pageCurrent;
    // 这一页要查询多少条数
    private Integer pageSize;
    // 在数据库中从下标几开始查询
    public Integer getIndex(){
        return (pageCurrent - 1) * pageSize;
    }

}
