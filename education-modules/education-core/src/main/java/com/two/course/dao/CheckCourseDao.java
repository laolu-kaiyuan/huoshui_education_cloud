package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduTbCheckCourse;
import com.two.core.entity.course.TbCheckCourse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 18:35
 */
public interface CheckCourseDao extends BaseMapper<TbCheckCourse> {
    // 分页连表查询  多参数传递 记得加上@Param
    List<EduTbCheckCourse> fenCheckCourse(@Param("index") Integer index,
                                          @Param("pageSize") Integer pageSize,
                                          @Param("title") String title,
                                          @Param("checkStatus") String checkStatus,
                                          @Param("isFree") String isFree);
    // 查询总数
    Integer fenCheckCourseNum(@Param("index") Integer index,
                              @Param("pageSize") Integer pageSize,
                              @Param("title") String title,
                              @Param("checkStatus") String checkStatus,
                              @Param("isFree") String isFree);
}
