package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduCourseDescription;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 15:26
 */
public interface EduCourseDescriptionDao extends BaseMapper<EduCourseDescription> {
}
