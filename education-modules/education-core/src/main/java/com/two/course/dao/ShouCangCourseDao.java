package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.TbShouCangCourse;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 15:30
 */
public interface ShouCangCourseDao extends BaseMapper<TbShouCangCourse> {
}
