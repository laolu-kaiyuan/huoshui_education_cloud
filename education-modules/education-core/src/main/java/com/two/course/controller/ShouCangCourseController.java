package com.two.course.controller;

import com.two.core.entity.course.EduCourse;
import com.two.core.vo.Result;
import com.two.course.service.ShouCangCourseService;
import com.two.course.vo.coursedetails.ShouCangDataVo;
import com.two.course.vo.reception.CancelShouCangCourseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 15:15
 */

@RestController
@RequestMapping("/core/shoucangCourse")
public class ShouCangCourseController {
    @Autowired
    private ShouCangCourseService shouCangCourseService;
    @PostMapping("/findFavoriteCourse")
    public Result<Integer> findFavoriteCourse(@RequestBody ShouCangDataVo shouCangDataVo){
        return shouCangCourseService.findFavoriteCourse(shouCangDataVo);
    }
    @PostMapping("/findFavoriteCourseById/{HuiYuanId}")
    public Result<List<EduCourse>> findFavoriteCourseById(@PathVariable String HuiYuanId){
        return shouCangCourseService.findFavoriteCourseById(HuiYuanId);
    }

    @PostMapping("/deleteFavoriteCourse")
    public Result<String> deleteFavoriteCourse(@RequestBody CancelShouCangCourseVo cancelShouCangCourseVo){
        return shouCangCourseService.deleteFavoriteCourse(cancelShouCangCourseVo);
    }
    @PostMapping("/addFavoritescourse")
    public Result<String> addFavoritescourse(@RequestBody ShouCangDataVo shouCangDataVo){
        return shouCangCourseService.addFavoritescourse(shouCangDataVo);
    }
}
