package com.two.course.vo.coursedetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2023/1/5 18:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuanZhuDataVo {

    private String teacherid;

    private String memberid;
}
