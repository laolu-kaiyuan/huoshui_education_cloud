package com.two.course.controller;

import com.two.core.entity.course.EduTbCheckCourse;
import com.two.core.vo.Result;
import com.two.course.service.CheckCourseService;
import com.two.course.vo.FenCourseCheck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 17:24
 */
@RestController
@RequestMapping("/core/checkCourse")
@Api(tags = "课程审核管理接口")
public class CheckCourseController {
    @Autowired
    private CheckCourseService checkCourseService;













    /*==================================下面是后台代码==============上面是前台代码===========================================*/


    // 提交审核的表单  ，， 对数据库操作就是修改某一行数据，把null的地方填上
    @PostMapping("/updateCourseCheck")
    @ApiOperation(value = "修改数据库课程审核信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "接收审核状态、审核内容、审核课程id",name = "eduTbCheckCourse")
    })
    public Result<String> updateCourseCheck(@RequestBody EduTbCheckCourse eduTbCheckCourse){
        return checkCourseService.updateCourseCheck(eduTbCheckCourse.getCheckStatus(),
                eduTbCheckCourse.getCheckContent(),
                eduTbCheckCourse.getCourseCheckId());
    }



    // 分页查询课程审核内容
    @ApiOperation("课程审核分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "第几页",name = "pageCurrent"),
            @ApiImplicitParam(value = "这个有几条",name = "pageSize"),
            @ApiImplicitParam(value = "课程名和审核状态和是否免费",name = "map")
    })
    @PostMapping("/findCourseCheck/{pageCurrent}/{pageSize}")
    public Result<Map<String,Object>> findCourseCheck(
            @PathVariable Integer pageCurrent,
            @PathVariable Integer pageSize,
            @RequestBody FenCourseCheck map
            ){
        return checkCourseService.findCourseCheck(pageCurrent,pageSize,map);
    }
}
