package com.two.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.course.EduSubject;
import com.two.core.entity.course.EduTbCheckCourse;
import com.two.core.entity.course.TbCheckCourse;
import com.two.core.util.JWTUtil;
import com.two.core.vo.Result;
import com.two.course.dao.CheckCourseDao;
import com.two.course.dao.SubjectDao;
import com.two.course.service.CheckCourseService;
import com.two.course.vo.FenCourseCheck;
import com.two.course.vo.FenPageVo;
import com.two.security.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 17:58
 */
@Service
public class CheckCourseServiceImpl implements CheckCourseService {
    @Autowired
    private CheckCourseDao checkCourseDao;
    @Autowired
    private SubjectDao subjectDao;











    /*==================================下面是后台代码==============上面是前台代码===========================================*/

    @Override
    public Result<String> updateCourseCheck(String status, String checkContent, Integer courseCheckId) {
        // 1. 修改数据库使用的方法需要传入对象
        TbCheckCourse tbCheckCourse = new TbCheckCourse();
        tbCheckCourse.setCheckStatus(status);
        tbCheckCourse.setCheckContent(checkContent);
        tbCheckCourse.setCourseCheckId(courseCheckId);
        tbCheckCourse.setCheckTime(LocalDateTime.now());
        // 2. 从前端传过来的token中解析出来审核用户的id
        String token = WebUtils.getRequest().getHeader("token");
        Map<String, Object> tokenChaim = JWTUtil.getTokenChaim(token);
        // 2.1 取出来userid
        String userId = tokenChaim.get("userId").toString();
        // 2.2 将userId也存放进去
        tbCheckCourse.setCheckUserId(userId);
        // 3. 修改数据库里面的数据，并判断是否成功，返回相应的信息
        int i = checkCourseDao.updateById(tbCheckCourse);
        if(i==0){
            return new Result<>(5000,"修改审核信息失败",null);
        }

        return new Result<>(2000,"修改审核信息成功",null);
    }
    @Override
    @Transactional
    public Result<Map<String,Object>> findCourseCheck(Integer pageCurrent, Integer pageSize, FenCourseCheck map) {
        // 1. 先（手写sql查询）分页查询出来，课程信息 先根据（第几页、这页的条数、）
        FenPageVo fenPageVo = new FenPageVo(pageCurrent, pageSize);
        // 这里写麻烦了，不想改了
        Integer index = fenPageVo.getIndex();
        String title = null;
        String checkStatus = null;
        String isFree = null;

        if(StringUtils.hasText(map.getTitle())){
            title = map.getTitle();
        }
        if(StringUtils.hasText(map.getCheckStatus())){
            checkStatus = map.getCheckStatus();
        }
        if(StringUtils.hasText(map.getIsFree())){
            isFree = map.getIsFree();
        }
        // 分页查询出来符合条件的数据
        List<EduTbCheckCourse> eduTbCheckCourses = checkCourseDao.fenCheckCourse(index,pageSize,title,checkStatus,isFree);
        // 分页查询出来符合条件数据的总数量
        Integer integer = checkCourseDao.fenCheckCourseNum(index,pageSize,title,checkStatus,isFree);

        // 前端有一列需要这种形式  【后端开发】 > 【Java】
        // 1. 查询出来所有的课程分类 （筛选条件status_id为1  有效  被查询出来）
        QueryWrapper<EduSubject> eduSubjectQueryWrapper = new QueryWrapper<>();
        eduSubjectQueryWrapper.eq("status_id","1");
        List<EduSubject> eduSubjects = subjectDao.selectList(eduSubjectQueryWrapper);
        if(eduSubjects == null){
            return new Result<>(5000,"查询全部科目失败",null);
        }
        // 分子父级
        // 2. 通过parent_id把父科目提取出来，放到list里面
        List<EduSubject> first = new ArrayList<>();
        for (EduSubject eduSubject : eduSubjects) {
            if(eduSubject.getParentId().equals("0")){
                first.add(eduSubject);
            }
        }
        // 3. 利用方法递归，来完成子科目添加到父科目的下面
        for (EduSubject eduSubject : first) {
            eduSubject.setChildren(findChildren(eduSubjects,eduSubject.getId()));
        }
        // 有什么：课程分类子父id   写个方法根据子父id 来生成【后端开发】 > 【Java】
        for (EduTbCheckCourse record : eduTbCheckCourses) {
            record.setSubjectName(createSubjectName(first,record.getSubjectId(),record.getSubjectParentId()));
            if(record.getSubjectName()==null){
                record.setSubjectName("抱歉，因为该课程分类被禁用，无法展示，请查看课程分类");
            }
        }

        Map<String, Object> map1 = new HashMap<>();
        map1.put("records",eduTbCheckCourses);
        map1.put("total",integer);
        map1.put("size",pageSize);
        return new Result<>(2000,"课程审核分页查询成功",map1);
    }

    private String createSubjectName(List<EduSubject> first, String subjectId, String subjectParentId){
        // 1.第一种情况：如果传过来的父id是 0 那么该课程就只有一个分类
        if(subjectParentId.equals("0")){
            for (EduSubject eduSubject : first) {
                if(eduSubject.getId().equals(subjectId)){
                    StringBuilder sb = new StringBuilder();
                    sb.append("【" + eduSubject.getTitle() + "】");
                    return sb.toString();
                }
            }
        }
        // 2.第二种情况:子父id都有值
        for (EduSubject eduSubject : first) {
            if(eduSubject.getId().equals(subjectParentId)){
                for (EduSubject child : eduSubject.getChildren()) {
                    if(child.getId().equals(subjectId)){
                        StringBuilder sb = new StringBuilder();
                        sb.append("【" + eduSubject.getTitle() + "】 > 【" + child.getTitle() + "】");
                        return sb.toString();
                    }
                }
            }
        }
        return null;
    }

    // 查找父科目下面的子科目，以及子科目下面的子科目  以及。。。。
    private List<EduSubject> findChildren(List<EduSubject> eduSubjects, String id){
        List<EduSubject> second = new ArrayList<>();
        // 整理出来二级科目
        for (EduSubject eduSubject : eduSubjects) {
            if(eduSubject.getParentId().equals(id)){
                second.add(eduSubject);
            }
        }
        // 递归调用
        for (EduSubject eduSubject : second) {
            eduSubject.setChildren(findChildren(eduSubjects,eduSubject.getId()));
        }

        return second;
    }
}
