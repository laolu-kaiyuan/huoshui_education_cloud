
package com.two.course.utils;

import lombok.extern.slf4j.Slf4j;
import ws.schild.jave.FfmpegFileInfo;
import ws.schild.jave.MultimediaInfo;

import java.io.File;


/**
 * @author 翟彦淇
 * @description 获取到视频时长
 * @date 2022/12/18 21:23
 */
@Slf4j
public class VideoTimeUtil {

/**
     * 获取视频时长：小时
     *
     * @param
     * @return
     */

    /*public static Float getVideoTime(File file) {
        try {
            FfmpegFileInfo instance = new FfmpegFileInfo(file);
            MultimediaInfo result = instance.getInfo();
            // 毫秒 -> 秒 -> 分钟 -> 小时  Float ls = result.getDuration() / 1000.0f / 60.0f / 60.0f;
            // 数据库要求存的秒
            Float ls = result.getDuration() / 1000.0f;
            return ls;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0.0f;
    }*/

    /*** 获取URL地址获取视频文件长度
     *
     *@paramfilePath 文件路径
     *@return视频长度(单位：秒)
     * */
    public static Long getVideoTime(String filePath) {

        File file= new File(filePath);
        try{

            FfmpegFileInfo ffmpegFileInfo= new FfmpegFileInfo(file);

            MultimediaInfo info=ffmpegFileInfo.getInfo(filePath);

            return info.getDuration() / 1000;

        }catch(Exception e) {

            log.error("获取URL视频文件时长失败", e);
            return 0L;
        }

    }
}
