package com.two.course.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/8 9:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "课程分页查询额外条件")
public class FenSubjectVo {
    @ApiModelProperty(value = "课程分类名关键字")
    private String title;
    @ApiModelProperty(value = "课程分类的状态")
    private String statusId;
}
