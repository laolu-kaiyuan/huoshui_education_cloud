package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduCourse;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 11:17
 */
public interface CourseDao extends BaseMapper<EduCourse> {
}
