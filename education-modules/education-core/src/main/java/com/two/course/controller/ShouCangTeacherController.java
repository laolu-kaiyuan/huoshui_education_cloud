package com.two.course.controller;

import com.two.core.entity.course.EduTeacher;
import com.two.core.vo.Result;
import com.two.course.service.ShouCangTeacherService;
import com.two.course.vo.coursedetails.GuanZhuDataVo;
import com.two.course.vo.reception.CancelShouCangTeacherVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 16:29
 */
@RestController
@RequestMapping("/core/shoucangTeacher")
public class ShouCangTeacherController {
    @Autowired
    private ShouCangTeacherService shouCangTeacherService;
    // 查询该会员关注这个老师了吗
    @PostMapping("/findAttentionTeacher")
    public Result<Integer> findAttentionTeacher(@RequestBody GuanZhuDataVo guanZhuDataVo){
        return shouCangTeacherService.findAttentionTeacher(guanZhuDataVo);
    }

    @PostMapping("/addAttentionTeacher")
    public Result<String> addAttentionTeacher(@RequestBody GuanZhuDataVo guanZhuDataVo){
        return shouCangTeacherService.addAttentionTeacher(guanZhuDataVo);
    }

    /*==================================上面是观看课程界面===========================================*/


    @PostMapping("/findAttentionTeacherById/{HuiYuanId}")
    public Result<List<EduTeacher>> findAttentionTeacherById(@PathVariable("HuiYuanId") String HuiYuanId){
        return shouCangTeacherService.findAttentionTeacherById(HuiYuanId);
    }

    @PostMapping("/deleteAttentionTeacher")
    public Result<String> deleteAttentionTeacher(@RequestBody CancelShouCangTeacherVo cancelShouCangTeacherVo){
        return shouCangTeacherService.deleteAttentionTeacher(cancelShouCangTeacherVo);
    }
}
