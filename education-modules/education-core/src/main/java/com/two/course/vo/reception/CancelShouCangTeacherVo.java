package com.two.course.vo.reception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 16:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "取消收藏讲师")
public class CancelShouCangTeacherVo {
    @ApiModelProperty(value = "讲师id")
    private String id;
    private String teacherid;
    @ApiModelProperty(value = "会员id")
    private String memberid;
}
