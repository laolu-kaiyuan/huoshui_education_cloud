package com.two.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.entity.course.EduVideo;
import com.two.core.vo.Result;
import com.two.course.dao.VideoDao;
import com.two.course.service.VideoService;
import com.two.course.utils.OSSUtil;
import com.two.course.utils.VideoTimeUtil;
import com.two.course.vo.reception.AddVideoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 12:10
 */
@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    private VideoDao videoDao;
    @Override
    public Result<List<EduVideo>> findAllVideoByChapter(String chapterId) {
        // 通过id查询
        QueryWrapper<EduVideo> eduVideoQueryWrapper = new QueryWrapper<>();
        eduVideoQueryWrapper.eq("chapter_id",chapterId);

        List<EduVideo> eduVideos = videoDao.selectList(eduVideoQueryWrapper);
        return new Result<>(2000,"查询成功",eduVideos);
    }

    @Override
    public Result<List<Object>> uploadMsg(MultipartFile file) {
        //video/mp4 视频
        // application/pdf
        //application/vnd.ms-powerpoint
        // 1. 获取到文件类型
        String contentType = file.getContentType();
        // 2. 根据/前面的单词判定文件类型
        String[] split = contentType.split("/");
        // 2. 判断文件类型使用对应的oss工具
        if(split[0].equals("application")){
            String s = OSSUtil.upLoad(file);
            // 在传一个视频文件大小
            List<Object> objects = new ArrayList<>();
            objects.add(s);
            objects.add(file.getSize());
            return new Result<>(2000,"上传成功",objects);
        }
        String s = OSSUtil.upLoadMp4(file);
        List<Object> objects = new ArrayList<>();
        objects.add(s);
        objects.add(file.getSize());
        return new Result<>(2000,"上传成功",objects);
    }

    @Override
    public Result<String> insertVideo(AddVideoVo addVideoVo) {
        EduVideo eduVideo = new EduVideo();
        eduVideo.setCourseId(addVideoVo.getCourseId());
        eduVideo.setChapterId(addVideoVo.getChapterId());
        eduVideo.setTitle(addVideoVo.getTitle());
        eduVideo.setVideoSourceId(addVideoVo.getVideoSourceId().get(0).toString());
        eduVideo.setVideoOriginalName(addVideoVo.getVideoOriginalName().get(0).toString());
        eduVideo.setSort(addVideoVo.getSort());
        eduVideo.setPlayCount(0L);
        eduVideo.setIsFree(addVideoVo.getIsFree());
        // 获取课时的时长,并存放到数据库当中
        Long videoTime = VideoTimeUtil.getVideoTime(addVideoVo.getVideoSourceId().get(0).toString());
        eduVideo.setDuration(Double.valueOf(videoTime.toString()));
        eduVideo.setStatus(1);
        // 先转String再转long
        eduVideo.setSize(Long.valueOf(addVideoVo.getVideoSourceId().get(1).toString()));
        eduVideo.setVersion(1L);
        eduVideo.setGmtCreate(LocalDateTime.now());
        eduVideo.setGmtModified(LocalDateTime.now());

        int insert = videoDao.insert(eduVideo);
        if(insert == 0){
            return new Result<>(5000,"添加失败",null);
        }
        return new Result<>(2000,"添加成功",null);
    }

    @Override
    public Result<String> deleteByIds(String id, Integer sort, String chapterId) {
        EduVideo eduVideo = new EduVideo();
        eduVideo.setId(id);
        eduVideo.setSort(sort);
        eduVideo.setChapterId(chapterId);
        int i = videoDao.deleteById(eduVideo);
        if(i == 0){
            return new Result<>(5000,"删除失败",null);
        }
        return new Result<>(2000,"删除成功",null);
    }

    @Override
    public Result<String> updateVideo(AddVideoVo addVideoVo) {
        EduVideo eduVideo = new EduVideo();
        eduVideo.setId(addVideoVo.getId());
        eduVideo.setCourseId(addVideoVo.getCourseId());
        eduVideo.setChapterId(addVideoVo.getChapterId());
        eduVideo.setTitle(addVideoVo.getTitle());
        eduVideo.setVideoSourceId(addVideoVo.getVideoSourceId().get(0).toString());
        eduVideo.setVideoOriginalName(addVideoVo.getVideoOriginalName().get(0).toString());
        eduVideo.setSort(addVideoVo.getSort());
        eduVideo.setPlayCount(0L);
        eduVideo.setIsFree(addVideoVo.getIsFree());
        // 获取课时的时长,并存放到数据库当中
        Long videoTime = VideoTimeUtil.getVideoTime(addVideoVo.getVideoSourceId().get(0).toString());
        eduVideo.setDuration(Double.valueOf(videoTime.toString()));
        eduVideo.setStatus(1);
        // 先转String再转long
        eduVideo.setSize(Long.valueOf(addVideoVo.getVideoSourceId().get(1).toString()));
        eduVideo.setVersion(1L);
        eduVideo.setGmtModified(LocalDateTime.now());
        int i = videoDao.updateById(eduVideo);
        if(i == 0){
            return new Result<>(5000,"修改失败",null);
        }
        return new Result<>(2000,"修改成功",null);
    }

    /*==================================下面是后台代码==============上面是前台代码===========================================*/
}
