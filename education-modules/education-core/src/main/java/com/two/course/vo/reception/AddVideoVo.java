package com.two.course.vo.reception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 13:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "课时添加的信息")
public class AddVideoVo {
    private String id;
    @ApiModelProperty(value = "章节id")
    private String chapterId;
    @ApiModelProperty(value = "课程id")
    private String courseId;
    @ApiModelProperty(value = "课时免费不")
    private Integer isFree;
    @ApiModelProperty(value = "课时排序")
    private Integer sort;
    @ApiModelProperty(value = "课时名称")
    private String title;
    @ApiModelProperty(value = "课时视频URL")
    private List<Object> videoOriginalName;
    @ApiModelProperty(value = "课时资料URL")
    private List<Object> videoSourceId;
}
