package com.two.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.course.*;
import com.two.core.vo.Result;
import com.two.course.dao.*;
import com.two.course.service.CourseService;
import com.two.course.utils.OSSUtil;
import com.two.course.vo.ChangeCourseStatusVo;
import com.two.course.vo.FenCourseVo;
import com.two.course.vo.coursecenter.SearchConditionVo;
import com.two.course.vo.coursedetails.CourseAndTeacherVo;
import com.two.course.vo.homepage.FindCourseByTeacherIdVo;
import com.two.course.vo.reception.AddCourseSubjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 11:16
 */
@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private SubjectDao subjectDao;
    @Autowired
    private EduCourseDescriptionDao eduCourseDescriptionDao;
    @Autowired
    private CheckCourseDao checkCourseDao;
    // 讲师表
    @Autowired
    private TeacherDao teacherDao;


    @Override
    public Result<Page<EduCourse>> findCourseByTeacherId(Integer pageCurrent, Integer pageSize, FindCourseByTeacherIdVo findCourseByTeacherIdVo) {
        // 1. 根据讲师id和课程名进行查询该老师的全部课程（下架的课程不查询）
        Page<EduCourse> eduCoursePage = new Page<>(pageCurrent,pageSize);
        QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
        eduCourseQueryWrapper.eq("teacher_id",findCourseByTeacherIdVo.getTeacherId());
        if(StringUtils.hasText(findCourseByTeacherIdVo.getCourseName())){
            eduCourseQueryWrapper.like("title",findCourseByTeacherIdVo.getCourseName());
        }

        eduCourseQueryWrapper.ne("status","Draft");

        Page<EduCourse> eduCoursePage1 = courseDao.selectPage(eduCoursePage, eduCourseQueryWrapper);

        return new Result<>(2000,"查询成功",eduCoursePage1);

    }

    @Override
    public Result<EduTeacher> findTeacherByTeacherId(String teacherId) {
        // 1. 根据讲师id查询出来讲师信息
        QueryWrapper<EduTeacher> eduTeacherQueryWrapper = new QueryWrapper<>();
        eduTeacherQueryWrapper.eq("id",teacherId);
        EduTeacher eduTeacher = teacherDao.selectOne(eduTeacherQueryWrapper);

        return new Result<>(2000,"查询成功",eduTeacher);
    }





    /*==================================上面是讲师中心===========================================*/












    @Override
    @Transactional
    public Result<CourseAndTeacherVo> queryAllCourseAndTeacherInfo(String id) {
        // 分析：1.通过课程id去查询出来该课程的所有信息
        QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
        eduCourseQueryWrapper.eq("id",id);
        EduCourse eduCourse = courseDao.selectOne(eduCourseQueryWrapper);
        // 1.1 将课程数据存放到规定的实体类当中
        CourseAndTeacherVo courseAndTeacherVo = new CourseAndTeacherVo();
        courseAndTeacherVo.setId(id);
        courseAndTeacherVo.setTeacherId(eduCourse.getTeacherId());
        courseAndTeacherVo.setSubjectId(eduCourse.getSubjectId());
        courseAndTeacherVo.setSubjectParentId(eduCourse.getSubjectParentId());
        courseAndTeacherVo.setTitle(eduCourse.getTitle());
        courseAndTeacherVo.setPrice(eduCourse.getPrice());
        courseAndTeacherVo.setLessonNum(eduCourse.getLessonNum());
        courseAndTeacherVo.setCover(eduCourse.getCover());
        courseAndTeacherVo.setViewCount(eduCourse.getViewCount());
        courseAndTeacherVo.setVersion(eduCourse.getVersion());
        courseAndTeacherVo.setStatus(eduCourse.getStatus());
        courseAndTeacherVo.setDeleted(eduCourse.isDeleted());
        courseAndTeacherVo.setGmtCreate(eduCourse.getGmtCreate());
        courseAndTeacherVo.setGmtModified(eduCourse.getGmtModified());
        courseAndTeacherVo.setRemark(eduCourse.getRemark());
        courseAndTeacherVo.setIsAdd(eduCourse.getIsAdd());
        courseAndTeacherVo.setCheckStatus(eduCourse.getCheckStatus());
        courseAndTeacherVo.setBuyCount(eduCourse.getBuyCount());
        courseAndTeacherVo.setViewCount(eduCourse.getViewCount());
        // 2. 在根据课程id去查询出来该课程的简介
        QueryWrapper<EduCourseDescription> eduCourseDescriptionQueryWrapper = new QueryWrapper<>();
        eduCourseDescriptionQueryWrapper.eq("id",id);
        EduCourseDescription eduCourseDescription = eduCourseDescriptionDao.selectOne(eduCourseDescriptionQueryWrapper);
        // 2.2 将课程描述放入实体类
        courseAndTeacherVo.setDescription(eduCourseDescription.getDescription());
        // 3. 再根据第一步查询出来的讲师id，根据该id查询出来讲师的全部信息
        QueryWrapper<EduTeacher> eduTeacherQueryWrapper = new QueryWrapper<>();
        eduTeacherQueryWrapper.eq("id",courseAndTeacherVo.getTeacherId());
        EduTeacher eduTeacher = teacherDao.selectOne(eduTeacherQueryWrapper);
        // 3.1 放入实体类
        courseAndTeacherVo.setName(eduTeacher.getName());
        courseAndTeacherVo.setIntro(eduTeacher.getIntro());
        courseAndTeacherVo.setCareer(eduTeacher.getCareer());
        courseAndTeacherVo.setLevel(eduTeacher.getLevel());
        courseAndTeacherVo.setAvatar(eduTeacher.getAvatar());
        courseAndTeacherVo.setSort(eduTeacher.getSort());
        courseAndTeacherVo.setMobile(eduTeacher.getMobile());
        courseAndTeacherVo.setEmail(eduTeacher.getEmail());

        return new Result<>(2000,"查询成功",courseAndTeacherVo);
    }






    /*==================================上面是课程详细页面===========================================*/





    @Override
    public Result<IPage<EduCourse>> fontFindCourse(Integer pageCurrent, Integer pageSize, SearchConditionVo search) {

        IPage<EduCourse> eduCoursePage = new Page<>(pageCurrent,pageSize);

        QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();

        // 如果一级id为null，二级id一定是null  父id为null就执行
        if(!StringUtils.hasText(search.getOneSubjectId())){
            // 分析：免费不免费的不说，都是要去课程表里面查询全部课程，但是不能查课程分类下架的课程
            // 1. 去课程分类里面查询出来，课程分类下架的子父id号
            QueryWrapper<EduSubject> eduSubjectQueryWrapper = new QueryWrapper<>();
            eduSubjectQueryWrapper.eq("status_id",0);
            List<EduSubject> eduSubjects = subjectDao.selectList(eduSubjectQueryWrapper);
            // 2. 分页查询添加条件在List<EduSubject> eduSubjects里面存放的id和父id不查找
            if(eduSubjects.size() != 0){
                for (EduSubject eduSubject : eduSubjects) {
                    eduCourseQueryWrapper.ne("subject_parent_id",eduSubject.getParentId());
                    eduCourseQueryWrapper.ne("subject_id",eduSubject.getId());
                }
            }
        }

        // 父id
        if(StringUtils.hasText(search.getOneSubjectId())){
            eduCourseQueryWrapper.eq("subject_parent_id",search.getOneSubjectId());
        }
        // 分析如果子id为null

        // 子id
        if(StringUtils.hasText(search.getTwoSubjectId())){
            eduCourseQueryWrapper.eq("subject_id",search.getTwoSubjectId());
        }
        // 是否免费 0:免费 2：付费
        if(search.getIsFee() != null){
            // 判断一下是免费还是付费
            if(search.getIsFee() == 0){
                eduCourseQueryWrapper.eq("price",0.00);
            }else{
                eduCourseQueryWrapper.gt("price",0.00);
            }
        }
        // 查询上架的课程
        eduCourseQueryWrapper.eq("status","Normal");
        IPage<EduCourse> page = courseDao.selectPage(eduCoursePage, eduCourseQueryWrapper);


        return new Result<>(2000,"查询成功",page);
    }




    /*==================================上面是课程中心===========================================*/


    @Override
    public Result<IPage<EduCourse>> getCourseByTeacherPhone(Integer pageCurrent, Integer pageSize, String teacherMobile, Integer checkStatus) {

        // 1. 先通过手机号去查询出来讲师的id
        QueryWrapper<EduTeacher> eduTeacherQueryWrapper = new QueryWrapper<>();
        eduTeacherQueryWrapper.eq("mobile",teacherMobile);
        EduTeacher eduTeacher = teacherDao.selectOne(eduTeacherQueryWrapper);
        if (eduTeacher == null) {
            return new Result<>(5000,"讲师不存在");
        }
        // 2. 然后根据讲师的id，分页条件以及审核状态来整理查询课程表的所有符合的条件，最后给前端返回数据
        if(checkStatus.equals(8)){
            // 全查
            IPage<EduCourse> eduCoursePage = new Page<>(pageCurrent,pageSize);
            QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
            eduCourseQueryWrapper.eq("teacher_id",eduTeacher.getId());
            IPage<EduCourse> page = courseDao.selectPage(eduCoursePage, eduCourseQueryWrapper);

            // 查询出来符合条件的之后，需要整合一下checkStatus每个课程的审核状态
            // 1. 先查询出来所有课程的审核状态
            List<TbCheckCourse> tbCheckCourses = checkCourseDao.selectList(null);
            // 2. 在使用for循环添加课程状态
            for (EduCourse record : page.getRecords()) {
                for (TbCheckCourse tbCheckCours : tbCheckCourses) {
                    if(record.getId().equals(tbCheckCours.getCourseId())){
                        record.setCheckStatus(Integer.valueOf(tbCheckCours.getCheckStatus()));
                    }
                }
            }
            // 还要去查询这个课程的介绍，然后整合起来
            // 1. 查询出来所有课程的简介
            List<EduCourseDescription> eduCourseDescriptions = eduCourseDescriptionDao.selectList(null);
            for (EduCourse record : page.getRecords()) {
                for (EduCourseDescription eduCourseDescription : eduCourseDescriptions) {
                    if(record.getId().equals(eduCourseDescription.getId())){
                        record.setDescription(eduCourseDescription.getDescription());
                    }
                }
            }

            return new Result<>(2000,"全部课程查询成功",page);
        }else if(checkStatus.equals(1)){
            //查询审核通过的
            IPage<EduCourse> eduCoursePage = new Page<>(pageCurrent,pageSize);
            QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
            eduCourseQueryWrapper.eq("teacher_id",eduTeacher.getId());
            // 加条件 1. 去审核表里面查询出来审核通过的课程 2.
            QueryWrapper<TbCheckCourse> tbCheckCourseQueryWrapper = new QueryWrapper<>();
            tbCheckCourseQueryWrapper.eq("check_status","1");
            List<TbCheckCourse> tbCheckCourses = checkCourseDao.selectList(tbCheckCourseQueryWrapper);
            ArrayList<String> strings = new ArrayList<>();
            for (TbCheckCourse tbCheckCours : tbCheckCourses) {
                strings.add(tbCheckCours.getCourseId());
            }
            eduCourseQueryWrapper.in("id",strings);

            IPage<EduCourse> page = courseDao.selectPage(eduCoursePage, eduCourseQueryWrapper);
            // 查询出来符合条件的之后，需要整合一下checkStatus每个课程的审核状态
            // 1. 先查询出来所有课程的审核状态
            // 2. 在使用for循环添加课程状态
            for (EduCourse record : page.getRecords()) {
                for (TbCheckCourse tbCheckCours : tbCheckCourses) {
                    if(record.getId().equals(tbCheckCours.getCourseId())){
                        record.setCheckStatus(Integer.valueOf(tbCheckCours.getCheckStatus()));
                    }
                }
            }
// 还要去查询这个课程的介绍，然后整合起来
            // 1. 查询出来所有课程的简介
            List<EduCourseDescription> eduCourseDescriptions = eduCourseDescriptionDao.selectList(null);
            for (EduCourse record : page.getRecords()) {
                for (EduCourseDescription eduCourseDescription : eduCourseDescriptions) {
                    if(record.getId().equals(eduCourseDescription.getId())){
                        record.setDescription(eduCourseDescription.getDescription());
                    }
                }
            }
            return new Result<>(2000,"审核通过查询成功",page);
        }else if(checkStatus.equals(2)){
            //查询审核通过的
            IPage<EduCourse> eduCoursePage = new Page<>(pageCurrent,pageSize);
            QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
            eduCourseQueryWrapper.eq("teacher_id",eduTeacher.getId());
            // 加条件 1. 去审核表里面查询出来审核通过的课程 2.
            QueryWrapper<TbCheckCourse> tbCheckCourseQueryWrapper = new QueryWrapper<>();
            tbCheckCourseQueryWrapper.eq("check_status","2");
            List<TbCheckCourse> tbCheckCourses = checkCourseDao.selectList(tbCheckCourseQueryWrapper);
            ArrayList<String> strings = new ArrayList<>();
            for (TbCheckCourse tbCheckCours : tbCheckCourses) {
                strings.add(tbCheckCours.getCourseId());
            }
            eduCourseQueryWrapper.in("id",strings);
            IPage<EduCourse> page = courseDao.selectPage(eduCoursePage, eduCourseQueryWrapper);
            // 查询出来符合条件的之后，需要整合一下checkStatus每个课程的审核状态
            // 1. 先查询出来所有课程的审核状态
            // 2. 在使用for循环添加课程状态
            for (EduCourse record : page.getRecords()) {
                for (TbCheckCourse tbCheckCours : tbCheckCourses) {
                    if(record.getId().equals(tbCheckCours.getCourseId())){
                        record.setCheckStatus(Integer.valueOf(tbCheckCours.getCheckStatus()));
                    }
                }
            }
            // 还要去查询这个课程的介绍，然后整合起来
            // 1. 查询出来所有课程的简介
            List<EduCourseDescription> eduCourseDescriptions = eduCourseDescriptionDao.selectList(null);
            for (EduCourse record : page.getRecords()) {
                for (EduCourseDescription eduCourseDescription : eduCourseDescriptions) {
                    if(record.getId().equals(eduCourseDescription.getId())){
                        record.setDescription(eduCourseDescription.getDescription());
                    }
                }
            }
            return new Result<>(2000,"审核中查询成功",page);
        }else if(checkStatus.equals(0)){
            //查询审核通过的
            IPage<EduCourse> eduCoursePage = new Page<>(pageCurrent,pageSize);
            QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
            eduCourseQueryWrapper.eq("teacher_id",eduTeacher.getId());
            // 加条件 1. 去审核表里面查询出来审核通过的课程 2.
            QueryWrapper<TbCheckCourse> tbCheckCourseQueryWrapper = new QueryWrapper<>();
            tbCheckCourseQueryWrapper.eq("check_status","0");
            List<TbCheckCourse> tbCheckCourses = checkCourseDao.selectList(tbCheckCourseQueryWrapper);
            ArrayList<String> strings = new ArrayList<>();
            for (TbCheckCourse tbCheckCours : tbCheckCourses) {
                strings.add(tbCheckCours.getCourseId());
            }
            eduCourseQueryWrapper.in("id",strings);
            IPage<EduCourse> page = courseDao.selectPage(eduCoursePage, eduCourseQueryWrapper);
            // 查询出来符合条件的之后，需要整合一下checkStatus每个课程的审核状态
            // 1. 先查询出来所有课程的审核状态
            // 2. 在使用for循环添加课程状态
            for (EduCourse record : page.getRecords()) {
                for (TbCheckCourse tbCheckCours : tbCheckCourses) {
                    if(record.getId().equals(tbCheckCours.getCourseId())){
                        record.setCheckStatus(Integer.valueOf(tbCheckCours.getCheckStatus()));
                    }
                }
            }
            // 还要去查询这个课程的介绍，然后整合起来
            // 1. 查询出来所有课程的简介
            List<EduCourseDescription> eduCourseDescriptions = eduCourseDescriptionDao.selectList(null);
            for (EduCourse record : page.getRecords()) {
                for (EduCourseDescription eduCourseDescription : eduCourseDescriptions) {
                    if(record.getId().equals(eduCourseDescription.getId())){
                        record.setDescription(eduCourseDescription.getDescription());
                    }
                }
            }
            return new Result<>(2000,"审核失败查询成功",page);
        }
        return new Result<>(5000,"查询失败",null);
    }

    @Override
    public Result<String> uploadCover(MultipartFile file) {
        String path = OSSUtil.upLoadImg(file);
        if(StringUtils.hasText(path)){
            return new Result<>(2000,"上传图片成功",path);
        }
        return new Result<>(5000,"上传图片失败",null);
    }

    @Override
    @Transactional
    public Result<String> AddCourseSubject(AddCourseSubjectVo addCourseSubjectVo) {
        // 分析：将前端的数据转换到后端实体类当中，根据各个实体类去操作对应的数据库
        if(addCourseSubjectVo.getId() != null){
            // 1. edu_course数据库表
            EduCourse eduCourse = new EduCourse();
            //----------------------------------假数据
            eduCourse.setTeacherId(addCourseSubjectVo.getTeacherId());
            eduCourse.setSubjectId(addCourseSubjectVo.getCategoryId2());
            eduCourse.setSubjectParentId(addCourseSubjectVo.getCategoryId1());
            eduCourse.setTitle(addCourseSubjectVo.getTitle());
            eduCourse.setPrice(addCourseSubjectVo.getPrice());
            eduCourse.setLessonNum(0);
            eduCourse.setCover(addCourseSubjectVo.getCover());
            eduCourse.setBuyCount(0L);
            eduCourse.setViewCount(0L);
            eduCourse.setVersion(1L);
            eduCourse.setStatus("Draft");
            eduCourse.setDeleted(false);
            eduCourse.setGmtCreate(LocalDateTime.now());
            eduCourse.setGmtModified(LocalDateTime.now());
            eduCourse.setRemark(addCourseSubjectVo.getRemark());
            eduCourse.setIsAdd(1);
            eduCourse.setId(addCourseSubjectVo.getId());
            int insert = courseDao.updateById(eduCourse);
            if(insert==0){
                new Result<>(5000,"课程表修改失败",null);
            }
            // 返回的主键id会存放到对象当中
            // 2. edu_course_description数据库表
            EduCourseDescription eduCourseDescription = new EduCourseDescription();
            eduCourseDescription.setId(addCourseSubjectVo.getId());
            eduCourseDescription.setDescription(addCourseSubjectVo.getDescription());
            eduCourseDescription.setGmtCreate(LocalDateTime.now());
            eduCourseDescription.setGmtModified(LocalDateTime.now());
            int insert2 = eduCourseDescriptionDao.updateById(eduCourseDescription);
            if(insert2==0){
                new Result<>(5000,"课程描述修改失败",null);
            }
            // 3. tb_check_course数据库表 ， 新添加的课程是需要审核的
            TbCheckCourse tbCheckCourse = new TbCheckCourse();
            tbCheckCourse.setCourseId(addCourseSubjectVo.getId());
            tbCheckCourse.setCheckStatus("2");
            UpdateWrapper<TbCheckCourse> tbCheckCourseUpdateWrapper = new UpdateWrapper<>();
            tbCheckCourseUpdateWrapper.eq("course_id",addCourseSubjectVo.getId());
            int insert1 = checkCourseDao.update(tbCheckCourse,tbCheckCourseUpdateWrapper);
            if(insert1==0){
                new Result<>(5000,"课程审核修改失败",null);
            }
            // 成就返回课程id
            return new Result<>(2000,"修改成功", eduCourse.getId());
        }else{
            // 1. edu_course数据库表
            EduCourse eduCourse = new EduCourse();
            //----------------------------------假数据
            eduCourse.setTeacherId(addCourseSubjectVo.getTeacherId());
            eduCourse.setSubjectId(addCourseSubjectVo.getCategoryId2());
            eduCourse.setSubjectParentId(addCourseSubjectVo.getCategoryId1());
            eduCourse.setTitle(addCourseSubjectVo.getTitle());
            eduCourse.setPrice(addCourseSubjectVo.getPrice());
            eduCourse.setLessonNum(0);
            eduCourse.setCover(addCourseSubjectVo.getCover());
            eduCourse.setBuyCount(0L);
            eduCourse.setViewCount(0L);
            eduCourse.setVersion(1L);
            eduCourse.setStatus("Draft");
            eduCourse.setDeleted(false);
            eduCourse.setGmtCreate(LocalDateTime.now());
            eduCourse.setGmtModified(LocalDateTime.now());
            eduCourse.setRemark(addCourseSubjectVo.getRemark());
            eduCourse.setIsAdd(1);
            int insert = courseDao.insert(eduCourse);
            if(insert==0){
                new Result<>(5000,"课程表添加失败",null);
            }
            // 返回的主键id会存放到对象当中
            // 2. edu_course_description数据库表
            EduCourseDescription eduCourseDescription = new EduCourseDescription();
            eduCourseDescription.setId(eduCourse.getId());
            eduCourseDescription.setDescription(addCourseSubjectVo.getDescription());
            eduCourseDescription.setGmtCreate(LocalDateTime.now());
            eduCourseDescription.setGmtModified(LocalDateTime.now());
            int insert2 = eduCourseDescriptionDao.insert(eduCourseDescription);
            if(insert2==0){
                new Result<>(5000,"课程描述添加失败",null);
            }
            // 3. tb_check_course数据库表 ， 新添加的课程是需要审核的
            TbCheckCourse tbCheckCourse = new TbCheckCourse();
            tbCheckCourse.setCourseId(eduCourse.getId());
            tbCheckCourse.setCheckStatus("2");
            int insert1 = checkCourseDao.insert(tbCheckCourse);
            if(insert1==0){
                new Result<>(5000,"课程审核添加失败",null);
            }
            // 成就返回课程id
            return new Result<>(2000,"添加成功", eduCourse.getId());
        }

    }










    /*==================================下面是后台代码==============上面是前台代码===========================================*/

    @Override
    @Transactional
    public Result<String> updateCourseInfo(EduCourse formData) {
        // 设置当前修改时间
        formData.setGmtModified(LocalDateTime.now());
        // 修改
        int i = courseDao.updateById(formData);
        // 判断
        if(i == 0){
            return new Result<>(5000,"课程主要信息修改失败",null);
        }
        // 修改课程描述
        EduCourseDescription eduCourseDescription = new EduCourseDescription();
        // 新的描述内容
        eduCourseDescription.setDescription(formData.getDescription());
        // 课程id
        eduCourseDescription.setId(formData.getId());
        // 当前修改时间
        eduCourseDescription.setGmtModified(LocalDateTime.now());
        int i1 = eduCourseDescriptionDao.updateById(eduCourseDescription);
        if(i1 == 0){
            return new Result<>(5000,"课程描述修改失败",null);
        }
        return new Result<>(2000,"课程信息修改成功",null);
    }

    @Override
    @Transactional
    public Result<String> changeCourseStatusById(ChangeCourseStatusVo changeCourseStatusVo) {
        // 分析：在去修改课程上下架的时候需要去判断一下这个课程有没有通过审核或者处于待审核的状态，
        // 如果没有通过审核或者处于待审核状态，就不允许上架，只能处于下架状态
        QueryWrapper<TbCheckCourse> tbCheckCourseQueryWrapper = new QueryWrapper<>();
        tbCheckCourseQueryWrapper.eq("course_id",changeCourseStatusVo.getId());
        TbCheckCourse tbCheckCourse = checkCourseDao.selectOne(tbCheckCourseQueryWrapper);
        if(tbCheckCourse.getCheckStatus().equals("2") || tbCheckCourse.getCheckStatus().equals("0")){
            // 应该也许能防止bug吧
            EduCourse eduCourse = new EduCourse(changeCourseStatusVo.getId(), "Draft");
            int i = courseDao.updateById(eduCourse);
            return new Result<>(2000,"该课程还没有审核或者通过审核所以不能够上架",null);
        }
        // 需要传入对象修改,
        EduCourse eduCourse = new EduCourse(changeCourseStatusVo.getId(), changeCourseStatusVo.getStatus());
        int i = courseDao.updateById(eduCourse);
        if(i == 0){
            return new Result<>(5000,"修改失败",null);
        }
        return new Result<>(2000,"修改成功",null);
    }

    @Override
    @Transactional
    public Result<IPage<EduCourse>> findCourseByPage(Integer pageCurrent, Integer pageSize, FenCourseVo map) {
        // 第几页  条数
        IPage<EduCourse> page = new Page<>(pageCurrent,pageSize);
        // 添加分页条件
        QueryWrapper<EduCourse> eduCourseQueryWrapper = new QueryWrapper<>();
        if(StringUtils.hasText(map.getTitle())){
            eduCourseQueryWrapper.like("title",map.getTitle());
        }
        if(StringUtils.hasText(map.getStatus())){
            eduCourseQueryWrapper.eq("status",map.getStatus());
        }
        // 查询出来当前页的全部课程信息
        IPage<EduCourse> page1 = courseDao.selectPage(page, eduCourseQueryWrapper);
        // 业务判断
        if(page1==null){
            return new Result<>(5000,"分页查询失败",null);
        }
        // 前端有一列需要这种形式  【后端开发】 > 【Java】
        // 1. 查询出来所有的课程分类 （筛选条件status_id为1  有效  被查询出来）
        QueryWrapper<EduSubject> eduSubjectQueryWrapper = new QueryWrapper<>();
        eduSubjectQueryWrapper.eq("status_id","1");
        List<EduSubject> eduSubjects = subjectDao.selectList(eduSubjectQueryWrapper);
        if(eduSubjects == null){
            return new Result<>(5000,"查询全部科目失败",null);
        }
        // 分子父级
        // 2. 通过parent_id把父科目提取出来，放到list里面
        List<EduSubject> first = new ArrayList<>();
        for (EduSubject eduSubject : eduSubjects) {
            if(eduSubject.getParentId().equals("0")){
                first.add(eduSubject);
            }
        }
        // 3. 利用方法递归，来完成子科目添加到父科目的下面
        for (EduSubject eduSubject : first) {
            eduSubject.setChildren(findChildren(eduSubjects,eduSubject.getId()));
        }
        // 有什么：课程分类子父id   写个方法根据子父id 来生成【后端开发】 > 【Java】
        for (EduCourse record : page1.getRecords()) {
            record.setSubjectName(createSubjectName(first,record.getSubjectId(),record.getSubjectParentId()));
            if(record.getSubjectName()==null){
                record.setSubjectName("抱歉，因为该课程分类被禁用，无法展示，请查看课程分类");
            }
        }

        // 4. 还需要查询课程描述并存放起来，因为在做修改的时候需要回先，而前端直接使用的是这次查找的数据
        List<EduCourseDescription> eduCourseDescriptions = eduCourseDescriptionDao.selectList(null);

        for (EduCourse record : page1.getRecords()) {
            for (EduCourseDescription eduCourseDescription : eduCourseDescriptions) {
                if(record.getId().equals(eduCourseDescription.getId())){
                    record.setDescription(eduCourseDescription.getDescription());
                }
            }
        }
        return new Result<>(200,"分页查询课程成功",page1);
    }

    private String createSubjectName(List<EduSubject> first,String subjectId, String subjectParentId){
        // 1.第一种情况：如果传过来的父id是 0 那么该课程就只有一个分类
        if(subjectParentId.equals("0")){
            for (EduSubject eduSubject : first) {
                if(eduSubject.getId().equals(subjectId)){
                    StringBuilder sb = new StringBuilder();
                    sb.append("【" + eduSubject.getTitle() + "】");
                    return sb.toString();
                }
            }
        }
        // 2.第二种情况:子父id都有值
        for (EduSubject eduSubject : first) {
            if(eduSubject.getId().equals(subjectParentId)){
                for (EduSubject child : eduSubject.getChildren()) {
                    if(child.getId().equals(subjectId)){
                        StringBuilder sb = new StringBuilder();
                        sb.append("【" + eduSubject.getTitle() + "】 > 【" + child.getTitle() + "】");
                        return sb.toString();
                    }
                }
            }
        }
        return null;
    }

    // 查找父科目下面的子科目，以及子科目下面的子科目  以及。。。。
    private List<EduSubject> findChildren(List<EduSubject> eduSubjects, String id){
        List<EduSubject> second = new ArrayList<>();
        // 整理出来二级科目
        for (EduSubject eduSubject : eduSubjects) {
            if(eduSubject.getParentId().equals(id)){
                second.add(eduSubject);
            }
        }
        // 递归调用
        for (EduSubject eduSubject : second) {
            eduSubject.setChildren(findChildren(eduSubjects,eduSubject.getId()));
        }

        return second;
    }
}








