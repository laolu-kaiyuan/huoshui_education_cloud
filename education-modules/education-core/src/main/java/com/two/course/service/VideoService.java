package com.two.course.service;

import com.two.core.entity.course.EduVideo;
import com.two.core.vo.Result;
import com.two.course.vo.reception.AddVideoVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 12:09
 */
public interface VideoService {
    /**
     * 根据章节id查询出来章节对应的所有课时信息
     * @param chapterId
     * @return
     */
    Result<List<EduVideo>> findAllVideoByChapter(String chapterId);

    /**
     * 上传阿里云oss 上传视频 和 课件
     * @param file
     * @return
     */
    Result<List<Object>> uploadMsg(MultipartFile file);

    /**
     * 为 对应的课程的章节添加课时
     * @param addVideoVo
     * @return
     */
    Result<String> insertVideo(AddVideoVo addVideoVo);

    /**
     * 根据课时id、排序、章节id
     * @param id
     * @param sort
     * @param chapterId
     * @return
     */
    Result<String> deleteByIds(String id, Integer sort, String chapterId);

    /**
     * 修改课时的信息
     * @param addVideoVo
     * @return
     */
    Result<String> updateVideo(AddVideoVo addVideoVo);

    /*==================================下面是后台代码==============上面是前台代码===========================================*/
}
