package com.two.course.vo.reception;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/8 15:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddCourseSubjectVo {
    /**
     * 课程ID
     */
    @TableId
    private String id;
    /**
     *课程讲师ID
     */
    private String teacherId;
    /**
     *课程专业ID
     */
    private String categoryId2;
    /**
     *课程专业父级ID
     */
    private String categoryId1;
    /**
     * 三级用不到
     */
    private String categoryId3;
    /**
     *课程标题
     */
    private String title;
    /**
     *课程销售价格，设置为0则可免费观看
     */
    private BigDecimal price;
    /**
     *课程封面图片路径
     */
    private String cover;
    /**
     *课程备注
     */
    private String remark;
    /**
     * 所属课程分类
     */
    private String subjectName;
    /**
     * 课程描述
     */
    private String description;
    /**
     * 是否免费
     */
    private String isFree;
}
