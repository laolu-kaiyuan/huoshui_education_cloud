package com.two.course.vo.coursecenter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 19:06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "课程中心菜单查询的条件")
public class SearchConditionVo {
    @ApiModelProperty(value = "一级菜单id")
    private String oneSubjectId;
    @ApiModelProperty(value = "二级菜单id")
    private String twoSubjectId;
    @ApiModelProperty(value = "是否免费 0：免费 2：付费")
    private Integer isFee;
}
