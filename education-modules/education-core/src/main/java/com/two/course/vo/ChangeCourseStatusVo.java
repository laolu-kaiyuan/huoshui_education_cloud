package com.two.course.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 14:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "修改课程上下架")
public class ChangeCourseStatusVo {
    @ApiModelProperty(value = "课程id")
    private String id;
    @ApiModelProperty(value = "课程状态 Draft未发布  Normal已发布")
    private String status;
}
