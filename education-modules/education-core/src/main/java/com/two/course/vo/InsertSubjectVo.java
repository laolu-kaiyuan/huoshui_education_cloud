package com.two.course.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/8 10:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "添加课程分类")
public class InsertSubjectVo {
    @ApiModelProperty(value = "课程分类主键id")
    private String id;
    @ApiModelProperty(value = "课程分类对应的父级id")
    private String parentId;
    @ApiModelProperty(value = "课程分类备注")
    private String remark;
    @ApiModelProperty(value = "课程分类名")
    private String title;
}
