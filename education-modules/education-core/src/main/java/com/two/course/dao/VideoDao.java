package com.two.course.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.core.entity.course.EduVideo;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 12:11
 */
public interface VideoDao extends BaseMapper<EduVideo> {
}
