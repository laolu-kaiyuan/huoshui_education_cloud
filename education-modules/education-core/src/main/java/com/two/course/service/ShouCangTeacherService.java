package com.two.course.service;

import com.two.core.entity.course.EduTeacher;
import com.two.core.vo.Result;
import com.two.course.vo.coursedetails.GuanZhuDataVo;
import com.two.course.vo.reception.CancelShouCangTeacherVo;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 16:35
 */
public interface ShouCangTeacherService {

    Result<Integer> findAttentionTeacher(GuanZhuDataVo guanZhuDataVo);

    Result<String> addAttentionTeacher(GuanZhuDataVo guanZhuDataVo);

    /*==================================上面是观看课程界面===========================================*/


    /**
     * 找到会员收藏的所有讲师信息
     * @param huiYuanId
     * @return
     */
    Result<List<EduTeacher>> findAttentionTeacherById(String huiYuanId);

    /**
     * 取消会员收藏的讲师
     * @param cancelShouCangTeacherVo
     * @return
     */
    Result<String> deleteAttentionTeacher(CancelShouCangTeacherVo cancelShouCangTeacherVo);


}
