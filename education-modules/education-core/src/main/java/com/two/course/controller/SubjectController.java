package com.two.course.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.two.core.entity.course.EduSubject;
import com.two.core.vo.Result;
import com.two.course.service.SubjectService;
import com.two.course.vo.ChangeStatusIdVo;
import com.two.course.vo.EditSubjectFormData;
import com.two.course.vo.FenSubjectVo;
import com.two.course.vo.InsertSubjectVo;
import com.two.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 11:43
 */
@RestController
@RequestMapping("/core/subject")
@Api(tags = "课程分类管理接口")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @GetMapping("/getAllQianMenu")
    public Result<List<EduSubject>> getAllQianMenu(){
        return subjectService.getAllMenu();
    }


    /*==================================上面是录播图上的分类===========================================*/

    @PostMapping("/getAllMenu")
    public Result<List<EduSubject>> getAllMenu(){
        return subjectService.getAllMenu();
    }

    /*==================================上面是课程中心===========================================*/

    @PostMapping("/findSubjectByStatus/{status1}/{status2}")
    @ApiOperation("查询课程的一、二、三级分类")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "这参数没有用到" , name = "status1"),
            @ApiImplicitParam(value = "不知道有什么用" , name = "status2")
    })
    public Result<List<EduSubject>> findSubjectByStatus(@PathVariable String status1, @PathVariable String status2){
        return subjectService.findSubjectByStatus();
    }
    // 判断父id来判断是一级菜单还是二级菜单
    @GetMapping("/findPidById/{pid}")
    public String findPidById(@PathVariable String pid){
        return subjectService.findPidById(pid);
    }








    /*==================================下面是后台代码==============上面是前台代码===========================================*/

    @PostMapping("/changeStatusId")
    @ApiOperation(value = "改变课程分类状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "接收改变课程分类状态的信息" , name = "changeStatusIdVo")
    })
    public Result<String> changeStatusId(@RequestBody ChangeStatusIdVo changeStatusIdVo){
        return subjectService.changeStatusId(changeStatusIdVo);
    }

    @PostMapping("/deletesubject")
    @ApiOperation(value = "删除课程分类")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "接收删除课程分类的信息" , name = "editSubjectFormData")
    })
    public Result<String> deletesubject(@RequestBody EditSubjectFormData editSubjectFormData){
        return subjectService.deletesubject(editSubjectFormData);
    }

    @PostMapping("/updatesubject")
    @ApiOperation(value = "修改课程分类状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "接收修改课程分类的信息" , name = "editSubjectFormData")
    })
    public Result<String> updatesubject(@RequestBody EditSubjectFormData editSubjectFormData){
        return subjectService.updatesubject(editSubjectFormData);
    }

    @PostMapping("/insertsubject")
    @ApiOperation(value = "添加课程分类")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "接收添加课程分类的信息" , name = "insertSubjectVo")
    })
    //@RequiresPermissions(value = "subject:import")
    public Result<String> insertsubject(@RequestBody InsertSubjectVo insertSubjectVo){
        return subjectService.insertsubject(insertSubjectVo);
    }

    @PostMapping("/findsubject/{pageCurrent}/{pageSize}")
    @ApiOperation(value = "分页查询课程分类")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "第几页" , name = "pageCurrent"),
            @ApiImplicitParam(value = "这页的条数" , name = "pageSize"),
            @ApiImplicitParam(value = "其他分页检索条件" , name = "map")
    })
    @RequiresPermissions(value = "course:list")
    public Result<IPage<EduSubject>> findsubject(
            @PathVariable Integer pageCurrent,
            @PathVariable Integer pageSize,
            @RequestBody FenSubjectVo map
            ){
        return subjectService.findsubject(pageCurrent,pageSize,map);
    }
}
