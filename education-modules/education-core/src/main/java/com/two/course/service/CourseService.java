package com.two.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.course.EduCourse;
import com.two.core.entity.course.EduTeacher;
import com.two.core.vo.Result;
import com.two.course.vo.ChangeCourseStatusVo;
import com.two.course.vo.FenCourseVo;
import com.two.course.vo.coursecenter.SearchConditionVo;
import com.two.course.vo.coursedetails.CourseAndTeacherVo;
import com.two.course.vo.homepage.FindCourseByTeacherIdVo;
import com.two.course.vo.reception.AddCourseSubjectVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 11:15
 */
public interface CourseService {

    Result<Page<EduCourse>> findCourseByTeacherId(Integer pageCurrent, Integer pageSize, FindCourseByTeacherIdVo findCourseByTeacherIdVo);

    Result<EduTeacher> findTeacherByTeacherId(String teacherId);
    /*==================================上面是讲师中心===========================================*/

    Result<CourseAndTeacherVo> queryAllCourseAndTeacherInfo(String id);



    /*==================================上面是课程详细信息页面===========================================*/



    /**
     * 查询出来符合条件的课程
     * @param pageCurrent
     * @param pageSize
     * @param search
     * @return
     */
    Result<IPage<EduCourse>> fontFindCourse(Integer pageCurrent, Integer pageSize, SearchConditionVo search);


    /*==================================上面是课程中心===========================================*/

    /**
     * 通过手机号来查询对应 对应讲师的全部课程
     * @param pageCurrent 第几页
     * @param pageSize 这页展示的条数
     * @param teacherMobile 讲师手机号
     * @param checkStatus 审核状态码 8：查询这个讲师全部课程 1：查询这个讲师的审核通过课程 2：查询这个讲师审核中的课程0：查询这个讲师审核失败的课程
     * @return
     */
    Result<IPage<EduCourse>> getCourseByTeacherPhone(Integer pageCurrent, Integer pageSize, String teacherMobile, Integer checkStatus);


    /**
     * 上传图片到阿里云的oss,并返回图片路径
     * @param file 图片文件
     * @return
     */
    Result<String> uploadCover(MultipartFile file);

    /**
     * 添加课程
     * @param addCourseSubjectVo
     * @return
     */
    Result<String> AddCourseSubject(AddCourseSubjectVo addCourseSubjectVo);


    /*==================================下面是后台代码==============上面是前台代码===========================================*/
    /**
     * 根据条件分页查询
     * @param pageCurrent 第几页
     * @param pageSize 这页展示的个数
     * @param map 查询课程的其他条件
     * @return
     */
    Result<IPage<EduCourse>> findCourseByPage(Integer pageCurrent, Integer pageSize, FenCourseVo map);

    /**
     * 根据课程id，来修改对应的课程上下架状态
     * @param changeCourseStatusVo 课程id和上下架状态
     * @return
     */
    Result<String> changeCourseStatusById(ChangeCourseStatusVo changeCourseStatusVo);

    Result<String> updateCourseInfo(EduCourse formData);


}
