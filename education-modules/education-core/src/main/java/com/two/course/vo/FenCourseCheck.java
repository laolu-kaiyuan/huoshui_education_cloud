package com.two.course.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 17:28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "分页查询课程审核")
public class FenCourseCheck {
    @ApiModelProperty(value = "课程关键字")
    private String title;
    @ApiModelProperty(value = "审核状态")
    private String checkStatus;
    @ApiModelProperty(value = "是否免费")
    private String isFree;
}
