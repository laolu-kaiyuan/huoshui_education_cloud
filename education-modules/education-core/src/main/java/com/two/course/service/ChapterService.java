package com.two.course.service;

import com.two.core.entity.course.EduChapter;
import com.two.core.vo.Result;
import com.two.course.vo.coursedetails.ChapterAndVideoVo;
import com.two.course.vo.reception.AddCourseChapterVo;
import com.two.course.vo.reception.DeleteCourseChapterVo;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 10:22
 */
public interface ChapterService {

    Result<List<ChapterAndVideoVo>> findChapterAndVideoById(String id);

    /*==================================上面是观看课程界面===========================================*/





    /**
     * 通过课程id把课程对应的章节信息查询出来
     * @param courseId 课程id
     * @return
     */
    Result<List<EduChapter>> findChapterById(String courseId);

    /**
     * 对edu_chapter数据库表添加对应课程的章节信息
     * @param data 添加章节的一些信息
     * @return
     */
    Result insertCourseChapter(AddCourseChapterVo data);

    /**
     * 删除章节
     * @param data
     * @return
     */
    Result<String> deleteCourseChapter(DeleteCourseChapterVo data);


    /*==================================下面是后台代码==============上面是前台代码===========================================*/
}
