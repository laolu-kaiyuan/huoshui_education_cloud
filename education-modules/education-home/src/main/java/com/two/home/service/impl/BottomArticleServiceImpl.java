package com.two.home.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.two.core.vo.Result;
import com.two.home.dao.BottomArticleDao;
import com.two.home.entity.BottomArticle;
import com.two.home.service.BottomArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/15 23:56
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class BottomArticleServiceImpl extends ServiceImpl<BottomArticleDao, BottomArticle> implements BottomArticleService {
    @Autowired
    BottomArticleDao bottomArticleDao;
    @Override
    public Result getAllBottomArticle(Integer currentPage, Integer pageSize, BottomArticle bottomArticle) {
        Page<BottomArticle> page = new Page<>(currentPage,pageSize);
        QueryWrapper<BottomArticle> wrapper = new QueryWrapper<>();

        if (StringUtils.hasText(bottomArticle.getName())){
            wrapper.like("name",bottomArticle.getName());
        }
        if (ObjectUtil.isNotEmpty(bottomArticle.getIsDisable())){
            wrapper.eq("is_disable",bottomArticle.getIsDisable());
        }
        Page<BottomArticle> page1 = bottomArticleDao.selectPage(page, wrapper);
        List<BottomArticle> records = page1.getRecords();
        if (records != null) {
            //拿到所有id
            List<String> ids=new ArrayList<>();
            for (BottomArticle id : records) {
                ids.add(id.getId());
            }

            List<BottomArticle> first=new ArrayList<>();
            for (BottomArticle record : records) {
                if (!ids.contains(record.getParentId()) || record.getParentId()=="0"){
                    first.add(record);
                }
            }
            for (BottomArticle record : first) {
                List<BottomArticle> seconds=new ArrayList<>();
                for (BottomArticle article : records) {
                    if(article.getParentId().equals(record.getId())){
                        seconds.add(article);
                    }
                    record.setChildren(seconds);
                }
            }
            page1.setRecords(first);
        }

        return new Result(2000,"分页查询成功",page1);
    }

    @Override
    public Result changeStatus(String id, Integer isDisable) {
        BottomArticle bottomArticle = new BottomArticle();
        bottomArticle.setId(id);
        bottomArticle.setIsDisable(isDisable);
        int res = bottomArticleDao.updateById(bottomArticle);
        if (res>0){
            return new Result(2000,"修改成功");
        }
        return new Result(5000,"修改失败");
    }

    @Override
    public Result addBottomArticle(BottomArticle bottomArticle) {
        bottomArticle.setGmtCreate(LocalDateTime.now());
        int i = 0;
        String msg = "";
        if (bottomArticle.getId()!=null){
            bottomArticle.setGmtModified(LocalDateTime.now());
            i = bottomArticleDao.updateById(bottomArticle);
            msg="修改";
        }else {
            bottomArticle.setGmtCreate(LocalDateTime.now());
            bottomArticle.setGmtModified(LocalDateTime.now());
            i = bottomArticleDao.insert(bottomArticle);
            msg="添加";
        }
        if (i > 0) {
            return new Result(2000,msg+"成功");
        }
        return new Result(5000,msg+"失败");
    }

    @Override
    public Result deleteById(String id) {
        int i = bottomArticleDao.deleteById(id);
        if (i >0) {
            return new Result<>(2000,"删除成功");
        }
        return new Result<>(5000,"删除失败");
    }

    @Override
    public Result getQianAllBottomArticle() {
        QueryWrapper<BottomArticle> wrapper = new QueryWrapper<>();
        List<BottomArticle> bottomArticles = bottomArticleDao.selectList(wrapper);
        List<BottomArticle> first=new ArrayList<>();
        if (bottomArticles != null) {
            //拿到所有id
            List<String> ids=new ArrayList<>();
            for (BottomArticle id : bottomArticles) {
                ids.add(id.getId());
            }
            for (BottomArticle record : bottomArticles) {
                if (!ids.contains(record.getParentId()) || record.getParentId()=="0"){
                    first.add(record);
                }
            }
            for (BottomArticle record : first) {
                List<BottomArticle> seconds=new ArrayList<>();
                for (BottomArticle article : bottomArticles) {
                    if(article.getParentId().equals(record.getId())){
                        seconds.add(article);
                    }
                    record.setChildren(seconds);
                }
            }
        }
        return new Result(2000,"成功",first);
    }
}