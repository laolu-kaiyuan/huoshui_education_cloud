package com.two.home;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/12 4:11
 * @description：
 * @modified By：
 * @version:
 */
//springboot默认扫描的主类所在的包以及子包。 能够可以修改springboot的包扫描
@SpringBootApplication(scanBasePackages ={"com.two.home","com.two.security","com.two.core.config"} )
@MapperScan(basePackages = "com.two.home.dao")
@EnableSwagger2
@EnableFeignClients
public class HomeApp {
    public static void main(String[] args) {
        SpringApplication.run(HomeApp.class,args);
    }
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}