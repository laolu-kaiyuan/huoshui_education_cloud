package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.Article;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/18 19:05
 * @description：
 * @modified By：
 * @version:
 */
public interface ArticleDao extends BaseMapper<Article> {
}