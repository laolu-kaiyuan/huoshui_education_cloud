package com.two.home.controller;

import com.two.core.vo.Result;
import com.two.home.entity.HeadNav;
import com.two.home.service.HeadNavService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/14 4:02
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/home/head")
@Api(tags = "头部导航接口类")
public class HeadNavController {
    @Autowired
    private HeadNavService headNavService;

    @GetMapping("getAllHead")
    @ApiOperation(value = "前端查询头")
    public Result getAllHead(){
        return headNavService.getAllHead();
    }

    @PostMapping("getAllHeadNav/{currentPage}/{pageSize}")
    @ApiOperation(value = "根据条件分页查询头部导航")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "currentPage",value = "当前页码"),
                    @ApiImplicitParam(name = "pageSize",value = "每页显示的条数")
            }
    )
    public Result<HeadNav> getAllHeadNav(@PathVariable(required = true) Integer currentPage,
                                         @PathVariable(required =true ) Integer pageSize,
                                         @RequestBody HeadNav headNav){
        return headNavService.getAllHeadNav(currentPage,pageSize,headNav);
    }
    @PostMapping("/addHeadNav")
    @ApiOperation(value = "添加和修改头部导航")
    public Result addHeadNav(@RequestBody HeadNav headNav){
        return headNavService.addHeadNav(headNav);
    }
    @DeleteMapping("deleteHeadNav/{id}")
    @ApiOperation(value = "删除导航")
    public Result deleteById(@PathVariable Integer id){
        return headNavService.deleteById(id);
    }

    @ApiOperation(value = "改变头部导航状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "头部导航id"),
            @ApiImplicitParam(name = "deleted",value = "状态")})
    @PostMapping("changeStatus/{id}/{deleted}")
    public Result changeStatus(@PathVariable Integer id,
                               @PathVariable Integer deleted){
        return headNavService.changeStatus(id,deleted);
    }
}