package com.two.home.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * (Link)实体类
 *
 * @author makejava
 * @since 2022-12-18 20:39:14
 */
@Data
@TableName("tb_link")
public class Link implements Serializable {
    private static final long serialVersionUID = 735933306059864908L;
    @TableId
    private String linkId;
    
    private String linkUrl;
    
    private String linkName;
    
    private Integer isDisable;
    
    private Integer isDeleted;
    
    private LocalDateTime gmtCreate;
    
    private LocalDateTime gmtModified;

}

