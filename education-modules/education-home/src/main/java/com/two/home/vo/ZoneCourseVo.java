package com.two.home.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/12 17:27
 * @description：
 * @modified By：
 * @version:
 */
@Data
public class ZoneCourseVo {
    private String id;
    private String courseTitle;
    private String parentname;
    private String subjecttitle;
    private String zoneId;
    private String courseId;
    private Integer isDisable;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModified;
}