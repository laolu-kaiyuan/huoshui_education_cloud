package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.Banner;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/15 23:54
 * @description：
 * @modified By： * @version:
 */
public interface BannerDao extends BaseMapper<Banner> {
}