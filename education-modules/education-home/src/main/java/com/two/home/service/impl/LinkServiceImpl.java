package com.two.home.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.two.core.vo.Result;
import com.two.home.dao.LinkDao;
import com.two.home.entity.Link;
import com.two.home.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/15 23:56
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class LinkServiceImpl extends ServiceImpl<LinkDao, Link> implements LinkService {
    @Autowired
    LinkDao linkDao;
    @Override
    public Result getAllLink(Integer currentPage, Integer pageSize, Link link) {
        Page<Link> page = new Page<>(currentPage,pageSize);
        QueryWrapper<Link> wrapper = new QueryWrapper<>();
        if (StringUtils.hasText(link.getLinkName())){
            wrapper.like("link_name",link.getLinkName());
        }
        if (ObjectUtil.isNotEmpty(link.getIsDisable())){
            wrapper.eq("is_disable",link.getIsDisable());
        }
        Page<Link> page1 = linkDao.selectPage(page, wrapper);
        return new Result(2000,"分页查询成功",page1);
    }

    @Override
    public Result changeStatus(String id, Integer isDisable) {
        Link link = new Link();
        link.setLinkId(id);
        link.setIsDisable(isDisable);
        int res = linkDao.updateById(link);
        if (res>0){
            return new Result(2000,"修改成功");
        }
        return new Result(5000,"修改失败");
    }

    @Override
    public Result addLink(Link link) {
        link.setGmtCreate(LocalDateTime.now());
        int i = 0;
        String msg = "";
        if (link.getLinkId()!=null){
            link.setGmtModified(LocalDateTime.now());
            i = linkDao.updateById(link);
            msg="修改";
        }else {
            link.setGmtCreate(LocalDateTime.now());
            link.setGmtModified(LocalDateTime.now());
            i = linkDao.insert(link);
            msg="添加";
        }
        if (i > 0) {
            return new Result(2000,msg+"成功");
        }
        return new Result(5000,msg+"失败");
    }

    @Override
    public Result deleteById(String id) {
        int i = linkDao.deleteById(id);
        if (i >0) {
            return new Result<>(2000,"删除成功");
        }
        return new Result<>(5000,"删除失败");
    }

    @Override
    public Result getQianLink() {
        QueryWrapper<Link> wrapper = new QueryWrapper<>();
        wrapper.eq("is_disable","0");
        List<Link> links = linkDao.selectList(wrapper);
        return new Result(2000,"",links);
    }
}