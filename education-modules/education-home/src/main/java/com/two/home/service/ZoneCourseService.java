package com.two.home.service;

import com.two.core.vo.Result;
import com.two.home.entity.Course;
import com.two.home.entity.ZoneCourse;
import com.two.home.vo.ZoneCourseVo;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/12 17:20
 * @description：
 * @modified By： * @version:
 */
public interface ZoneCourseService {
    /**
     * 分页查询课程专区
     * @param currentPage
     * @param pageSize
     * @param zoneCourseVo
     * @return
     */
    Result getAllZoneCourse(Integer currentPage, Integer pageSize, ZoneCourseVo zoneCourseVo);

    /**
     * 改变课程状态
     * @param id
     * @param isDisable
     * @return
     */
    Result changeStatus(String id, Integer isDisable);

    /**
     * 分页查询课程
     * @param pageCurrent
     * @param pageSize
     * @param course
     * @return
     */
    Result findCourse(Integer pageCurrent, Integer pageSize, Course course);

    /**
     * 添加
     * @param zoneCourse
     * @return
     */
    Result addCourseDetail(ZoneCourse zoneCourse);

    /**
     * 删除
     * @param zoneCourse
     * @return
     */
    Result deleteZoneCourse(ZoneCourse zoneCourse);
}