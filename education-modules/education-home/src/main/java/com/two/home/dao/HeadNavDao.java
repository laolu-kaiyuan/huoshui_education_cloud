package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.HeadNav;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/14 3:59
 * @description：
 * @modified By：
 * @version:
 */
public interface HeadNavDao extends BaseMapper<HeadNav> {
}