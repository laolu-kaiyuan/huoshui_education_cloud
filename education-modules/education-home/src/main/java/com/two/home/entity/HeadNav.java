package com.two.home.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * (HeadNav)实体类
 *
 * @author makejava
 * @since 2022-12-14 03:58:34
 */
@Data
@TableName(value = "tb_head_nav")
public class HeadNav implements Serializable {
    private static final long serialVersionUID = 430483667667099190L;
    @TableId(type= IdType.AUTO)
    private Integer headId;
    
    private String headName;
    
    private String href;
    /**
     * 启用状态
     */
    private String isDiable;
    /**
     * 逻辑删除
     */
    private Integer isDelete;

}

