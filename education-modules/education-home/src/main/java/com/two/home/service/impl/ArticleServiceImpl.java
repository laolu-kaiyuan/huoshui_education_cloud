package com.two.home.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.two.core.vo.Result;
import com.two.home.dao.ArticleDao;
import com.two.home.entity.Article;
import com.two.home.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/18 19:09
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleDao,Article> implements ArticleService {
    @Autowired
    private ArticleDao articleDao;
    @Override
    public Result getArticleByNavId(Article article) {
        QueryWrapper<Article> wrapper = new QueryWrapper<>();
        wrapper.eq("nav_id", article.getNavId());
        List<Article> list = articleDao.selectList(wrapper);
        if (ObjectUtil.isNotEmpty(list)) {
            return new Result(2000, "查询成功", list.get(0));
        }
        return new Result(2000, "查询不到",article);
    }

    @Override
    public Result updateArticleByNavId(Article article) {
        if (ObjectUtil.isNotEmpty(article.getId())) {
            article.setGmtModified(LocalDateTime.now());
            int i = articleDao.updateById(article);
            if (i > 0) {
                return new Result(2000, "更改成功");
            }
        } else {
            article.setGmtCreate(LocalDateTime.now());
            int i = articleDao.insert(article);
            if (i > 0) {
                return new Result(2000, "创建成功");
            }
        }
        return new Result(5000, "失败");
    }
}