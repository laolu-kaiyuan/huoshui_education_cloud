package com.two.home.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.vo.Result;
import com.two.home.dao.HeadNavDao;
import com.two.home.entity.HeadNav;
import com.two.home.service.HeadNavService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/14 4:01
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class HeadNavServiceImpl implements HeadNavService {
    @Autowired
    private HeadNavDao headNavDao;
    @Override
    public Result getAllHeadNav(Integer current, Integer pageSize, HeadNav headNav) {
        IPage<HeadNav> page=new Page<>(current,pageSize);
        QueryWrapper<HeadNav> wrapper=new QueryWrapper<>();
        if(StringUtils.hasText(headNav.getHeadName())){
            wrapper.like("head_name",headNav.getHeadName());
        }
        if(headNav.getIsDelete()!=null){
            wrapper.eq("is_delete",headNav.getIsDelete());
        }
        IPage<HeadNav> page1 = headNavDao.selectPage(page, wrapper);
        return new Result(2000,"分页查询成功",page1);
    }

    @Override
    public Result addHeadNav(HeadNav headNav) {
        int i = 0;
        String msg = "";
        if (headNav.getHeadId()!=null){
            i = headNavDao.updateById(headNav);
            msg="修改";
        }else {
            i = headNavDao.insert(headNav);
            msg="添加";
        }
        if (i > 0) {
            return new Result(2000,msg+"成功");
        }
        return new Result(5000,msg+"失败");
    }

    @Override
    public Result deleteById(Integer id) {
        int i = headNavDao.deleteById(id);
        if (i >0) {
            return new Result<>(2000,"删除成功");
        }
        return new Result<>(5000,"删除失败");
    }

    @Override
    public Result changeStatus(Integer id, Integer delete) {
        HeadNav headNav=new HeadNav();
        headNav.setHeadId(id);
        headNav.setIsDelete(delete);
        int i = headNavDao.updateById(headNav);
        if (i > 0) {
            return new Result(2000,"禁用成功");
        }
        return new Result(5000,"禁用失败");
    }

    @Override
    public Result getAllHead() {
        QueryWrapper<HeadNav> wrapper = new QueryWrapper<>();
        wrapper.eq("is_diable", 0);
        wrapper.eq("is_delete", 0);
        List<HeadNav> headNavs = headNavDao.selectList(wrapper);
        return new Result(2000,"查询成功",headNavs);
    }
}