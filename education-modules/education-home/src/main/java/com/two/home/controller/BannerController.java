package com.two.home.controller;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.two.core.vo.Result;
import com.two.home.entity.Banner;
import com.two.home.entity.OssEntity;
import com.two.home.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/15 23:48
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/home/banner")
@Api(tags = "轮播接口类")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @Autowired
    private OssEntity ossEntity;

    @PostMapping("uploadMsg")
    public String upload(MultipartFile file) {
        String filename = file.getOriginalFilename();

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ossEntity.getEndpoint(), ossEntity.getAccessKeyId(), ossEntity.getAccessKeySecret());
        try {
            try {
                String newFileName = UUID.randomUUID() + filename;
                ossClient.putObject(ossEntity.getBucketName(), newFileName, new ByteArrayInputStream(file.getBytes()));
                return "https://" + ossEntity.getBucketName() + "." + ossEntity.getEndpoint() + "/" + newFileName;
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return null;
    }


    @ApiOperation("前台查询专区接口")
    @GetMapping("/getQianBanner")
    public Result getQianBanner(){
        return bannerService.getQianBanner();
    }

    @PostMapping("getAllBanner/{currentPage}/{pageSize}")
    @ApiOperation(value = "根据条件分页查询")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "currentPage",value = "当前页码"),
                    @ApiImplicitParam(name = "pageSize",value = "每页显示的条数")
            }
    )
    private Result getAllBanner(@PathVariable(required = true) Integer currentPage,
                                @PathVariable(required = true) Integer pageSize,
                                @RequestBody Banner banner) {
        return bannerService.getAllBanner(currentPage, pageSize, banner);
    }

    @PostMapping("changeStatus/{id}/{isDisable}")
    @ApiOperation(value = "改变状态")
    public Result changeStatus (@PathVariable String id,
                                @PathVariable Integer isDisable){
        return bannerService.changeStatus(id,isDisable);
    }
    @PostMapping("/addBanner")
    @ApiOperation(value = "添加和修改")
    public Result addBanner(@RequestBody Banner banner){
        return bannerService.addBanner(banner);
    }

    @DeleteMapping("deleteBanner/{id}")
    @ApiOperation(value = "删除")
    public Result deleteBanner(@PathVariable String id){
        return bannerService.deleteById(id);
    }
}