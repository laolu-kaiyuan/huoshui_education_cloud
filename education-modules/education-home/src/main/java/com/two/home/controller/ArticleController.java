package com.two.home.controller;

import com.two.core.vo.Result;
import com.two.home.entity.Article;
import com.two.home.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/18 19:18
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/home/article")
@Api(tags = "文章接口类")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @PostMapping("getArticleByNavId")
    @ApiOperation(value = "根据条件查询、navId")
    public Result getArticleByNavId(@RequestBody Article article){
        return articleService.getArticleByNavId(article);
    }

    @PostMapping("updateArticleByNavId")
    @ApiOperation(value = "添加修改文章")
    public Result updateArticleByNavId(@RequestBody Article article){
        return articleService.updateArticleByNavId(article);
    }
}