package com.two.home.vo;

import lombok.Data;

import java.util.List;

@Data
public class LoginVo {
    private String mobile;
    private String nickname;
    private String isDisabled;
    private List<String> gmtCreate;
    private String qianPassword;
}