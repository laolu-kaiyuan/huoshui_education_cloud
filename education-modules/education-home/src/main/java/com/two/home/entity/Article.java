package com.two.home.entity;

import cn.hutool.core.annotation.MirroredAnnotationAttribute;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.io.Serializable;

/**
 * (Article)实体类
 *
 * @author makejava
 * @since 2022-12-18 19:04:29
 */
@Data
@TableName("tb_article")
public class Article implements Serializable {
    private static final long serialVersionUID = 393409982506728925L;
    /**
     * 主键
     */
    @TableId
    private String id;
    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;
    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;
    /**
     * 状态（1有效，0无效）
     */
    private Integer isDeleted;
    /**
     * 导航ID
     */
    private String navId;
    /**
     * 文章标题
     */
    private String artTitle;
    /**
     * 文章图片
     */
    private String artPic;
    /**
     * 文章描述
     */
    private String artDesc;

}

