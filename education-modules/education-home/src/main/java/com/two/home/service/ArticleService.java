package com.two.home.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.two.core.vo.Result;
import com.two.home.entity.Article;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/18 19:07
 * @description：
 * @modified By：
 * @version:
 */
public interface ArticleService extends IService<Article> {
    Result getArticleByNavId(Article article);

    Result updateArticleByNavId(Article article);
}