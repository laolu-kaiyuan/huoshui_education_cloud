package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.Course;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/23 13:56
 * @description：
 * @modified By： * @version:
 */
public interface CourseDao extends BaseMapper<Course> {
}