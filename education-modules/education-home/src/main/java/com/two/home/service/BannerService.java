package com.two.home.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.two.core.vo.Result;
import com.two.home.entity.Banner;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/15 23:55
 * @description：
 * @modified By： * @version:
 */
public interface BannerService extends IService<Banner> {

    Result getAllBanner(Integer currentPage, Integer pageSize, Banner banner);

    Result changeStatus(String id, Integer isDisable);

    Result addBanner(Banner banner);

    Result deleteById(String id);

    Result getQianBanner();
}