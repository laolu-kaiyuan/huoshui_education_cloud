package com.two.home.controller;

import com.two.core.vo.Result;
import com.two.home.entity.Course;
import com.two.home.entity.Zone;
import com.two.home.entity.ZoneCourse;
import com.two.home.service.ZoneCourseService;
import com.two.home.vo.ZoneCourseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/12 17:23
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/home/zone-course")
@Api(tags = "专区课程接口类")
public class ZoneCourseController {
    @Autowired
    private ZoneCourseService zoneCourseService;

    @PostMapping("getAllDetail/{currentPage}/{pageSize}")
    @ApiOperation(value = "根据条件分页查询专区课程")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "currentPage",value = "当前页码"),
                    @ApiImplicitParam(name = "pageSize",value = "每页显示的条数")
            }
    )
    public Result getAllZone(@PathVariable(required = true) Integer currentPage,
                                           @PathVariable(required =true ) Integer pageSize,
                                           @RequestBody ZoneCourseVo zoneCourseVo){
        return zoneCourseService.getAllZoneCourse(currentPage,pageSize,zoneCourseVo);
    }
    @PostMapping("findCourse/{pageCurrent}/{pageSize}")
    @ApiOperation(value = "分页查询所有专区课程")
    public Result findCourse(@PathVariable Integer pageCurrent,@PathVariable Integer pageSize,@RequestBody Course course){
        return zoneCourseService.findCourse(pageCurrent,pageSize,course);
    }
    @PostMapping("addCourseDetail")
    @ApiOperation(value = "添加专区课程")
    public Result addCourseDetail(@RequestBody ZoneCourse zoneCourse){
        return zoneCourseService.addCourseDetail(zoneCourse);
    }

    @PostMapping("deleteZoneCourse")
    @ApiOperation(value = "删除")
    public Result deleteZoneCourse(@RequestBody ZoneCourse zoneCourse){
        return zoneCourseService.deleteZoneCourse(zoneCourse);
    }
    @PostMapping("changeStatus/{id}/{isDisable}")
    @ApiOperation(value = "改变状态")
    public Result changeStatus(@PathVariable String id,@PathVariable Integer isDisable){
        return zoneCourseService.changeStatus(id,isDisable);
    }
}