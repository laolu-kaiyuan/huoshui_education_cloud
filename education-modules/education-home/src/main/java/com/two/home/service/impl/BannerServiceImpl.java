package com.two.home.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.two.core.vo.Result;
import com.two.home.dao.BannerDao;
import com.two.home.entity.Banner;
import com.two.home.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/15 23:56
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class BannerServiceImpl extends ServiceImpl<BannerDao, Banner> implements BannerService {
    @Autowired
    BannerDao bannerDao;
    @Override
    public Result getAllBanner(Integer currentPage, Integer pageSize, Banner banner) {
        Page<Banner> page = new Page<>(currentPage,pageSize);
        QueryWrapper<Banner> wrapper = new QueryWrapper<>();
        if (StringUtils.hasText(banner.getTitle())){
            wrapper.like("title",banner.getTitle());
        }
        if (ObjectUtil.isNotEmpty(banner.getIsDisable())){
            wrapper.eq("is_disable",banner.getIsDisable());
        }
        Page<Banner> page1 = bannerDao.selectPage(page, wrapper);
        return new Result(2000,"分页查询成功",page1);
    }

    @Override
    public Result changeStatus(String id, Integer isDisable) {
        Banner banner = new Banner();
        banner.setId(id);
        banner.setIsDisable(isDisable);
        int res = bannerDao.updateById(banner);
        if (res>0){
            return new Result(2000,"修改成功");
        }
        return new Result(5000,"修改失败");
    }

    @Override
    public Result addBanner(Banner banner) {
        banner.setGmtCreate(LocalDateTime.now());
        int i = 0;
        String msg = "";
        if (banner.getId()!=null){
            banner.setGmtModified(LocalDateTime.now());
            i = bannerDao.updateById(banner);
            msg="修改";
        }else {
            banner.setGmtCreate(LocalDateTime.now());
            banner.setGmtModified(LocalDateTime.now());
            i = bannerDao.insert(banner);
            msg="添加";
        }
        if (i > 0) {
            return new Result(2000,msg+"成功");
        }
        return new Result(5000,msg+"失败");
    }

    @Override
    public Result deleteById(String id) {
        int i = bannerDao.deleteById(id);
        if (i >0) {
            return new Result<>(2000,"删除成功");
        }
        return new Result<>(5000,"删除失败");
    }

    @Override
    public Result getQianBanner() {
        QueryWrapper<Banner> wrapper = new QueryWrapper<>();
        wrapper.eq("is_disable", 0);
        wrapper.eq("is_deleted", 0);
        wrapper.orderByAsc("sort");
        List<Banner> list = bannerDao.selectList(wrapper);

        return new Result<>(2000,"查询成功",list);
    }
}