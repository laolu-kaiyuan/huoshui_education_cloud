package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.Member;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2023/1/3 0:01
 * @description：
 * @modified By：
 * @version:
 */
public interface MemberDao extends BaseMapper<Member> {
}