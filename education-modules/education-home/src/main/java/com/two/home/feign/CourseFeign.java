package com.two.home.feign;


import com.two.core.entity.course.EduSubject;
import com.two.core.vo.Result;
import com.two.home.entity.Course;
import com.two.home.vo.ZoneCourseVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/21 12:55
 * @description：
 * @modified By：
 * @version:
 */
@FeignClient("education-core")
public interface CourseFeign {
    @PostMapping("/core/course/getCourseByIds")
    Result<List<Course>> getCourseByIds(@RequestBody List<String> Ids);

    @GetMapping("/core/subject/getAllQianMenu")
    Result<List<EduSubject>> getAllQianMenu();
}