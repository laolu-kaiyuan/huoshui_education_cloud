package com.two.home.service;

import com.two.core.vo.Result;
import com.two.home.entity.HeadNav;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/14 4:00
 * @description：
 * @modified By：
 * @version:
 */
public interface HeadNavService {
    /**
     * 分页查询头部导航
     * @param current
     * @param pageSize
     * @param HeadNav
     * @return
     */
    Result getAllHeadNav(Integer current, Integer pageSize, HeadNav HeadNav);

    /**
     * 添加头部导航
     * @param HeadNav
     * @return
     */
    Result addHeadNav(HeadNav HeadNav);

    /**
     * 删除头部导航
     * @param id
     * @return
     */
    Result deleteById(Integer id);

    /**
     * 改变头部导航状态
     * @param id
     * @param delete
     * @return
     */
    Result changeStatus(Integer id,Integer delete);

    /**
     * 前台获取头
     * @return
     */
    Result getAllHead();
}