package com.two.home.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "tb_member")
public class Member {
    /**
     * 会员id
     */
    @TableId
    private String id;
    /**
     * 微信openid
     */
    private String openid;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别 女(1) 男(0)
     */
    private Integer sex;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 个性签名
     */
    private String sign;
    /**
     * 会员类型
     */
    private Integer isType;
    /**
     * 会员状态 已禁用(1) 未禁用(0)
     */
    private Integer isDisabled;
    /**
     * 逻辑删除 已删除(1) 未删除(0)
     */
    @TableLogic(value = "0",delval = "1")
    private Integer isDeleted;
    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;
    /**
     * 更新时间
     */
    private LocalDateTime gmtModified;
    /**
     * 备注
     */
    private String remark;
    /**
     * 盐值
     */
    private String salt;
}