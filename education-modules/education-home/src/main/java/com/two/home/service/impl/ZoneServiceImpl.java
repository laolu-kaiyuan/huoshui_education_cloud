package com.two.home.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.entity.AclRole;
import com.two.core.entity.AclUserRole;
import com.two.core.vo.Result;
import com.two.home.dao.CourseDao;
import com.two.home.dao.ZoneCourseDao;
import com.two.home.dao.ZoneDao;
import com.two.home.entity.Course;
import com.two.home.entity.Zone;
import com.two.home.entity.ZoneCourse;
import com.two.home.service.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * 专区(Zone)表服务实现类
 *
 * @author lushimin
 * @since 2022-12-12 04:27:07
 */
@Service
public class ZoneServiceImpl implements ZoneService {

    @Autowired
    private ZoneDao zoneDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private ZoneCourseDao zoneCourseDao;

    @Override
    @Transactional
    public Result getAllQianZone() {
        QueryWrapper<Zone> wrapper = new QueryWrapper<>();
        wrapper.eq("is_deleted","0");
        wrapper.eq("is_disable","0");
        List<Zone> zones = zoneDao.selectList(wrapper);
        for (Zone zone : zones) {
            //暂时没远程调用
            QueryWrapper<ZoneCourse> wrapper3=new QueryWrapper<>();
            wrapper3.eq("zone_id",zone.getId());
            List<ZoneCourse> zoneCourses = zoneCourseDao.selectList(wrapper3);
            List<String> ids = new ArrayList<>();
            for (ZoneCourse zoneCours : zoneCourses) {
                ids.add(zoneCours.getCourseId());
            }
            QueryWrapper<Course> wrapper2 = new QueryWrapper<>();
            wrapper2.in("id",ids);
            List<Course> courses = courseDao.selectList(wrapper2);
            zone.setCourseList(courses);
        }

        return new Result(2000,"查询成功",zones);
    }

    @Override
    public Result getAllZone(Integer current, Integer pageSize, Zone zone) {
        IPage<Zone> page=new Page<>(current,pageSize);
        QueryWrapper<Zone> wrapper=new QueryWrapper<>();
        if(StringUtils.hasText(zone.getZoneName())){
            wrapper.like("zone_name",zone.getZoneName());
        }
        if(zone.getIsDeleted()!=null){
            wrapper.eq("is_deleted",zone.getIsDeleted());
        }
        IPage<Zone> page1 = zoneDao.selectPage(page, wrapper);
        return new Result(2000,"分页查询成功",page1);
    }

    @Override
    public Result addZone(Zone zone) {
        zone.setGmtCreate(LocalDateTime.now());
        int i = 0;
        String msg = "";
        if (zone.getId()!=null){
            zone.setGmtModified(LocalDateTime.now());
            i = zoneDao.updateById(zone);
            msg="修改";
        }else {
            zone.setGmtCreate(LocalDateTime.now());
            zone.setGmtModified(LocalDateTime.now());
            i = zoneDao.insert(zone);
            msg="添加";
        }
        if (i > 0) {
            return new Result(2000,msg+"成功");
        }
        return new Result(5000,msg+"失败");
    }

    @Override
    public Result deleteById(String id) {
        int i = zoneDao.deleteById(id);
        if (i >0) {
            return new Result<>(2000,"删除成功");
        }
        return new Result<>(5000,"删除失败");
    }

    @Override
    public Result changeStatus(String id, Integer delete) {
        Zone zone=new Zone();
        zone.setId(id);
        zone.setIsDeleted(delete);
        int i = zoneDao.updateById(zone);
        if (i > 0) {
            return new Result(2000,"禁用成功");
        }
        return new Result(5000,"禁用失败");
    }
}
