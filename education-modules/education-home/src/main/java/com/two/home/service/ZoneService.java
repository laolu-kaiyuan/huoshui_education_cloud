package com.two.home.service;


import com.two.core.vo.Result;
import com.two.home.entity.Zone;
import org.yaml.snakeyaml.events.Event;


/**
 * 专区(Zone)表服务接口
 *
 * @author lushimin
 * @since 2022-12-12 04:27:00
 */
public interface ZoneService {
    /**
     * 前台获取专区
     * @return
     */
    Result getAllQianZone();
    /**
     * 分页查询专区
     * @param current
     * @param pageSize
     * @param zone
     * @return
     */
    Result getAllZone(Integer current,Integer pageSize,Zone zone);

    /**
     * 添加专区
     * @param zone
     * @return
     */
    Result addZone(Zone zone);

    /**
     * 删除专区
     * @param id
     * @return
     */
    Result deleteById(String id);

    /**
     * 改变专区状态
     * @param id
     * @param delete
     * @return
     */
    Result changeStatus(String id,Integer delete);
}
