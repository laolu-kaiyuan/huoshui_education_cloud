package com.two.home.controller;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.two.core.vo.Result;
import com.two.home.entity.Link;
import com.two.home.entity.OssEntity;
import com.two.home.service.LinkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/15 23:48
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("/home/link")
@Api(tags = "链接接口类")
public class LinkController {
    @Autowired
    private LinkService linkService;

    @PostMapping("getAllLink/{currentPage}/{pageSize}")
    @ApiOperation(value = "根据条件分页查询")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "currentPage",value = "当前页码"),
                    @ApiImplicitParam(name = "pageSize",value = "每页显示的条数")
            }
    )
    private Result getAllLink(@PathVariable(required = true) Integer currentPage,
                              @PathVariable(required = true) Integer pageSize,
                              @RequestBody Link link) {
        return linkService.getAllLink(currentPage, pageSize, link);
    }

    @PostMapping("changeStatus/{id}/{isDisable}")
    @ApiOperation(value = "改变状态")
    public Result changeStatus (@PathVariable String id,
                                @PathVariable Integer isDisable){
        return linkService.changeStatus(id,isDisable);
    }
    @PostMapping("/addLink")
    @ApiOperation(value = "添加和修改")
    public Result addLink(@RequestBody Link link){
        return linkService.addLink(link);
    }

    @DeleteMapping("deleteLink/{id}")
    @ApiOperation(value = "删除")
    public Result deleteLink(@PathVariable String id){
        return linkService.deleteById(id);
    }
    @GetMapping("/getQianLink")
    @ApiOperation(value = "前台链接")
    public Result getQianLink(){
        return linkService.getQianLink();
    }
}