package com.two.home.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.two.core.util.JWTUtil;
import com.two.core.vo.Result;
import com.two.home.dao.MemberDao;
import com.two.home.entity.Member;
import com.two.home.service.LoginService;
import com.two.home.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2023/1/2 23:57
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Result qianLogin(LoginVo memberVo) {
        QueryWrapper<Member> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", memberVo.getMobile());
        Member member = memberDao.selectOne(wrapper);

        String password = member.getPassword();
        boolean matches = passwordEncoder.matches(memberVo.getQianPassword(), password);
        if (matches) {
            Map<String, Object> map = new HashMap<>();
            map.put("mobile", memberVo.getMobile());
            map.put("nickname", member.getNickname());
            String jwt = JWTUtil.createJWT(map);

            Map<String, Object> result = new HashMap<>();
            result.put("homeToken", jwt);
            result.put("mobile", memberVo.getMobile());
            result.put("memberId",member.getId());
            if(member.getIsType()==1){
                result.put("isTeacher","1");
                return new Result(2000, "登录成功", result);
            }
            if(member.getIsType()==0){
                result.put("isTeacher","0");
                return new Result(2000, "登录成功", result);
            }

        }
        return new Result(5000, "用户名或密码错误！");
    }
}
