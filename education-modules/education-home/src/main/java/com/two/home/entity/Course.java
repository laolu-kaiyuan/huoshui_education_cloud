package com.two.home.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.io.Serializable;

/**
 * 课程(Course)实体类
 *
 * @author makejava
 * @since 2022-12-23 13:50:29
 */
@Data
@TableName(value = "edu_course")
public class Course implements Serializable {
    private static final long serialVersionUID = 688622428588142012L;
    /**
     * 课程ID
     */
    @TableId
    private String id;
    /**
     * 课程讲师ID
     */
    private String teacherId;
    /**
     * 课程专业ID
     */
    private String subjectId;
    /**
     * 课程专业父级ID
     */
    private String subjectParentId;
    /**
     * 课程标题
     */
    private String title;
    /**
     * 课程销售价格，设置为0则可免费观看
     */
    private String price;
    /**
     * 总课时
     */
    private String lessonNum;
    /**
     * 课程封面图片路径
     */
    private String cover;
    /**
     * 销售数量
     */
    private String buyCount;
    /**
     * 浏览数量
     */
    private String viewCount;
    /**
     * 乐观锁
     */
    private String version;
    /**
     * 课程状态 Draft未发布  Normal已发布
     */
    private String status;
    /**
     * 逻辑删除 1（true）已删除， 0（false）未删除
     */
    private Integer isDeleted;
    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;
    /**
     * 更新时间
     */
    private LocalDateTime gmtModified;
    /**
     * 课程备注
     */
    private String remark;
    /**
     * 是否添加 0 否  1 是
     */
    private Integer isAdd;

}

