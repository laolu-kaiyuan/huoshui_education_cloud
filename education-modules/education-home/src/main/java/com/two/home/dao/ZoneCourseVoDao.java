package com.two.home.dao;

import com.two.home.vo.ZoneCourseVo;

import java.util.List;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/14 2:39
 * @description：
 * @modified By： * @version:
 */
public interface ZoneCourseVoDao {
    List<ZoneCourseVo> queryAll(String id);
}