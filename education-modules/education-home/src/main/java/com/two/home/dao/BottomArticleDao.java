package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.BottomArticle;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/15 23:54
 * @description：
 * @modified By： * @version:
 */
public interface BottomArticleDao extends BaseMapper<BottomArticle> {
}