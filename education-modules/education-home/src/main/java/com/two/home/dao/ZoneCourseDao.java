package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.ZoneCourse;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/12 17:19
 * @description：
 * @modified By： * @version:
 */
public interface ZoneCourseDao extends BaseMapper<ZoneCourse> {
}