package com.two.home.controller;

import com.two.core.vo.Result;

import com.two.home.service.LoginService;
import com.two.home.vo.LoginVo;
import com.two.security.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2023/1/2 23:52
 * @description：
 * @modified By：
 * @version:
 */
@RequestMapping("/sso/admin")
@RestController
public class LoginController {
    @Autowired
    private LoginService loginService;
    @PostMapping("qianLogin")
    public Result qianLogin(@RequestBody LoginVo loginVo) {
        return loginService.qianLogin(loginVo);
    }
    /**
     * 退出登录的方法
     * @return
     */
    @GetMapping("loginOut")
    public Result loginOut(){
//        HttpServletRequest request = WebUtils.getRequest();
//        String token = request.getHeader("token");
//        if (token!=null) {
//            redisTemplate.delete(token);
//        }
        return new Result(2000,"退出成功",null);
    }
}