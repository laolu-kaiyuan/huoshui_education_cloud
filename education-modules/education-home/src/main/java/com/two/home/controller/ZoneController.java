package com.two.home.controller;

import com.two.core.vo.Result;
import com.two.home.entity.Zone;
import com.two.home.feign.CourseFeign;
import com.two.home.service.ZoneService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 专区(Zone)表控制层
 *
 * @author lushimin
 * @since 2022-12-12 04:26:59
 */
@RestController
@RequestMapping("/home/zone")
@Api(tags = "专区接口类")
public class ZoneController {
    @Autowired
    private ZoneService zoneService;
    @Autowired
    private CourseFeign courseFeign;

    @ApiOperation("前台查询专区接口")
    @GetMapping("/getAllQianZone")
    public Result getAllQianZone(){
        return zoneService.getAllQianZone();
    }

    @PostMapping("getAllZone/{currentPage}/{pageSize}")
    @ApiOperation(value = "根据条件分页查询专区")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "currentPage",value = "当前页码"),
                    @ApiImplicitParam(name = "pageSize",value = "每页显示的条数")
            }
    )
    public Result<Zone> getAllZone(@PathVariable(required = true) Integer currentPage,
                                   @PathVariable(required =true ) Integer pageSize,
                                   @RequestBody Zone zone){
        return zoneService.getAllZone(currentPage,pageSize,zone);
    }
    @PostMapping("/addZone")
    @ApiOperation(value = "添加和修改专区")
    public Result addZone(@RequestBody Zone zone){
        return zoneService.addZone(zone);
    }
    @DeleteMapping("deleteZone/{id}")
    @ApiOperation(value = "删除导航")
    public Result deleteById(@PathVariable String id){
        return zoneService.deleteById(id);
    }

    @ApiOperation(value = "改变专区状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "专区id"),
            @ApiImplicitParam(name = "deleted",value = "状态")})
    @PostMapping("changeStatus/{id}/{deleted}")
    public Result changeStatus(@PathVariable String id,
                                       @PathVariable Integer deleted){
        return zoneService.changeStatus(id,deleted);
    }
    @GetMapping("/getAllQianMenu")
    @ApiOperation(value = "banner中的课程菜单")
    public Result getAllQianMenu(){
        return courseFeign.getAllQianMenu();
    }

}

