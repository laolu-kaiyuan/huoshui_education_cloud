package com.two.home.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * (BottomArticle)实体类
 *
 * @author makejava
 * @since 2022-12-18 09:38:06
 */
@Data
@TableName(value = "tb_bottom_article")
public class BottomArticle implements Serializable {
    private static final long serialVersionUID = 675913220526366128L;
    /**
     * 主键
     */
    @TableId
    private String id;
    /**
     * 导航名称
     */
    private String name;
    /**
     * 启用状态
     */
    private Integer isDisable;
    /**
     * 逻辑删除
     */
    private Integer isDeleted;
    /**
     * 父类id
     */
    private String parentId;
    /**
     * 创建时间
     */
    //从后端返回json数据到前端页面的时候使用
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    //从前端页面传值到controller的时候，自动解析字符串日期到date
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gmtCreate;
    /**
     * 更新时间
     */
    //从后端返回json数据到前端页面的时候使用
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    //从前端页面传值到controller的时候，自动解析字符串日期到date
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gmtModified;

    @TableField(exist = false)
    private List<BottomArticle> children;

}

