package com.two.home.service;

import com.two.core.vo.Result;
import com.two.home.vo.LoginVo;

/**
 * @author ：lushimin
 * @date ：Created in 2023/1/2 23:56
 * @description：
 * @modified By： * @version:
 */
public interface LoginService {
    Result qianLogin(LoginVo memberVo);
}