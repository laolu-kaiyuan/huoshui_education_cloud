package com.two.home.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * 专区(Zone)实体类
 *
 * @author lushimin
 * @since 2022-12-12 04:27:00
 */
@Data
@TableName(value = "tb_zone")
public class Zone implements Serializable {
    private static final long serialVersionUID = 174178702825230738L;
    /**
     * 主键
     */
    @TableId
    private String id;
    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;
    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;
    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer isDeleted;
    /**
     * 排序
     */
    private Integer isDisable;
    /**
     * 名称
     */
    private String zoneName;
    /**
     * 描述
     */
    private String zoneDesc;

    @TableField(exist = false)
    private List<Course> courseList;

}

