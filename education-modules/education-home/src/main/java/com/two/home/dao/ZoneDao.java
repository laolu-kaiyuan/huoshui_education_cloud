package com.two.home.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.two.home.entity.Zone;

/**
 * 专区(Zone)表数据库访问层
 *
 * @author lushimin
 * @since 2022-12-12 04:27:00
 */
public interface ZoneDao extends BaseMapper<Zone> {
}

