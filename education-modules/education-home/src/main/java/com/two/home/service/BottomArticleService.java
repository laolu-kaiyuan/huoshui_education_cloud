package com.two.home.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.two.core.vo.Result;
import com.two.home.entity.BottomArticle;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/15 23:55
 * @description：
 * @modified By： * @version:
 */
public interface BottomArticleService extends IService<BottomArticle> {

    Result getAllBottomArticle(Integer currentPage, Integer pageSize, BottomArticle bottomArticle);

    Result changeStatus(String id, Integer isDisable);

    Result addBottomArticle(BottomArticle bottomArticle);

    Result deleteById(String id);

    Result getQianAllBottomArticle();
}