package com.two.home.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.two.core.vo.Result;
import com.two.home.dao.CourseDao;
import com.two.home.dao.ZoneCourseDao;
import com.two.home.dao.ZoneCourseVoDao;
import com.two.home.entity.Course;
import com.two.home.entity.Zone;
import com.two.home.entity.ZoneCourse;
import com.two.home.feign.CourseFeign;
import com.two.home.service.ZoneCourseService;
import com.two.home.vo.ZoneCourseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/12 17:22
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class ZoneCourseServiceImpl implements ZoneCourseService {

    @Autowired
    private ZoneCourseVoDao zoneCourseVoDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private ZoneCourseDao zoneCourseDao;
    @Autowired
    private CourseFeign courseFeign;
    @Override
    public Result getAllZoneCourse(Integer currentPage, Integer pageSize, ZoneCourseVo zoneCourseVo) {
        Page<ZoneCourse> page=new Page<>(currentPage, pageSize);
        QueryWrapper<ZoneCourse> wrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(zoneCourseVo.getZoneId())) {
            wrapper.eq("zone_id", zoneCourseVo.getZoneId());
        }
        if (ObjectUtil.isNotEmpty(zoneCourseVo.getIsDisable())) {
            wrapper.eq("is_disable", zoneCourseVo.getIsDisable());
        }
        Page<ZoneCourse> zoneCoursePage = zoneCourseDao.selectPage(page, wrapper);
        List<ZoneCourse> zoneCourses = zoneCoursePage.getRecords();
        //课程id
        List<String> courseIds = new ArrayList<>();
        //遍历课程结果，将该专区下的课程id封装到list集合中
        for (ZoneCourse zoneCourse : zoneCourses) {
            String courseId = zoneCourse.getCourseId();
            courseIds.add(courseId);
        }
//        Result<List<Course>> result = courseFeign.getCourseByIds(courseIds);
        if(courseIds.size()==0){
            return new Result(2000, "暂无数据",zoneCoursePage);
        }
        QueryWrapper<Course> wrapper2 = new QueryWrapper<>();
        wrapper2.in("id",courseIds);
        List<Course> courses = courseDao.selectList(wrapper2);
        Result<List<Course>> result = new Result<>(2000, "", courses);

        //如果查询成功
        if (result.getCode() == 2000) {
            //拿到返回的结果
            List<Course> data = result.getData();
            //此集合封装返回的data数据
            List<ZoneCourseVo> list = new ArrayList<>();
            if (ObjectUtil.isNotEmpty(data)) {
                //遍历结果
                for (Course datum : data) {
                    //封装每一条数据
                    ZoneCourseVo zcv = new ZoneCourseVo();
                    for (ZoneCourse record : zoneCourses) {
                        if (record.getCourseId().equals(datum.getId())) {
                            //设置为结果状态和id
                            zcv.setIsDisable(record.getIsDisable());
                            zcv.setId(record.getId());
                        }
                    }
                    zcv.setZoneId(datum.getId());
                    zcv.setCourseTitle(datum.getTitle());
                    zcv.setParentname(datum.getSubjectId());
                    zcv.setSubjecttitle(datum.getSubjectParentId());
                    zcv.setGmtCreate(datum.getGmtCreate());
                    list.add(zcv);
                }
            }
            com.two.core.vo.Page page2=new com.two.core.vo.Page();
            List<ZoneCourse> all = zoneCourseDao.selectList(wrapper);
            page2.setPageSize(pageSize);
            page2.setTotal(all.size());
            page2.setRecords(list);
            return new Result(2000, "查询成功", page2);
        }
        return new Result(5000,"");

//        List<ZoneCourseVo> zoneCourseVos = zoneCourseVoDao.queryAll(zoneCourseVo.getZoneId());
//
//        return new Result(2000,"分页查询成功",zoneCourseVos);
    }

    @Override
    public Result changeStatus(String id, Integer isDisable) {
        ZoneCourse zoneCourse = new ZoneCourse();
        zoneCourse.setId(id);
        zoneCourse.setIsDisable(isDisable);
        int i = zoneCourseDao.updateById(zoneCourse);
        if (i > 0) {
            return new Result(2000, "成功");
        }
        return new Result(5000, "失败");
    }

    @Override
    public Result findCourse(Integer pageCurrent, Integer pageSize, Course course) {
//        Result<Page<Course>> result = courseClints.findCourse(pageCurrent, pageSize, course);
        Page<Course> page = new Page<>(pageCurrent,pageSize);
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
//        wrapper.eq("subject_parent_id",course.getSubjectParentId());
        wrapper.eq("status","Normal");
        Page<Course> coursePage = courseDao.selectPage(page, wrapper);
        return new Result(2000,"成功",coursePage);
    }

    @Override
    @Transactional
    public Result addCourseDetail(ZoneCourse zoneCourse) {
        Course course = new Course();
        course.setIsAdd(1);
        course.setId(zoneCourse.getCourseId());
//        Result result = courseFeign.insert(course);
        int insert = courseDao.updateById(course);
        Result<Integer> result = new Result<>(2000, "", insert);

        if (result.getCode()==2000){
            int i = zoneCourseDao.insert(zoneCourse);
            if (i > 0) {
                return new Result(2000, "添加成功", true);
            }
        }
        return new Result(5000, "添加失败", false);
    }

    @Override
    @Transactional
    public Result deleteZoneCourse(ZoneCourse zoneCourse) {
        ZoneCourse zs = zoneCourseDao.selectById(zoneCourse.getId());
        Course course = new Course();
        course.setId(zs.getCourseId());
        course.setIsAdd(0);
//        Result result = courseFeign.updateIsAdd(course);
        UpdateWrapper<Course> update = new UpdateWrapper<>();
        update.eq("id", course.getId());
        int update1 = courseDao.update(course, update);
        Result<Integer> result = new Result<>(2000, "", update1);

        if (result.getCode()==2000){
            int res = zoneCourseDao.deleteById(zoneCourse.getId());
            if (res>0){
                return new Result(2000,"删除成功",true);
            }
        }
        return new Result(5000,"删除失败",false);
    }

}