package com.two.home.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.two.core.vo.Result;
import com.two.home.entity.Link;

/**
 * @author ：lushimin
 * @date ：Created in 2022/12/15 23:55
 * @description：
 * @modified By： * @version:
 */
public interface LinkService extends IService<Link> {

    Result getAllLink(Integer currentPage, Integer pageSize, Link link);

    Result changeStatus(String id, Integer isDisable);

    Result addLink(Link link);

    Result deleteById(String id);

    Result getQianLink();
}