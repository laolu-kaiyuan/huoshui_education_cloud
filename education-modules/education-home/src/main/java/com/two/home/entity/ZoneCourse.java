package com.two.home.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 专区(Zone)实体类
 *
 * @author lushimin
 * @since 2022-12-12 04:27:00
 */
@Data
@TableName(value = "tb_zone_course")
public class ZoneCourse implements Serializable {
    private static final long serialVersionUID = 174178702825230738L;
    /**
     * 主键
     */
    @TableId
    private String id;
    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;
    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;
    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer isDeleted;
    /**
     * 排序
     */
    private Integer isDisable;
    /**
     * 专区
     */
    private String zoneId;
    /**
     * 课程
     */
    private String courseId;

}

