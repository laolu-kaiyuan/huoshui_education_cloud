package com.two.gatewayfont;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/23 11:04
 * @description：
 * @modified By：
 * @version:
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class GatewayFontApp {
    public static void main(String[] args) {
        SpringApplication.run(GatewayFontApp.class,args);
    }
}