package com.two.gateway.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/11/22 21:44
 * @description：统一跨域处理
 * @modified By：
 * @version:
 */
@Configuration
public class MyCorsConfiguration {

    @Bean
    public CorsWebFilter corsFilter() {
        // 初始化cors配置对象
        CorsConfiguration config = new CorsConfiguration();
        //设置允许哪些url访问跨域资源,*表示全部允许
        config.addAllowedOrigin("*");
        //允许访问的头信息,*表示全部
        config.addAllowedHeader("*");
        //允许所有请求方法(GET,POST等)访问跨域资源
        config.addAllowedMethod("*");
        // 预检请求的缓存时间（秒），即在这个时间内，对于相同的跨域请求不会再预检了
        config.setMaxAge(18000L);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());
        source.registerCorsConfiguration("/**", config);
        return new CorsWebFilter(source);
    }

}