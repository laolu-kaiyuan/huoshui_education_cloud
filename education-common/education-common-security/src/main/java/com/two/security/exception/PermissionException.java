package com.two.security.exception;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/2 23:27
 * @description：
 * @modified By：
 * @version:
 */
//自定义异常类
public class PermissionException extends RuntimeException {

    public PermissionException(){
        super();
    }
    public PermissionException(String msg){
        super(msg);
    }
}