package com.two.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//该注解使用在哪个位置。
@Target(value=ElementType.METHOD)
//该注解什么有效。
@Retention(value = RetentionPolicy.RUNTIME)
public @interface RequiresPermissions {
    //注解中的属性
    String value();
}
