package com.two.security.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/2 23:18
 * @description：
 * @modified By：
 * @version:
 */
public class WebUtils {
    /** 获取request对象 **/
    public static HttpServletRequest getRequest(){

        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null){

            return null;
        }
        return ((ServletRequestAttributes)requestAttributes).getRequest();
    }
    /** 获取response对象 **/
    public static HttpServletResponse getResponse(){

        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null){

            return null;
        }
        return ((ServletRequestAttributes)requestAttributes).getResponse();
    }
}