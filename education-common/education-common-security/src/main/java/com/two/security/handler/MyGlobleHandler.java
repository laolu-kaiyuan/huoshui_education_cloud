package com.two.security.handler;

import com.two.core.vo.Result;
import com.two.security.exception.PermissionException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/2 23:39
 * @description：
 * @modified By：
 * @version:
 */
@RestControllerAdvice //标记该类为全局异常处理类
public class MyGlobleHandler {

    //如果你抛出PermissionException，则由该方法处理这个异常
    @ExceptionHandler(value = PermissionException.class)
    public Result<String> permissonException(PermissionException e){
        return new Result<String>(4001,e.getMessage());
    }
}