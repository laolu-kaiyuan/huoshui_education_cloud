package com.two.core.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/7 16:21
 * @description：
 * @modified By：
 * @version:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T> {
    private Integer total;
    private Integer pageSize;
    private T records;
}