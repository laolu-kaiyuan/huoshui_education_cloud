package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/11 13:07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "edu_teacher")
public class EduTeacher {
    @TableId
    private String id;

    private String name;

    private String intro;

    private String career;

    private Integer level;

    private String avatar;

    private Integer sort;

    private boolean is_deleted;

    private LocalDateTime gmtCreate;

    private LocalDateTime gmtModified;

    private String mobile;

    private String email;

    private String status;
}
