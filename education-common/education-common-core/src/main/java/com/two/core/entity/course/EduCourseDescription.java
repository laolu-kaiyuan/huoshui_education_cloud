package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 15:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "edu_course_description")
public class EduCourseDescription {
    /**
     * 课程id
     */
    private String id;
    /**
     *课程描述
     */
    private String description;
    /**
     *生成时间
     */
    private LocalDateTime gmtCreate;
    /**
     *修改时间
     */
    private LocalDateTime gmtModified;
}
