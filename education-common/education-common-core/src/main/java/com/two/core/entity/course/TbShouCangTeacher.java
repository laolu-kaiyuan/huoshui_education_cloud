package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 16:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_shoucang_teacher")
public class TbShouCangTeacher {
    @TableId
    private String id;

    private String memberId;

    private String teacherId;
}
