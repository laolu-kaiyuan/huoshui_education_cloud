package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 18:45
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EduTbCheckCourse {
    /**
     * 课程ID
     */
    @TableId
    private String id;
    /**
     *课程讲师ID
     */
    private String teacherId;
    /**
     *课程专业ID
     */
    private String subjectId;
    /**
     *课程专业父级ID
     */
    private String subjectParentId;
    /**
     *课程标题
     */
    private String title;
    /**
     *课程销售价格，设置为0则可免费观看
     */
    private BigDecimal price;
    /**
     *总课时
     */
    private Integer lessonNum;
    /**
     *课程封面图片路径
     */
    private String cover;
    /**
     *销售数量
     */
    private Long buyCount;
    /**
     *浏览数量
     */
    private Long viewCount;
    /**
     *乐观锁
     */
    private Long version;
    /**
     *课程状态 Draft未发布  Normal已发布
     */
    private String status;
    /**
     *逻辑删除 1（true）已删除， 0（false）未删除
     */
    private String isDeleted;
    /**
     *创建时间
     */
    private LocalDateTime gmtCreate;
    /**
     *更新时间
     */
    private LocalDateTime gmtModified;
    /**
     *课程备注
     */
    private String remark;
    /**
     *是否添加 0 否  1 是
     */
    private Integer isAdd;
    /**
     * 所属课程分类
     */
    private String subjectName;
    /**
     * 课程描述
     */
    private String description;
    /**
     * 课程审核id
     */
    private Integer courseCheckId;
    /**
     *课程id
     */
    private String courseId;
    /**
     *审核状态
     */
    private String checkStatus;
    /**
     *审核人的id
     */
    private String checkUserId;
    /**
     *审核内容
     */
    private String checkContent;
    /**
     *审核时间
     */
    private LocalDateTime checkTime;

}
