package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/5 20:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("edu_subject")
public class EduSubject {
    /* 主键id */
    @TableId
    private String id;

    /* 科目名 */
    private String title;

    /* 父id */
    private String parentId;

    /* 课程分类的状态 1 正常  0  禁用 */
    private String statusId;

    /* 备注 */
    private String remark;

    /* 排序 */
    private Integer sort;

    /* 生成时间 */
    private LocalDateTime gmtCreate;

    /* 上次修改时间 */
    private LocalDateTime gmtModified;

    /* 子科目 */
    @TableField(exist = false)
    private List<EduSubject> children = new ArrayList<>();
}
