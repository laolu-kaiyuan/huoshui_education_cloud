package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/7 18:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_check_course")
public class TbCheckCourse {
    /**
     * 课程审核id
     */
    @TableId(type = IdType.AUTO)
    private Integer courseCheckId;
    /**
     *课程id
     */
    private String courseId;
    /**
     *审核状态
     */
    private String checkStatus;
    /**
     *审核人的id
     */
    private String checkUserId;
    /**
     *审核内容
     */
    private String checkContent;
    /**
     *审核时间
     */
    private LocalDateTime checkTime;
}
