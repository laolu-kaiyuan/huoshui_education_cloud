package com.two.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName(value = "edu_course")
public class EduCourse {
    @TableId
    private String id;
    private String teacherId;
    private String subjectId;
    private String subjectParentId;
    private String title;
    private BigDecimal price;
    private Integer lessonNum;
    private String cover;
    private Integer buyCount;
    private Integer viewCount;
    private Integer version;
    private String status;
    private Integer isDeleted;
    private String gmtCreate;
    private String gmtModified;
    private String remark;
    private Integer isAdd;
}
