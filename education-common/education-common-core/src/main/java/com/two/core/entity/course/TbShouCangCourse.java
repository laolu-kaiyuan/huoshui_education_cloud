package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/12 15:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_shoucang_course")
public class TbShouCangCourse {
    @TableId
    private String id;

    private String memberId;

    private String courseId;
}
