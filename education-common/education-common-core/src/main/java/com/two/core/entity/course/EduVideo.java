package com.two.core.entity.course;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 11:56
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EduVideo {
    /**
     * 视频id
     */
    private String id;
    /**
     * 课程id
     */
    private String courseId;
    /**
     * 章节id
     */
    private String chapterId;
    /**
     * 课时名称
     */
    private String title;
    /**
     * 云端视频资源
     */
    private String videoSourceId;
    /**
     * 附属文件
     */
    private String videoOriginalName;
    /**
     * 排序字段
     */
    private Integer sort;
    /**
     * 播放次数
     */
    private Long playCount;
    /**
     * 是否可以试听：0收费1免费
     */
    private Integer isFree;
    /**
     *视频时长
     */
    private Double duration;
    /**
     *1正常2修改
     */
    private Integer status;
    /**
     *视频源文件大小（字节）
     */
    private Long size;
    /**
     *乐观锁
     */
    private Long version;
    /**
     *创建时间
     */
    private LocalDateTime gmtCreate;
    /**
     *更新时间
     */
    private LocalDateTime gmtModified;
}
