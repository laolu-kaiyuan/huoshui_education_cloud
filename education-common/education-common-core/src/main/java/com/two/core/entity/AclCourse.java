package com.two.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClassName AclCourse
 * @Description TODO
 * @Aythor WYX
 * @Date 2022/12/8 13:36
 * Version 1.0
 **/
@Data
@TableName("edu_course")
public class AclCourse {
    @TableId
    private String id;
    private String teacherId;
    private String subjectId;
    private String subjectParentId;
    private Integer lessonNum;
    private Long buyCount;
    private Long viewCount;
    private Long version;
    private Boolean isDeleted;
    private String remark;
    private Integer isAdd;
    private String cover;
    private String title;
    @TableField(exist = false) //表示该属性没有在表中存在对应的列
    private String subjectName;
    @TableField(exist = false)
    private String description;
    @TableField(exist = false) //审核状态
    private String checkStatus;
    @TableField(exist = false) //审核id
    private Integer courseCheckId;

    private Double price;
    private String status;
    /**
     * 创建时间
     * **/
    private LocalDateTime gmtCreate;
    /**
     * 更新时间
     * **/
    private LocalDateTime gmtModified;
}
