package com.two.core.entity.course;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 翟彦淇
 * @description TODO
 * @date 2022/12/9 10:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "edu_chapter")
public class EduChapter {
    /**
     * 章节的id
     */
    @TableId
    private String id;
    /**
     * 课程id
     */
    private String courseId;
    /**
     * 章节名称
     */
    private String title;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 生成时间
     */
    private LocalDateTime gmtCreate;
    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;
}
