package com.two.core.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket(){
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                        //组名
                        .groupName("在线教育")
                        //api信息
                        .apiInfo(getInfo())
                        //选择哪些生成api接口---根据请求路径选择  (2)根据包名选择
                        .select()
                         //根据请求路径选择
                        .paths(Predicates.not(PathSelectors.regex("/error")))
                        //        (2)根据包名选择
                         //.apis(RequestHandlerSelectors.basePackage("com.aaa.qy158springboot02.controller"))
                        .build()
                ;

        return docket;
    }

    private ApiInfo getInfo(){
        Contact DEFAULT_CONTACT = new Contact("用户名", "http://www.jd.com", "110@qq.com");
        ApiInfo info = new ApiInfo("在线教育API", "在线预约API", "2.0", "http://www.baidu.com",
                DEFAULT_CONTACT, "漫动者", "http://www.aaa.com", new ArrayList<VendorExtension>());
        return info;
    }
}