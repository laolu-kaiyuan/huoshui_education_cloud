package com.two.core.util;

import com.two.core.entity.AclPermission;
import com.two.core.vo.Page;


import java.util.List;

/**
 * @author ：lushimin（2741890790@qq.com）
 * @date ：Created in 2022/12/7 16:07
 * @description：
 * @modified By：
 * @version:
 */
public class MyListPage {
    /**
     * 为了本项目的特殊分页查询
     * @param list
     * @param pageNum
     * @param pageSize
     * @return
     */
    public static Page startPage(List<AclPermission> list, int pageNum, int pageSize) {
        if (list == null || list.size() == 0) {
            return null;
        }
        int count = list.size(); // 记录总数
        int pageCount = 0; // 页数
        if (count % pageSize == 0) {
            pageCount = count / pageSize;
        } else {
            pageCount = count / pageSize + 1;
        }
        if (pageNum > pageCount) {
            return null;
        }
        int fromIndex = 0; // 开始索引
        int toIndex = 0; // 结束索引

        if (pageNum != pageCount) {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = fromIndex + pageSize;
        } else {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = count;
        }
        if (fromIndex > count || toIndex > count) {
            return null;
        }
        return new Page(count,pageSize,list.subList(fromIndex, toIndex));
    }
}