package com.two.core.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "tb_member")
public class TbMember {
    @TableId
    private String id;
    private String openid;
    private String mobile;
    private String password;
    private String nickname;
    private Integer sex;
    private Integer age;
    private String avatar;
    private String sign;
    private Integer isType;
    private Integer isDisabled;
    private Integer isDeleted;
    private LocalDateTime gmtCreate;
    private LocalDateTime gmtModified;
    private String remark;
    private String salt;

}
