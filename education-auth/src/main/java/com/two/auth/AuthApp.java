package com.two.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

//启动时不加载DataSourceAutoConfiguration自动装配类
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableFeignClients  //开启openfeign注解
public class AuthApp {
    public static void main(String[] args) {
        SpringApplication.run(AuthApp.class,args);
    }
}