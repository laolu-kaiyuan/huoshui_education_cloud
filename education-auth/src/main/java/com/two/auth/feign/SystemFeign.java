package com.two.auth.feign;

import com.two.core.entity.AclPermission;
import com.two.core.entity.AclUser;
import com.two.core.vo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "education-system")
public interface SystemFeign {

    //根据账户查询用户信息
    @GetMapping("/system/user/getByName/{username}")
    public Result<AclUser> getByName(@PathVariable String username);

    //根据用户id查询对应的权限
    @GetMapping("/system/permission/getByUserid/{userid}")
    public Result<List<AclPermission>> getByUserid(@PathVariable String userid);
}